//
//  RequestTaskCell.swift
//  neoBMSCare
//
//  Created by mac on 7/3/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
class RequestTaskCell: MasterTableViewCell {

    
    
    //MARK:- Outlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightTableContraint: NSLayoutConstraint!
    
    
    //MARK:- Declare
    var idCell = "TaskCell"
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
        setupDataSource()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK:- SubFunction
    override func setupUI() {
        
    }
    override func setupDataSource() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "TaskCell", bundle: nil), forCellReuseIdentifier: idCell)
       self.heightTableContraint.constant = 5*Constants.HeightCell.task
    }
    
    //MARK:- Outlet Actions
}
extension  RequestTaskCell : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: idCell) as! TaskCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.HeightCell.task
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.didSelect(tag: self.tag, indexSelected: indexPath)
    }
    
}
