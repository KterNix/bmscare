//
//  NoticeCell.swift
//  ApartmentManager
//
//  Created by pro on 7/27/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit

class NoticeCell: MasterTableViewCell {

    
    
    
    
    @IBOutlet weak var noticeTitle: UILabel!
    @IBOutlet weak var noticeType: UIImageView!
    @IBOutlet weak var noticeDateTime: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    
    
    func configWithValue(n: Notice) {
        self.noticeTitle.text = "Bạn đã đăng ký tiện ích \(n.title)"
        let isUserPost = n.isUserPost ? "person" : "smallNotice"
        self.noticeType.tintColor = UIColor.lightGray
        self.noticeType.image = UIImage(named: "\(isUserPost)")?.withRenderingMode(.alwaysTemplate)
        let getDate:Date = Utils.StringToDate(n.date)
        self.noticeDateTime.text = Utils.timeAgoSince(getDate)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
