//
//  Houses.swift
//
//  Created by mac on 4/20/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public struct HouseModel: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let sellType = "sellType"
    static let areaWall = "areaWall"
    static let type = "type"
    static let price = "price"
    static let direction = "direction"
    static let bedNum = "bedNum"
    static let priceByMeter = "priceByMeter"
    static let id = "id"
    static let code = "code"
    static let viewType = "viewType"
    static let num = "num"
    static let images = "images"
    static let floor = "floor"
    static let area = "area"
    static let status = "status"
    static let projectTitle = "projectTitle"
    static let projectCode = "projectCode"
    static let blockName = "blockName"
    static let blockCode = "blockCode"
    static let path = "path"
    static let agencyCode = "agencyCode"
    static let projectId = "projectId"

  }

  // MARK: Properties
  public var sellType: String?
  public var areaWall: String?
  public var type: String?
  public var price: String?
  public var direction: String?
  public var bedNum: String?
  public var priceByMeter: String?
  public var id: String?
  public var code: String?
  public var viewType: String?
  public var num: String?
//  public var images: [ImageModel]?
  public var floor: String?
  public var area: String?
    public var projectTitle: String?
    public var projectCode: String?
    public var blockName: String?
    public var blockCode: String?
    public var path: String?
    public var status: String?
    public var agencyCode: String?
    public var projectId: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public init?(map: Map){

  }
    public init?() {}

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public mutating func mapping(map: Map) {
    sellType <- map[SerializationKeys.sellType]
    areaWall <- map[SerializationKeys.areaWall]
    type <- map[SerializationKeys.type]
    price <- map[SerializationKeys.price]
    direction <- map[SerializationKeys.direction]
    bedNum <- map[SerializationKeys.bedNum]
    priceByMeter <- map[SerializationKeys.priceByMeter]
    id <- map[SerializationKeys.id]
    code <- map[SerializationKeys.code]
    viewType <- map[SerializationKeys.viewType]
    num <- map[SerializationKeys.num]
//    images <- map[SerializationKeys.images]
    floor <- map[SerializationKeys.floor]
    area <- map[SerializationKeys.area]
    projectTitle <- map[SerializationKeys.projectTitle]
    projectCode <- map[SerializationKeys.projectCode]
    blockName <- map[SerializationKeys.blockName]
    blockCode <- map[SerializationKeys.blockCode]
    path <- map[SerializationKeys.path]
    status <- map[SerializationKeys.status]
    agencyCode <- map[SerializationKeys.agencyCode]
    projectId <- map[SerializationKeys.projectId]


  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = sellType { dictionary[SerializationKeys.sellType] = value }
    if let value = areaWall { dictionary[SerializationKeys.areaWall] = value }
    if let value = type { dictionary[SerializationKeys.type] = value }
    if let value = price { dictionary[SerializationKeys.price] = value }
    if let value = direction { dictionary[SerializationKeys.direction] = value }
    if let value = bedNum { dictionary[SerializationKeys.bedNum] = value }
    if let value = priceByMeter { dictionary[SerializationKeys.priceByMeter] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = viewType { dictionary[SerializationKeys.viewType] = value }
    if let value = num { dictionary[SerializationKeys.num] = value }
//    if let value = images { dictionary[SerializationKeys.images] = value.map { $0.dictionaryRepresentation() } }
    if let value = floor { dictionary[SerializationKeys.floor] = value }
    if let value = area { dictionary[SerializationKeys.area] = value }
    if let value = projectTitle { dictionary[SerializationKeys.projectTitle] = value }
    if let value = projectCode { dictionary[SerializationKeys.projectCode] = value }
    if let value = blockName { dictionary[SerializationKeys.blockName] = value }
    if let value = blockCode { dictionary[SerializationKeys.blockCode] = value }
    if let value = path { dictionary[SerializationKeys.path] = value }
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = agencyCode { dictionary[SerializationKeys.agencyCode] = value }
    if let value = projectId { dictionary[SerializationKeys.projectId] = value }

    return dictionary
  }

}
