//
//  FieldChatView.swift
//  neoBMSCare
//
//  Created by mac on 8/9/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class FieldChatView: UICustomView {

    
    //MARK:- Outlet
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btnSend: UIButton!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
         setupUI()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.backgroundColor = .white
        self.textView.text = "Tin nhắn ..."
        self.textView.textColor = .lightGray
        self.textView.delegate = self
        self.btnSend.setAttributedTitle(NSAttributedString(string: "Gửi", attributes: [NSAttributedStringKey.font : UIFont.recommentFont,NSAttributedStringKey.foregroundColor : UIColor.secondColor()  ]), for: .normal)
    }
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions
    
}
extension FieldChatView:UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Tin nhắn ..."
            textView.textColor = UIColor.lightGray
        }
    }
}
