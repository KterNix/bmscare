//
//  Blocks.swift
//
//  Created by mac on 4/20/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public struct FloorModel: Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let name = "name"
        static let floor = "floor"
        static let id = "id"
        static let code = "code"
        static let image = "image"
        static let houses = "houses"
    }
    
    // MARK: Properties
    public var name: String?
    public var floor: String?
    public var id: String?
    public var code: String?
    public var image: String?
    public var houses: [HouseModel]?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public init?(map: Map){
        
    }
    init() {
        
    }
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public mutating func mapping(map: Map) {
        name <- map[SerializationKeys.name]
        floor <- map[SerializationKeys.floor]
        id <- map[SerializationKeys.id]
        code <- map[SerializationKeys.code]
        image <- map[SerializationKeys.image]
        houses <- map[SerializationKeys.houses]
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = name { dictionary[SerializationKeys.name] = value }
        if let value = floor { dictionary[SerializationKeys.floor] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = code { dictionary[SerializationKeys.code] = value }
        if let value = image { dictionary[SerializationKeys.image] = value }
        if let value = houses { dictionary[SerializationKeys.houses] = value.map { $0.dictionaryRepresentation() } }
        return dictionary
    }
    
}
