//
//  CategoryProductCell.swift
//  neoBMSCare
//
//  Created by mac on 8/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class CategoryProductCell: MasterCollectionViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var imageCategoryProduct: UIImageView!
    @IBOutlet weak var nameCategory: RecommentLabel!
    @IBOutlet weak var numberProuct: NormalLabel!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        uiAfterLoaded()
        // Initialization code
    }
    
    //MARK:- SubFunction
    func uiAfterLoaded() {
        self.backgroundColor = .white
        self.numberProuct.color = .lightGray
        self.dropShadow()
    }
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions

}
