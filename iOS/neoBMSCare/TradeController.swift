//
//  MBController.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit

class TradeController: UIViewController {

    
    
    
    
    @IBOutlet weak var myTableView: UITableView!
    
    
    var listTrading = [Trading]() {
        didSet{
            self.myTableView.reloadData()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
        // Do any additional setup after loading the view.
    }
    
    
    
    private func setupUI() {
        self.title = "Mua bán"
        setBackButton()
    }

    private func setupDataSource() {
        var t = Trading()
        t.date = "2017-02-23 09:10:11"
        t.image = "apartment1.jpg"
        t.name = "Lò Văn Pháo"
        t.price = 200000
        t.title = "Bán chung cư cao cấp,Bán chung cư cao cấp,Bán chung cư cao cấp,Bán chung cư cao cấp,Bán chung cư cao cấp"
        t.type = "Cần Bán"
        t.phone = 0909090909
        t.address = "25 Trần Quốc Toản"
        self.listTrading.append(t)
        
        t = Trading()
        t.date = "2017-05-23 09:10:11"
        t.image = "apartment2.jpg"
        t.name = "Lò Văn Pháo"
        t.price = 300000
        t.title = "Mua xe hơi"
        t.phone = 0909090909
        t.type = "Cần Mua"
        t.address = "25 Trần Quốc Toản"
        self.listTrading.append(t)
        
        t = Trading()
        t.date = "2017-07-23 09:10:11"
        t.image = "apartment3.jpeg"
        t.name = "Lò Văn Pháo"
        t.price = 200000
        t.title = "Cho thuê căn hộ cao cấp"
        t.phone = 0909090909
        t.type = "Cho Thuê"
        t.address = "25 Trần Quốc Toản"
        self.listTrading.append(t)
    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}

extension TradeController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listTrading.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mbCell", for: indexPath) as! MBCell
        cell.configWithValue(t: self.listTrading[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.myTableView.deselectRow(at: indexPath, animated: true)
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "TradeDetailController") as! TradeDetailController
        vc.tradingDetail = self.listTrading[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lineColor = UIColor.init(red: 228, green: 227, blue: 229)
        if indexPath.row == 0 {
            let line = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 1))
            line.backgroundColor = lineColor
            cell.addSubview(line)
            cell.bringSubview(toFront: line)
        }else if indexPath.row == self.listTrading.count - 1 {
            let line = UIView(frame: CGRect(x: 8, y: 0, width: self.view.frame.size.width, height: 1))
            line.backgroundColor = lineColor
            cell.addSubview(line)
            cell.bringSubview(toFront: line)
            let sizeHeight = self.myTableView.rectForRow(at: indexPath)
            let lineBottom = UIView(frame: CGRect(x: 0, y: sizeHeight.size.height - 1, width: self.view.frame.size.width, height: 1))
            lineBottom.backgroundColor = lineColor
            cell.addSubview(lineBottom)
            cell.bringSubview(toFront: lineBottom)
        }else{
            let line = UIView(frame: CGRect(x: 8, y: 0, width: self.view.frame.size.width, height: 1))
            line.backgroundColor = lineColor
            cell.addSubview(line)
            cell.bringSubview(toFront: line)
        }
    }
}
















