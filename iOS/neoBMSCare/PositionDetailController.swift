//
//  PositionDetailController.swift
//  neoBMSCare
//
//  Created by pro on 7/28/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit

class PositionDetailController: UIViewController {

    @IBOutlet weak var textLBL: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setupUI() {
        self.title = "Chi tiết địa điểm"
        setBackButton()
    }
    func setupDataSource() {
        
    }

    

}
