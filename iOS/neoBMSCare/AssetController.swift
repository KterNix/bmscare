//
//  AssetController.swift
//  neoBMSCare
//
//  Created by mac on 7/4/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class AssetController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    let listTitileSection = ["Thiết bị điện tử" , "Thiết bị công trình","Vật liệu","Thực vật","Khác"]
    let textDevice = [["Camera","Báo cháy"],["Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm","Máy bơm"],["Thép","Dây điện","Cầu giao","Đồng hồ điện"],["Cây si","Bonsai","Cây dầu"],["Dầu xăng"]]
    var textSelected = [[String]]()
    let idCell = "AssetTypeCell"
    let idReuseView = "HeaderSectionCollection"
    var selectedBtnMore = [Bool]()
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return listTitileSection.count
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return textSelected[section].count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath)
        as! AssetTypeCell

        let checkUnder = [true,false]
        let randomIndex = Int(arc4random_uniform(UInt32(checkUnder.count)))

        cell.setupDataSource(under: checkUnder[randomIndex])
        cell.nameAsset.text = textDevice[indexPath.section][indexPath.row]
//        cell.delegate = self
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = DetailAssetController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height:CGFloat = /*(indexPath.section == 0 ? 156 :*/ 110
        return CGSize(width: collectionView.frame.width - 10, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5 )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 70)
    }
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: idReuseView, for: indexPath) as! HeaderSectionCollection
            view.text = "\(listTitileSection[indexPath.section]) ( \(textDevice[indexPath.section].count) )"
            view.isEnableResizingBtnMore = true
            view.isSelected = selectedBtnMore[indexPath.section]
            view.btnMore.tag = indexPath.section
            view.btnMore.addTarget(self, action: #selector(self.didSelectMore(btn:)), for: .touchUpInside)
            view.setupDataSource()
            return view
        default:
            return  UICollectionReusableView()
            
        }
        
    }
    //MARK:- SubFunction
    func setupUI() {
        self.collectionView?.backgroundColor = UIColor.groupTableViewBackground
        self.collectionView?.alwaysBounceVertical = true
        setBarButtons()
    }
    func setBarButtons() {
        setBackButton()
        navigationItem.titleView = nil
        navigationItem.rightBarButtonItems = nil
        createRightBarItem(title: "Tìm kiếm", selector: #selector(self.didPressSearch),fontSize: 15)
        self.title = "Tài sản"
    }
    @objc func didPressSearch(){
        self.createSearchBar(placeHolder: "Điện, dụng cụ ...", doneSelector: #selector(self.didPressDone))
        UIView.animate(withDuration: 0.5, animations: {
            self.navigationItem.titleView?.transform = CGAffineTransform.identity

            
        }) { (bool) in
            
        }
//        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0, initialSpringVelocity: 0, options: .layoutSubviews, animations: {
//        }, completion: nil)
    }
    @objc func didPressDone() {
        
    }
    func setupDataSource() {
        self.parseTextSelect()
        self.collectionView?.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        self.collectionView?.register(UINib(nibName: idReuseView, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: idReuseView)
        self.collectionView?.delegate = self
        self.collectionView?.dataSource = self
    }
    @objc func didSelectMore(btn:UIButton) {
        let tag = btn.tag
        self.selectedBtnMore[tag] = self.textSelected[tag].count > 0 ? false : true
        self.textSelected[tag] = self.textSelected[tag].count > 0 ? [] : self.textDevice[tag]
        self.collectionView?.performBatchUpdates({
            self.collectionView?.reloadSections(IndexSet(integer: btn.tag))
            
        }, completion: nil)
    }
    func parseTextSelect() {
        var i = 0
        repeat {
            i += 1
            let array = [String]()
            textSelected.append(array)
            let selected = false
            selectedBtnMore.append(selected)
        }while i < textDevice.count
    }
    //MARK:- Outlet Actions

}
extension AssetController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        UIView.animate(withDuration: 0.25, animations: {
            self.navigationItem.titleView?.transform = CGAffineTransform(translationX: 0, y: -50)

        }) { (bool) in
            UIView.animate(withDuration: 0.25, animations: {
                self.setBarButtons()
            }, completion: nil)
        }

    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
    
}
