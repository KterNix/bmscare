//
//  LeftController.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
import Firebase
import ObjectMapper
import SDWebImage

class LeftController: UIViewController {

    
    
    
    
    @IBOutlet weak var myTableView: UITableView!
    
    @IBOutlet weak var avatarUser: UIImageView!
    @IBOutlet weak var title_1: UILabel!
    @IBOutlet weak var title_2: UILabel!
    var accountType:AccountType = .Manager
    var userModel = UserModel()
    
    let facebookManager = FBSDKLoginManager()
    
    var itemMenu:[String] = ["Ban quản lý","Câu hỏi thường gặp","Về chúng tôi","Đánh giá ứng dụng","Góp ý"]
    var itemIcon:[String] = ["manager","FAQ","aboutUS","rating","feedBack"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    private func setupUI() {
        self.title = "Trang cá nhân"
        
        let tap =  UITapGestureRecognizer(target: self, action: #selector(self.didSelectedAvatar ))
        avatarUser.addGestureRecognizer(tap)
        avatarUser.isUserInteractionEnabled = true
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        avatarUser.layer.cornerRadius = avatarUser.height/2
        avatarUser.layer.masksToBounds = true
        avatarUser.borderColor = .mainColor()
        avatarUser.layer.borderWidth = 0.3
    }
    @objc func didSelectedAvatar() {
        let vc = ProfileController.getViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    private func setupDataSource() {
        switch accountType {
        case .Manager:
            break
        case .User:
            let arrayText = ["Nhà bạn","Phàn nàn"]
            
            let icons = ["house","complaint"]
            self.itemMenu.insert(contentsOf: arrayText, at: 0)
            self.itemIcon.insert(contentsOf: icons, at: 0)
        case .Supplier:
            break
        }
        self.myTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
extension LeftController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemMenu.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "leftCell", for: indexPath) as! LeftCell
        cell.itemList.text = itemMenu[indexPath.row]
        cell.imageList.tintColor = UIColor.mainColor()
        cell.imageList.image = UIImage(named: "\(itemIcon[indexPath.row])")?.withRenderingMode(.alwaysTemplate)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        myTableView.deselectRow(at: indexPath, animated: true)
        let iconText = itemIcon[indexPath.row]
        var vc = UIViewController()
        switch iconText {
        case "FAQ":
            vc = FAQController.getViewController()
            
        case "manager":
            guard (self.tabBarController as! MasterController).accountType == .Manager else {
                vc = AboutBQLController.getViewController()
                self.navigationController?.pushViewController(vc, animated: true)
                return
            }
            vc = ManagementController.getViewController()
            
        case "aboutUS":
            vc = AboutUSController.getViewController()
            
        case "rating":
            return
            
        case "feedBack":
            vc = FeedbackController.getViewController()
            
        case "complaint":
            vc = ComplainController()

        case "house":
            vc = DetailHouseInfo()
            
        case "service":
            vc = ExtrasController.getViewController()
        
        default:
            break
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
