//
//  DetailTaskController.swift
//  neoBMSCare
//
//  Created by mac on 7/9/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka
import PopupDialog
private let idCell = "ResponsibleCell"
class DetailTaskController: MasterFormController {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func setUpForm() {
        setupDataSource()
        
    }
//    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 50
//    }
    //MARK:- SubFunction
    func setupUI() {
        self.title = "Chi tiết yêu cầu"
        setBackButton()
    }
    
    func setupDataSource() {
        form
            +++ Section("Dịch vụ"){ section in
                
//                var header = HeaderFooterView<HeaderDetailTask>(.nibFile(name: "HeaderDetailTask", bundle: nil))
//
//                header.height = { 200 }
//                header.onSetupView = { view, _ in
//                    view.title.color = .white
//                }
//                section.header = header
            }
            <<< LabelRow() {
                $0.title = "Loại"
                $0.value = "Sửa chữa"
                }
            <<< LabelRow() {
                $0.title = "Thiết bị"
                $0.value = "Ống nước"
            }
            +++ Section("Khách") { section in
                
            }
            <<< LabelRow() {
                $0.title = "Tên"
                $0.value = "Nguyễn Kim Bình"
            }
            <<< LabelRow() {
                $0.title = "SĐT"
                $0.value = "091234567"
            }
            <<< LabelRow(){
                $0.title = "Căn hộ"
                $0.value = "Block A - 308"
                }
            <<< TextAreaRow() {
                $0.title = "Ghi chú"
                $0.value = "Ống nước bị rỉ, lỗ lớn cần thay mới. Trước khi lên sửa vui lòng gọi điện trước."
                }
            +++ Section("Thời gian")
            <<< LabelRow(){
                $0.title = "Đặt vào lúc"
                $0.value = "18:30 28/07/2018"
            }
            <<< LabelRow(){
                $0.title = "Có thể bắt đầu"
                $0.value = "29/07/2018"
            }
            <<< LabelRow(){
                $0.title = "Khoảng thời gian rãnh"
                $0.value = "18:00 - 21:00"
            }
            +++ Section(){section in
                //Footer Người đảm nhiệm
                var footer = HeaderFooterView<DetailHouseInforHeader>(.nibFile(name:"DetailHouseInforHeader",bundle:nil))
                footer.onSetupView = {view, _ in
                    view.title.text = "Người đảm nhiệm"
                    view.setupUI()
                    view.btnMore.text = "Thêm"
                    view.btnMore.addTarget(self, action: #selector(self.didPressAddMoreResponsible), for: .touchUpInside)
                    view.collectionView.backgroundColor = UIColor.groupTableViewBackground
                    view.collectionView.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
                    view.collectionView.delegate = self
                    view.collectionView.dataSource = self
                    
                }
                footer.height = {200}
                section.footer = footer
            }
            +++ Section(){section in
                var header = HeaderFooterView<UIView>(.callback({
                    let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
                    let button = StyleGhost(frame: CGRect(x: 0, y: 0, width: self.view.width * 0.8, height: 40))
                    button.text = "Hoàn thành"
                    button.center = view.center
                    button.tag = 2
                    view.addSubview(button)
                    return view
                }))
                header.height = {50}
                header.onSetupView = {view, _ in
                    guard let button = view.viewWithTag(2) as? UIButton else {return}
                    button.addTarget(self, action: #selector(self.didPressDone), for: .touchUpInside)
                }
                section.header = header
            }
            
            +++ Section("Bình luận") { section in
                var footer =  HeaderFooterView<CommentFooter>(.nibFile(name: "CommentFooter", bundle: nil))
                footer.height = {self.view.height - (Constants.heightSubmitComment + Constants.HeightTool.tabbar)}
                section.footer = footer
                
                var header = HeaderFooterView<SubmitComment>(.nibFile(name:"SubmitComment",bundle:nil))
                header.onSetupView = {view, _ in
                    view.setupDataSource(textTitle: "Bình luận")

                }
                header.height = {Constants.heightSubmitComment}
                section.header = header
            }
        
        
    }
    @objc func didPressDone() {
        
    }
    @objc func  didPressAddMoreResponsible() {
        
        let vc = HumanResourceController(nibName: "HumanResourceController", bundle: nil)
        vc.collectionView?.frame = CGRect(x: 0, y: 0, width: 335, height: 400)
        let heightContraint = NSLayoutConstraint(item:vc.view , attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 400)
        vc.view.addConstraint(heightContraint)
        vc.collectionView?.allowsMultipleSelection = true
        let cancel = CancelButton(title: "Huỷ") {
            
        }
        let accept = DefaultButton(title: "Xong") {
            
        }
        cancel.setTitleColor(.lightGray, for: .normal)
        cancel.backgroundColor = UIColor.groupTableViewBackground
        accept.setTitleColor(.white, for: .normal)
        accept.backgroundColor = .mainColor()
        let popUp = PopupDialog(viewController: vc)
        popUp.buttonAlignment = .horizontal
        popUp.addButtons([cancel,accept])
        self.present(popUp, animated: true, completion: nil)
    }
    //MARK:- Outlet Actions

}
extension DetailTaskController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! ResponsibleCell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: collectionView.frame.height * 0.8)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
