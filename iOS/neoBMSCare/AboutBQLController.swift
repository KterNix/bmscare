//
//  AboutBQLController.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import Eureka
class AboutBQLController: MasterFormController {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        title = "Ban quản lý"
        setBackButton()
        form
        +++ Section() {
            var header = HeaderFooterView<HeaderView>(.class)
            header.onSetupView = { view, _ in
                view.imageProfile.image = UIImage(named: "logoApartment")
            }
            $0.header = header
        }
        +++ Section()
            <<< ButtonRow(){
//                $0.title =
                $0.title = "Chung cư VINHOMES Green Bay"                
            }
            <<< LabelRow(){
                $0.title = "Địa chỉ"
                $0.value = "47/25 Trần Quốc Toản, phường 8, Quận 3"
                
            }
            <<< LabelRow(){
                $0.title = "Email"
                $0.value = "cskh.vinhomes@gmail.com"
            }
            <<< LabelRow(){
                $0.title = "Số điện thoại"
                $0.value = "0983748218"
                }
        +++ Section(){section in
                var header = HeaderFooterView<UIView>(.callback({
                    let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
                    let button = StyleGhost(frame: CGRect(x: 0, y: 0, width: self.view.width * 0.8, height: 40))
                    button.text = "Gọi đến Ban Quản Lý"
                    button.center = view.center
                    button.tag = 2
                    view.addSubview(button)
                    return view
                }))
                header.height = {50}
                header.onSetupView = {view, _ in
                    guard let button = view.viewWithTag(2) as? UIButton else {return}
                    button.addTarget(self, action: #selector(self.callNumber), for: .touchUpInside)
                }
                section.header = header
            }
        
        
    }
    func setupDataSource() {
        
    }
    @objc func callNumber()  {
        if let url = URL(string: "tel://\(0983182893)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    //MARK:- Outlet Actions

    

}
