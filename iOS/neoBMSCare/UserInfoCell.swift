//
//  UserInfoCell.swift
//  ApartmentManager
//
//  Created by pro on 7/27/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import Eureka
import SDWebImage

final class UserInfoRow: Row<UserInfoCell>, RowType {
    required init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<UserInfoCell>(nibName: "UserInfoCell")
    }
}

struct Users: Equatable {
    var name: String
    var buildingCode: String
    var pictureUrl: String
}
func ==(lhs: Users, rhs: Users) -> Bool {
    return lhs.name == rhs.name
}

final class UserInfoCell: Cell<Users>, CellType {

    
    @IBOutlet weak var avatarUser: UIImageView!
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var buildingCode: UILabel!
    
    required init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    
    
    
    override func setup() {
        super.setup()
        // we do not want our cell to be selected in this case. If you use such a cell in a list then you might want to change this.
        selectionStyle = .none
        
        // configure our profile picture imageView
        avatarUser.contentMode = .scaleAspectFill
        avatarUser.clipsToBounds = true
        avatarUser.layer.cornerRadius = 10
        
        // define fonts for our labels
        nameUser.font = .systemFont(ofSize: 15)
        buildingCode.font = .systemFont(ofSize: 12)
        
        // set the textColor for our labels
        
        
        // specify the desired height for our cell
        height = { return CGFloat(115) }()
        
        // set a light background color for our cell
        backgroundColor = UIColor.white
    }
    
    override func update() {
        super.update()
        
        // we do not want to show the default UITableViewCell's textLabel
        textLabel?.text = nil
        
        // get the value from our row
        guard let user = row.value else { return }
        
        avatarUser.sd_setIndicatorStyle(.gray)
        avatarUser.sd_addActivityIndicator()
        avatarUser.sd_setImage(with: URL(string: user.pictureUrl), completed: nil)
        // set the texts to the labels
        buildingCode.text = user.buildingCode
        nameUser.text = user.name
    }
}
