//
//  HeaderTitleView.swift
//  neoBMSCare
//
//  Created by mac on 7/12/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class HeaderTitleView: UICustomView {

    
    //MARK:- Outlet
    @IBOutlet var viewLine: [UIView]!
    @IBOutlet weak var titleLbl: TitleLabel!
    @IBOutlet weak var btnMore: StyleGhost!
    
    
    
    //MARK:- Declare
    var text = ""
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.viewLine.forEach { (view) in
            view.backgroundColor = .black
        }
    }
    func setupDataSource() {
        self.titleLbl.text = text
    }
    
    //MARK:- Outlet Actions
    @IBAction func abtnMore(_ sender: Any) {
        
    }

}
