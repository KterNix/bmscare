//
//  HouseDataSource.swift
//  neoReal
//
//  Created by pro on 4/18/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import UIKit

class BlockDetailDataSource: NSObject {
    
    var datasource = [FloorModel]()
    
    
    var didSelectRow: ((_ row: Int,_ item: HouseModel) -> Void)?
    init(datasource: [FloorModel]) {
        self.datasource = datasource
    }
    var didScroll: ((_ scrollView:UIScrollView) -> Void)?
    var didEndScroll: (() -> Void)?
}

extension BlockDetailDataSource: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.didScroll!(scrollView)
            
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.didEndScroll!()
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 10//datasource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        let value = datasource[section].houses
        return 20//value?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BlockDetailCell", for: indexPath) as! BlockDetailCell
        
//        if var val = datasource[indexPath.section].houses {
//            let item = val[indexPath.row]
//            cell.configWith(value: item)
//        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        if let val = datasource[indexPath.section].houses {
//            let item = val[indexPath.row]
//            if let selectedRow = didSelectRow {
//                selectedRow(indexPath.row, item)
//            }
//        }
        let house = HouseModel()
        didSelectRow!(4,house!)
        
    }
    
   
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sizeWidth = UIScreen.main.bounds.width / 4
        let size = CGSize(width: sizeWidth, height: 75)
        return size
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier:"SectionView", for: indexPath) as! SectionView
//            let value = datasource[indexPath.section]
            headerView.titleLBL.text = "Tầng \(indexPath.section)"
            return headerView

    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        let size = CGSize(width: UIScreen.main.bounds.width, height: 8)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let size = CGSize(width: UIScreen.main.bounds.width, height: 50)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
}
