//
//  DetailHouseInfo.swift
//  neoBMSCare
//
//  Created by mac on 7/24/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka
import PopupDialog
private let idCellHistory = "HistoryBasicCell"
private let idCellPayment = "PaymentBasicCell"
private let tagSectionActivityHistory = "activityHistory"
private let tagSectionFeeMonthly = "feeMonthly"
class DetailHouseInfo: MasterFormController {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    var textMonth = ["12","11","10","07","06","05","04","03","02","01"]
    var didPressMore = false
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func setUpForm() {
        
        form
        +++ Section("chủ căn hộ")
            <<< LabelRow(){
                $0.title = "Tên"
                $0.value = "Huỳnh Thị Mến"
            }
            <<< LabelRow(){
                $0.title = "Sinh ngày"
                $0.value = "11/10/1988"
            }
            <<< LabelRow(){
                $0.title = "CMND/Hộ chiếu"
                $0.value = "280 176 528"
            }
            <<< LabelRow(){
                $0.title = "Ngày mua"
                $0.value = "15/02/2015"
            }
            <<< PushRow<String>() {
                $0.title = "Thành viên"
                $0.value = "(4)"
                $0.disabled = true
                }.onCellSelection({ (cell, row) in
                    let vc = HumanResourceController(nibName: "HumanResourceController", bundle: nil)
                    vc.collectionView?.frame = CGRect(x: 0, y: 0, width: 335, height: 400)
                    let heightContraint = NSLayoutConstraint(item:vc.view , attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 400)
                    vc.view.addConstraint(heightContraint)
                    vc.collectionView?.allowsSelection = false
                    vc.type = .housesMembers
                    let popUp = PopupDialog(viewController: vc)
                    self.present(popUp, animated: true, completion: nil)
                })
        +++ Section("Liên hệ")
            <<< LabelRow(){
                $0.title = "Điện thoại"
                $0.value = "091837426"
            }
            <<< LabelRow(){
                $0.title = "Email"
                $0.value = "Men@gmail.com"
                
            }
            
        +++ Section() { section in
            section.tag = tagSectionActivityHistory
                var header = HeaderFooterView<DetailHouseInforHeader>(.nibFile(name: "DetailHouseInforHeader", bundle:nil))
                header.height = {200}
                header.onSetupView = { view , _ in
                    view.title.text = "Lịch sử hoạt động"
                    view.isEnableResizingButton = true
                    view.setupUI()
                    view.btnMore.addTarget(self, action: #selector(self.didPressMore(btn:)), for: .touchUpInside)
                    view.btnMore.tag = 0
                    view.collectionView.tag = 0
                    view.collectionView.register(UINib(nibName: idCellHistory, bundle: nil), forCellWithReuseIdentifier: idCellHistory)
                    view.collectionView.delegate = self
                    view.collectionView.dataSource = self
                }
                section.header = header
                
            }
            
        +++ Section() { section in
            section.tag = tagSectionFeeMonthly
                var header = HeaderFooterView<DetailHouseInforHeader>(.nibFile(name: "DetailHouseInforHeader", bundle:nil))
                header.height = {200}
                header.onSetupView = { view , _ in
                    view.title.text = "Phí dịch vụ"
                    view.setupUI()
                    view.isEnableResizingButton = true
                    view.btnMore.addTarget(self, action: #selector(self.didPressMore(btn:)), for: .touchUpInside)
                    view.btnMore.tag = 1
                    view.collectionView.tag = 1
                    view.collectionView.register(UINib(nibName: idCellPayment, bundle: nil), forCellWithReuseIdentifier: idCellPayment)
                    view.collectionView.delegate = self
                    view.collectionView.dataSource = self
                }
                section.header = header
        }
        
    }
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    //MARK:- SubFunction
    @objc func didPressMore(btn:UIButton) {
        self.changeHeightForSection(button: btn)
    }
    func changeHeightForSection(button:UIButton) {
        didPressMore = !didPressMore
        let tag = button.tag == 0 ? tagSectionActivityHistory : tagSectionFeeMonthly
        let height = didPressMore ? self.view.height - Constants.HeightTool.tabbar : 200
        
        form.sectionBy(tag: tag)?.header?.height = {height}
        form.sectionBy(tag: tag)?.reload(with: .fade)
        ((form.sectionBy(tag: tag)?.header?.viewForSection(form.sectionBy(tag: tag)!, type: .header) as! DetailHouseInforHeader).collectionView.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = didPressMore ? .vertical : .horizontal
        (form.sectionBy(tag: tag)?.header?.viewForSection(form.sectionBy(tag: tag)!, type: .header) as! DetailHouseInforHeader).collectionView.collectionViewLayout.invalidateLayout()
        
        (form.sectionBy(tag: tag)?.header?.viewForSection(form.sectionBy(tag: tag)!, type: .header) as! DetailHouseInforHeader).pressMore = didPressMore ? true : false
        self.tableView.scrollToRow(at: IndexPath(row: NSNotFound, section: button.tag == 0 ? 2 : 3), at: .top, animated: true)
        
        self.tableView.isScrollEnabled = didPressMore ? false : true
    }
    func setupUI() {
        if self.navigationController?.viewControllers.first != self {
             self.title =  "Chi tiết căn hộ"
            setBackButton()
        }else {
             self.title = "Nhà bạn"
        }
    }
    func setupDataSource() {
        setUpForm()
    }
    
    //MARK:- Outlet Actions
}
extension DetailHouseInfo : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCellHistory, for: indexPath)
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCellPayment, for: indexPath) as! PaymentBasicCell
            
            cell.setUpData(month: textMonth[indexPath.row], year: "2018" )
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 0 {
            let vc = DetailTaskController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: "HistoryDetailController") as! HistoryDetailController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = didPressMore ? (collectionView.width - 15 )/2 : 170
        let height:CGFloat = didPressMore ? 160 : 140
        return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
}

