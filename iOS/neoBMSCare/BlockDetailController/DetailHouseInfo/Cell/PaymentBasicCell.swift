//
//  PaymentBasicCell.swift
//  neoBMSCare
//
//  Created by mac on 7/24/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class PaymentBasicCell: MasterCollectionViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var monthLbl: UILabel!
    @IBOutlet weak var yearLbl: UILabel!
    @IBOutlet weak var totalMoney: RecommentDiscountLabel!
    @IBOutlet weak var viewCaution: UIView!
    @IBOutlet weak var numberNotice: RecommentLabel!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setupUI() {
        self.dropShadow()
    }
    
    //MARK:- SubFunction
    func setUpData(month:String, year:String)  {
        self.monthLbl.text = month
        self.yearLbl.text = year
        self.totalMoney.color = .black
        self.numberNotice.color = .white
    }
    
    //MARK:- Outlet Actions

}
