//
//  HistoryBasicCell.swift
//  neoBMSCare
//
//  Created by mac on 7/24/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class HistoryBasicCell: MasterCollectionViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var timeAgo: NormalLabel!
    @IBOutlet weak var titleHistory: RecommentLabel!
    @IBOutlet weak var codeHistory: NormalLabel!
    @IBOutlet weak var timeCreate: NormalLabel!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setupUI() {
        backgroundColor = .mainColor()
        dropShadow()
        timeAgo.color = .white
        titleHistory.color = .white
        codeHistory.color = .white
        timeCreate.color = .white
    }
    func setupDataSource() {
        
    }
    //MARK:- SubFunction
    
    
    //MARK:- Outlet Actions

}
