//
//  HouseController.swift
//  neoReal
//
//  Created by pro on 4/18/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import DropDown


class BlockDetailController: UIViewController, IndicatorInfoProvider {
    
    

    
    var itemInfo:IndicatorInfo = ""
    
    @IBOutlet weak var myCollectionView: UICollectionView!
    @IBOutlet weak var collectionViewConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var statusHouseCollectionView: UICollectionView!
    
    
    var blockDetailDataSource: BlockDetailDataSource?
    var statusHouseDataSource: StatusHouseDataSource?
    
    let textForStatusHouse = ["Báo vắng","Trống","Đang ở"]
    var listBlocks = [FloorModel]() {
        didSet {
//            myCollectionView.reloadData()
        }
    }
    var block :BlockModel?
//    let keyStatus = [HouseStatus.Active.rawValue,HouseStatus.Lock.rawValue,HouseStatus.Deactive.rawValue,HouseStatus.Deposited.rawValue,HouseStatus.Sold.rawValue,HouseStatus.Book.rawValue]
    
    
    
    var floor:String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        collectionViewConstraintHeight.constant = 0
//        view.layoutIfNeeded()
        setupUI()
        configTable()
        
    }
    
    func setupUI() {
        self.statusHouseCollectionView.dropShadow()
    }
    
    //MARK: - FILTER USING DROPDOWN
    func dropdownHandle() {
        
        guard floor != "Tất cả" else {
            setupDataSource(blocks: listBlocks)
            return
        }
        
        var filterFloor = listBlocks
        filterFloor = filterFloor.filter { $0.floor.asString == floor.split(separator: " ")[1] }
        
        setupDataSource(blocks: filterFloor)
        
        DispatchQueue.main.async {
            self.myCollectionView.reloadData()
        }
    }
    
    
    
    func configTable() {
        myCollectionView.register(UINib(nibName: "BlockDetailCell", bundle: nil), forCellWithReuseIdentifier: "BlockDetailCell")
        myCollectionView.register(UINib(nibName: "SectionView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "SectionView")
        statusHouseCollectionView.register(UINib(nibName: "StatusHouseCell", bundle: nil), forCellWithReuseIdentifier: "StatusHouseCell")
        
        //MARK: - ASSIGN HOUSES TO DICTIONARY
        if self.block != nil {
            var houses = [HouseModel]()
            guard let floors = block?.floors else { return }
            self.listBlocks = floors
            for i in 0..<floors.count {
                let houseInFloor = floors[i].houses ?? [HouseModel]()
                houses.append(contentsOf: houseInFloor)
            }
            
            // Key Status
//            var genaralClosedHouse = 0
//
//            keyStatus.forEach { (staus) in
//                
//                let houseStatus = houses.filter{$0.status.asString == staus}
//                let dic = (staus , houseStatus.count)
//                if staus == HouseStatus.Deactive.rawValue || staus == HouseStatus.Lock.rawValue{
//                    genaralClosedHouse += houseStatus.count
//                }else {
//                    categories.append(dic)
//                }
//                
//            }
//            let keyClosedHouse = (HouseStatus.Deactive.rawValue,genaralClosedHouse)
//            categories.append(keyClosedHouse)
        }
        
        setupDataSource(blocks: listBlocks)

    }
    
    
    func setupDataSource(blocks: [FloorModel]) {
        //MARK: - SORT
//        var blocksSorted = [String: [HouseModel]]()
//        let sortedValues = blocks.sorted(by: { $0.0 < $1.0 })
        
//        for (k,v) in sortedValues {
//            blocksSorted[k] = v
//        }
        blockDetailDataSource = BlockDetailDataSource(datasource: blocks)
        blockDetailDataSource?.didSelectRow = didSelect(row:item:)
        blockDetailDataSource?.didScroll = didScroll(scrollView:)
        blockDetailDataSource?.didEndScroll = didEndScroll

        statusHouseDataSource = StatusHouseDataSource(datasource: self.textForStatusHouse)
        
        myCollectionView.delegate = blockDetailDataSource
        myCollectionView.dataSource = blockDetailDataSource
        statusHouseCollectionView.delegate = statusHouseDataSource
        statusHouseCollectionView.dataSource = statusHouseDataSource
        
        DispatchQueue.main.async {
            self.myCollectionView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}
extension BlockDetailController {
    func didEndScroll() {
        guard let parent = self.parent as? PageBlocks else {return}
        guard let brandParent = parent.parent as? ParentBlocksController else {return}
        brandParent.addGesture()
    }
    func didScroll(scrollView:UIScrollView) {

        guard let parent = self.parent as? PageBlocks else {return}
        guard let brandParent = parent.parent as? ParentBlocksController else {return}
        let yOffSet = scrollView.contentOffset.y
        let contant = brandParent.topContraintViewController.constant
        brandParent.viewController.removeGestureRecognizers()
//        let value:CGFloat = 3
        if contant == 0 && yOffSet > 0 {
            
        }else if yOffSet <= -50 && contant <= 242{
            brandParent.topContraintViewController.constant =  242
            UIView.animate(withDuration: 0.5, animations: {
                brandParent.view.layoutIfNeeded()

            }) { (bool) in
            }
        }else if yOffSet > 0 && contant <= 242{
            brandParent.topContraintViewController.constant = 0
            UIView.animate(withDuration: 0.5, animations: {
                brandParent.view.layoutIfNeeded()
            }) { (bool) in
                
            }
        }
        
    }
    
    func didSelect(row: Int, item: HouseModel) {
        let vc = DetailHouseInfo()
        navigationController?.pushViewController(vc, animated: true)
    }
}


