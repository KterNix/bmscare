//
//  HouseCell.swift
//  neoReal
//
//  Created by pro on 4/18/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class BlockDetailCell: UICollectionViewCell {

    
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var subtitleLBL: UILabel!
    
    @IBOutlet weak var parentView: UIView!
    
    @IBOutlet weak var statusView: UIView!
    
    var house:HouseModel?
//    UIColor.init(hexString: Config.fixingColor),
//    UIColor.init(hexString: Config.brokenColor),
    let colors:[UIColor] = [
    UIColor.init(hexString: Config.absentColor),
    UIColor.init(hexString: Config.emptyColor),
    UIColor.init(hexString: Config.liveInColor)
    ]
    
    func configWith(value: HouseModel) {
        
        self.house = value
        titleLBL.text = value.area.asString
        subtitleLBL.text = value.num.asString
//        switch value.status {
//        case HouseStatus.Sold.rawValue:
//            parentView.backgroundColor = .init(hexString: Config.soldColor)
//        case HouseStatus.Book.rawValue:
//            parentView.backgroundColor = .init(hexString: Config.bookColor)
//        case HouseStatus.Deactive.rawValue:
//            parentView.backgroundColor = .init(hexString: Config.deactiveColor)
//        case HouseStatus.Deposited.rawValue:
//            parentView.backgroundColor = .init(hexString: Config.depositedColor)
//        case HouseStatus.Active.rawValue:
//            parentView.backgroundColor = .init(hexString: Config.activeColor)
//        case HouseStatus.Lock.rawValue:
//            parentView.backgroundColor = .init(hexString: Config.lockedColor)
//        case .none:
//            break
//        case .some(_):
//            break
//        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.parentView.backgroundColor = colors.shuffled().first
        self.parentView.layer.cornerRadius = 5
        self.titleLBL.textColor = .white
        self.parentView.dropShadow()
    }
    
    
    

}

