//
//  BlocksCell.swift
//  neoReal
//
//  Created by pro on 4/18/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class StatusHouse: MasterCollectionViewCell {
    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    typealias T = BlocksCell

    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    //MARK:- SubFunction
    func configure(_ item: BlocksCell, at indexPath: IndexPath) {
        
    }
    override func setupUI() {
        
    }
    override func setupDataSource() {
        
    }
    //MARK:- Outlet Actions
    
}
