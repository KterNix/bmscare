//
//  BlocksDataSource.swift
//  neoReal
//
//  Created by pro on 4/18/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import UIKit

class StatusHouseDataSource: NSObject {
    
    var datasource = [String]()
    
    var didSelectRow: ((_ row: Int, _ item: String) -> Void)?
    
    init(datasource: [String]) {
        self.datasource = datasource
    }
    
    
    
}

extension StatusHouseDataSource: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return
//    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count = datasource.count
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatusHouseCell", for: indexPath) as! StatusHouseCell
        
        let item = datasource[indexPath.row]
        
        cell.configure(item, at: indexPath)
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        let item = datasource[indexPath.row]
//        if let selectedRow = didSelectRow {
//            selectedRow(indexPath.row, item)
//        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sizeWidth = collectionView.frame.width/CGFloat(self.datasource.count) - 6
        let size = CGSize(width: sizeWidth, height: 50)
        return size
    }
    
    
    
//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        if kind == UICollectionElementKindSectionHeader {
//            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier:"SectionView", for: indexPath) as! SectionView
//            let value = Array(datasource.keys)
//            let item = value[indexPath.section]
//            headerView.titleLBL.text = item
//            return headerView
//        }
//        //MARK: - SHOW PLACE VIEW
//        let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier:"SectionView", for: indexPath) as! SectionView
//        footerView.titleLBL.isHidden = true
//        footerView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
//        return footerView
//    }
//
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
//        let size = CGSize(width: UIScreen.main.bounds.width, height: 8)
//        return size
//    }
//

//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        let size = CGSize(width: UIScreen.main.bounds.width, height: 50)
//        return size
//    }

    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    
    
}
