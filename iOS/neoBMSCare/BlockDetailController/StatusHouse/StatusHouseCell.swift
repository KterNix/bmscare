//
//  StatusHouseCell.swift
//  neoReal
//
//  Created by pro on 4/18/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class StatusHouseCell: MasterCollectionViewCell {
    
    //MARK:- Outlet
    @IBOutlet weak var titleText: RecommentLabel!
    
    
    //MARK:- Declare

    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
//    ["Báo vắng", "Trống","Đang ở", "Tân trang","Không bán"]
    //MARK:- SubFunction
    func configure(_ item: String, at indexPath: IndexPath) {
        self.titleText.text = item

        switch item {
        case "Báo vắng":
            self.backgroundColor = UIColor.init(hexString: Config.absentColor)

        case "Trống":
            self.backgroundColor = UIColor.init(hexString: Config.emptyColor)

        case "Tân trang":
            self.backgroundColor = UIColor.init(hexString: Config.fixingColor)

        case "Hỏng":
            self.backgroundColor = UIColor.init(hexString: Config.brokenColor)

        case "Đang ở":
            self.backgroundColor = UIColor.init(hexString: Config.liveInColor)
        default:
            break
        }
    }
    override func setupUI() {
        self.titleText.color = .white
        self.dropShadow()
    }
    func setupDataSource() {
        
    }
    //MARK:- Outlet Actions
    
}
