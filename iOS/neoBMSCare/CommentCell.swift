//
//  CommentCell.swift
//  neoBMSCare
//
//  Created by mac on 7/24/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class CommentCell: MasterTableViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var viewInforUser: UIView!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var nameUser: RecommentLabel!
    @IBOutlet weak var positionJob: NormalLabel!
    @IBOutlet weak var timeAgo: NormalLabel!
    @IBOutlet weak var collectionViewImage: UICollectionView!
    @IBOutlet weak var contentComment: NormalLabel!
    @IBOutlet weak var parentView: UIView!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layoutAfterLayoutSubviews()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    override func setupUI() {
        timeAgo.color = .white
        positionJob.color = .white
        nameUser.color = .white
        viewInforUser.backgroundColor = UIColor.lightGray.withAlphaComponent(0.7)
        parentView.layer.cornerRadius = 5
        parentView.dropShadow()
        
    }
    override func setupDataSource() {
        
    }
    //MARK:- SubFunction
    func layoutAfterLayoutSubviews() {
        layoutIfNeeded()
        self.avatar.layer.cornerRadius =  self.avatar.height/2
        self.avatar.layer.masksToBounds = true
        self.avatar.layer.borderColor = UIColor.mainColor().cgColor
        self.avatar.layer.borderWidth = 0.5
        self.viewInforUser.layer.cornerRadius = self.viewInforUser.height/2
    }
    
    //MARK:- Outlet Actions
    
}
