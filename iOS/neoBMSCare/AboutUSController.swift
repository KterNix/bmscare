//
//  AboutUSController.swift
//  ApartmentManager
//
//  Created by pro on 7/28/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import Eureka

class AboutUSController: MasterFormController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    
    private func setupUI() {
        self.title = "Về chúng tôi"
        setBackButton()
        
        form +++
            Section() {
                var header = HeaderFooterView<HeaderView>(.class)
                header.onSetupView = { view, _ in
                    view.imageProfile.image = UIImage(named: "logoNEO")
                }
                $0.header = header
        }
        +++ Section("Phiên bản phần mềm")
            <<< LabelRow() {
                $0.title = "Phiên bản"
                $0.value = "1.0"
        }
     
        +++ Section("Hỗ trợ kỹ thuật")
            <<< ButtonRow() {
                $0.title = "Công ty TNHH Giải Pháp Bàn Tay Số"
            }
            <<< LabelRow() {
                $0.title = "Địa chỉ"
                $0.value = "47/25 Trần Quốc Toản Phường 8 Quận 3"
                $0.cell.detailTextLabel?.numberOfLines = 0
                $0.cell.detailTextLabel?.lineBreakMode = .byWordWrapping
                
        }
            <<< LabelRow() {
                $0.cell.detailTextLabel?.numberOfLines = 0
                $0.cell.detailTextLabel?.lineBreakMode = .byWordWrapping
                $0.title = "Phone"
                $0.value = "(+84-028) 38 209 799 \n (+84) 0909 25 47 88"
                
        }
            <<< LabelRow() {
                $0.title = "Email"
                $0.value = "info@bantayso.com"
        }
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}




class HeaderView: UIView {
    
    let imageProfile = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.white
        addSubview(imageProfile)
        let sizeWidth = UIScreen.main.bounds.size.width
        
        imageProfile.frame = CGRect(x: sizeWidth/2 - 100, y: 8, width: 200, height: 200)
        self.imageProfile.isUserInteractionEnabled = true
        self.imageProfile.contentMode = .scaleAspectFit
        self.frame = CGRect(x: 0, y: 0, width: sizeWidth, height: 220)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}










