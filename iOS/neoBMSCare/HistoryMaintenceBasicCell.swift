//
//  HistoryBasicCell.swift
//  neoBMSCare
//
//  Created by mac on 7/26/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class HistoryMaintenceBasicCell: MasterCollectionViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var lastComment: NormalLabel!
    @IBOutlet weak var timeStart: UILabel!
    @IBOutlet weak var timeDone: UILabel!
    @IBOutlet weak var status: StatusLabel!
    @IBOutlet weak var avatarLastCommentator: UIImageView!
    
    
    //MARK:- Declare
    var colors:[UIColor] = [
        UIColor.init(hexString: Config.absentColor),
        UIColor.init(hexString: Config.emptyColor),
        UIColor.init(hexString: Config.fixingColor),
        UIColor.init(hexString: Config.brokenColor),
        UIColor.init(hexString: Config.liveInColor)
    ]
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutIfNeeded()
        avatarLastCommentator.makeCircle = true
        avatarLastCommentator.borderColor = .mainColor()
        avatarLastCommentator.borderWidth = 0.5
    }
    override func setupUI() {
        self.dropShadow()
        let textForTimeStart = "Bắt đầu: 30/02/2017"
        let stringStart = NSMutableAttributedString(string: textForTimeStart, attributes: [
            .font : UIFont.recommentFont
            ])
        stringStart.addAttribute(NSAttributedStringKey.font, value: UIFont.textFont, range: NSRange(location: 0, length: 8))
        stringStart.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.lightGray, range: NSRange(location: 0, length: 8))
        timeStart.attributedText = stringStart
        
        let textForTimeDone = "Kết thúc: 10/10/2017"
        let stringDone = NSMutableAttributedString(string: textForTimeDone, attributes: [
            .font : UIFont.recommentFont
            
            ])
        stringDone.addAttribute(NSAttributedStringKey.font, value: UIFont.textFont, range: NSRange(location: 0, length: 9))
        stringDone.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.lightGray, range: NSRange(location: 0, length: 9))
        timeDone.attributedText = stringDone
        self.status.bgColor = colors.shuffle().first
    }
    func setupDataSource() {
        
    }
    //MARK:- SubFunction
    
    
    //MARK:- Outlet Actions

}
