//
//  Image.swift
//  neoBMSCare
//
//  Created by mac on 7/19/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
extension UIImage {
    func resize(width : Int , height : Int) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(CGSize.init(width: width, height: height), false, 0)
        self.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
}
