//
//  CellIcon.swift
//  neoBMSCare
//
//  Created by pro on 7/28/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import Eureka

final class CellIconRow: Row<CellIcon>, RowType {
    required init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<CellIcon>(nibName: "CellIcon")
    }
}

struct ItemCellIcon: Equatable {
    var title: String
    var icon: String
}
func ==(lhs: ItemCellIcon, rhs: ItemCellIcon) -> Bool {
    return lhs.title == rhs.title
}





class CellIcon: Cell<ItemCellIcon>, CellType {

    
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    
    
    required init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func setup() {
        super.setup()
        // we do not want our cell to be selected in this case. If you use such a cell in a list then you might want to change this.
        selectionStyle = .none
        
        // configure our profile picture imageView
        
        
        // define fonts for our labels
        title.font = .systemFont(ofSize: 15)
        
        // set the textColor for our labels
        
        
        // specify the desired height for our cell
        height = { return CGFloat(44) }()
        
        // set a light background color for our cell
        backgroundColor = UIColor.white
    }
    
    
    override func update() {
        super.update()
        
        // we do not want to show the default UITableViewCell's textLabel
        textLabel?.text = nil
        
        // get the value from our row
        guard let user = row.value else { return }
        // set the texts to the labels
        title.text = user.title
        iconView.tintColor = UIColor.lightGray
        iconView.image = UIImage(named: "\(user.icon)")?.withRenderingMode(.alwaysTemplate)
    }
}
