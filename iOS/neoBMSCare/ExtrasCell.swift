//
//  ExtrasCell.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit

class ExtrasCell: UITableViewCell {

    
    
    @IBOutlet weak var titleExtras: UILabel!
    
    
    @IBOutlet weak var extrasImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
