//
//  ParentDetailInboxController.swift
//  neoBMSCare
//
//  Created by mac on 8/9/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ParentDetailInboxController: MasterViewController {

    
    //MARK:- Outlet
    @IBOutlet weak var viewDetailInbox: UIView!
    @IBOutlet weak var filedView: FieldChatView!
    @IBOutlet weak var bottomContraintField: NSLayoutConstraint!

    
    //MARK:- Declare
    let vc = DetailInboxController()

    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func setupUI() {
        setBackButton()
        self.title = "Nix"
        self.filedView.backgroundColor = .white
        tabBarController?.tabBar.isHidden = true
        addChildViewController(vc)
        vc.view.frame = viewDetailInbox.bounds
        viewDetailInbox.addSubview(vc.view)
        vc.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        vc.didMove(toParentViewController: self)
        vc.setupUI()
        NotificationCenter.default.addObserver(self, selector: #selector(haldeKeyBoard), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(haldeKeyBoard), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        filedView.dropShadow()
        
    }
    override func setupDataSource() {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    //MARK:- SubFunction
    
    @objc func haldeKeyBoard(notification:NSNotification)  {
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTap))

        guard let userInfo = notification.userInfo else {return}
        let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
        let noficationName = notification.name == NSNotification.Name.UIKeyboardWillShow
        self.bottomContraintField.constant = noficationName ? (keyboardFrame?.height)! : 0
        noficationName ? viewDetailInbox.addGestureRecognizer(tap) : viewDetailInbox.removeGestureRecognizers()

        UIView.animate(withDuration: 0.1, animations: {
            self.view.layoutIfNeeded()
            
        }) { (bool) in
            UIView.animate(withDuration: 0.2, animations: {
                let lastSection = self.vc.tableView.numberOfSections - 1
                let lastRow = self.vc.tableView.numberOfRows(inSection: lastSection) - 1
                self.vc.tableView.scrollToRow(at: IndexPath(item: lastRow, section: lastSection), at: .bottom, animated: false)
            })
            
        }
    }
    @objc func didTap() {
        self.view.endEditing(true)
    }
    //MARK:- Outlet Actions

}
