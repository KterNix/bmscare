//
//  HistoryController.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit

class HistoryController: UIViewController {

    
    
    
    
    
    @IBOutlet weak var myTableView: UITableView!
    
    var historyBills = [BillModel]() {
        didSet {
            self.myTableView.reloadData()
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
        // Do any additional setup after loading the view.
    }

    private func setupUI() {
        self.title = "Lịch sử"
        setBackButton()
    }
    
    private func setupDataSource() {
        var bill = BillModel()
        bill.date = "12/2012"
        bill.serviceFee = 20000
        bill.waterPrice = 10000
        bill.electricPrice = 100000
        bill.billStatus = false
        bill.totalPrice = bill.electricPrice + bill.serviceFee + bill.waterPrice
        self.historyBills.append(bill)
        
        bill = BillModel()
        bill.date = "12/2012"
        bill.serviceFee = 50000
        bill.waterPrice = 10000
        bill.electricPrice = 100000
        bill.billStatus = false
        bill.totalPrice = bill.electricPrice + bill.serviceFee + bill.waterPrice
        self.historyBills.append(bill)
        
        bill = BillModel()
        bill.date = "12/2012"
        bill.serviceFee = 10000
        bill.waterPrice = 10000
        bill.electricPrice = 100000
        bill.billStatus = false
        bill.totalPrice = bill.electricPrice + bill.serviceFee + bill.waterPrice
        self.historyBills.append(bill)
        
        bill = BillModel()
        bill.date = "12/2012"
        bill.serviceFee = 60000
        bill.waterPrice = 10000
        bill.electricPrice = 100000
        bill.billStatus = true
        bill.totalPrice = bill.electricPrice + bill.serviceFee + bill.waterPrice
        self.historyBills.append(bill)
        
        bill = BillModel()
        bill.date = "12/2012"
        bill.serviceFee = 70000
        bill.waterPrice = 10000
        bill.electricPrice = 100000
        bill.billStatus = true
        bill.totalPrice = bill.electricPrice + bill.serviceFee + bill.waterPrice
        self.historyBills.append(bill)
        
        bill = BillModel()
        bill.date = "12/2012"
        bill.serviceFee = 90000
        bill.waterPrice = 10000
        bill.electricPrice = 100000
        bill.billStatus = false
        bill.totalPrice = bill.electricPrice + bill.serviceFee + bill.waterPrice
        self.historyBills.append(bill)
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension HistoryController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyBills.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyCell", for: indexPath) as! HistoryCell
        cell.configWithValue(bill: self.historyBills[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lineColor = UIColor.init(red: 228, green: 227, blue: 229)
        if indexPath.row == 0 {
            let line = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 1))
            line.backgroundColor = lineColor
            cell.addSubview(line)
            cell.bringSubview(toFront: line)
        }else if indexPath.row == self.historyBills.count - 1 {
            let line = UIView(frame: CGRect(x: 8, y: 0, width: self.view.frame.size.width, height: 1))
            line.backgroundColor = lineColor
            cell.addSubview(line)
            cell.bringSubview(toFront: line)
            
            let lineBottom = UIView(frame: CGRect(x: 0, y: 59, width: self.view.frame.size.width, height: 1))
            lineBottom.backgroundColor = lineColor
            cell.addSubview(lineBottom)
            cell.bringSubview(toFront: lineBottom)
            
        }else{
            let line = UIView(frame: CGRect(x: 8, y: 0, width: self.view.frame.size.width, height: 1))
            line.backgroundColor = lineColor
            cell.addSubview(line)
            cell.bringSubview(toFront: line)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.myTableView.deselectRow(at: indexPath, animated: true)
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "HistoryDetailController") as! HistoryDetailController
        vc.billDetails = self.historyBills[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}





















