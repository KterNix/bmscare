//
//  Utils.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage

open class Utils {
    static let share = Utils()
    static let userDefault = UserDefaults.standard
    static var currentBlocks:[BlockModel]?
    /// Khai báo biến để dùng Storage của Firebase
    static let storage = Storage.storage()
    //Link Storegae của project
    static let storageRef = Storage.storage().reference(forURL: "gs://apartmentmanager-71e0b.appspot.com")
    var isDefaultBar = false
    //Khai báo biến ref để truy cập datbase json của Firebase
//    static let ref = Database.database().reference()
    
//    static let avatarDefault = "https://firebasestorage.googleapis.com/v0/b/clickfoodbts.appspot.com/o/avatars%2FavatarDefault.png?alt=media&token=17ce3df8-c2b5-4b2a-bc08-1de2f1c56ba5"
    
    open static func getUserDefaults(withKey:String) -> Bool {
        return (userDefault.value(forKey: withKey) != nil) ? true:false
    }
    
    open static func checkLogedIn() -> Bool {
        if getUserDefaults(withKey: "loggedUserId") {
            return true
        } else {
            return false
        }
    }
    open static var isDefaultBarStatus:Bool?{
        didSet{
            if isDefaultBarStatus! {
                UIApplication.shared.statusBarStyle = .default
            }else {
                UIApplication.shared.statusBarStyle = .lightContent
                
            }
        }
        
    }
    open static func getUserID() -> String {
        return userDefault.value(forKey: "loggedUserId") as! String
    }
    
    open static func calculateContentSize(scrollView: UIScrollView) -> CGSize {
        var topPoint = CGFloat()
        var height = CGFloat()
        
        for subview in scrollView.subviews {
            if subview.frame.origin.y > topPoint {
                topPoint = subview.frame.origin.y
                height = subview.frame.size.height
            }
        }
        return CGSize(width: scrollView.frame.size.width, height: height + topPoint)
    }
    
    open static func showAlertWithCompletion(title: String, message: String,viewController: UIViewController,completion completionHandler: (() -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
            completionHandler?()
        }
        alert.addAction(action)
        viewController.present(alert, animated: true, completion: nil)
    }
    
    open static func alertWithAction(title: String?,cancelTitle:String = "OK", message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?],viewController: UIViewController,style: UIAlertControllerStyle) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        let cancel = UIAlertAction(title: cancelTitle, style: .cancel, handler: nil)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        alert.addAction(cancel)
        DispatchQueue.main.async {
            viewController.present(alert, animated: true, completion: nil)
        }
    }
    
    // get current time
    open static func getCurrentDateTime() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = Date()
        return formatter.string(from: date)
    }
    
    open static func getDayOfWeek(today:String)->Int {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let todayDate = formatter.date(from: today)!
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let myComponents = myCalendar.components(.weekday, from: todayDate)
        let weekDay = myComponents.weekday
        return weekDay!
    }
    
    open static func StringToDate(_ dateString: String, stringFormat: String = "yyyy-MM-dd HH:mm:ss") -> Date {
        if(dateString != "") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = stringFormat //"yyyy-MM-dd HH:mm:ss"
            if let d = dateFormatter.date( from: dateString ) {
                return d
            }
        }
        return Date()
    }
    open static func StringToDateFormat(_ dateString: String, stringFormat: String = "HH:mm") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let date:Date = dateFormatter.date( from: dateString )!
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: date)
    }
    
    open static func StringDateFormat(_ dateString: String, stringFormat: String = "dd/MM/yyyy HH:mm") -> String {
        let dateFormatter =  DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date:Date = dateFormatter.date( from: dateString )!
        dateFormatter.dateFormat = stringFormat
        return dateFormatter.string(from: date)
    }
    open static func getDayName(_ day: Int) ->String {
        switch day {
        case 1:
            return "Thứ 2"
        case 2:
            return "Thứ 3"
        case 3:
            return "Thứ 4"
        case 4:
            return "Thứ 5"
        case 5:
            return "Thứ 6"
        case 6:
            return "Thứ 7"
        case 7:
            return "Chủ nhật"
        default:
            return ""
        }
    }
    open static func timeAgoSince(_ date: Date, lang: String = "vn") -> String {
        
        let calendar = Calendar.current
        let now = Date()
        let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
        let components = (calendar as NSCalendar).components(unitFlags, from: date, to: now, options: [])
        var prefix:String = ""
        
        
        if components.year! >= 2 {
            prefix = (components.year?.description)! as String
            
            return "\(prefix) năm trước"
        }
        
        if components.year! >= 1 {
            return "1 năm trước"
        }
        
        if components.month! >= 2 {
            prefix = (components.month?.description)! as String
            return "\(prefix) tháng trước"
        }
        
        if components.month! >= 1 {
            return "1 tháng trước"
        }
        
        if components.weekOfYear! >= 2 {
            prefix = (components.weekOfYear?.description)! as String
            return "\(prefix) tuần trước"
        }
        
        if components.weekOfYear! >= 1 {
            return "1 tuần trước"
        }
        
        if components.day! >= 2 {
            prefix = (components.day?.description)! as String
            return prefix + " ngày trước"
        }
        
        if components.day! >= 1 {
            return "Hôm qua"
        }
        
        if components.hour! >= 2 {
            prefix = (components.hour?.description)! as String
            return "\(prefix) giờ trước"
        }
        
        if components.hour! >= 1 {
            return "1 giờ trước"
        }
        
        if components.minute! >= 2 {
            prefix = (components.minute?.description)! as String
            return prefix + " phút trước"
        }
        
        if components.minute! >= 1 {
            return "1 phút trước"
        }
        
        if components.second! >= 3 {
            prefix = (components.second?.description)! as String
            return "\(prefix) giây trước"
        }
        
        return "Vừa xong"
        
    }
    
    open static func openUrl(scheme: String) {
        if let url = URL(string: scheme) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                                            print("Open \(scheme): \(success)")
                })
            } else {
                let success = UIApplication.shared.openURL(url)
                print("Open \(scheme): \(success)")
            }
        }
    }
    func changeRootView(_ vc : UIViewController,completion_: (() -> (Void))?){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let a = appDelegate.window {
            let snapshot:UIView = a.snapshotView(afterScreenUpdates: true)!
            vc.view.addSubview(snapshot)
            a.rootViewController = vc
            a.makeKeyAndVisible()
            UIView.animate(withDuration: 0.3, animations: {() in
                snapshot.layer.opacity = 0;
                snapshot.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5)
            }, completion: {
                (value: Bool) in
                snapshot.removeFromSuperview()
                completion_?()
            })
        }
    }
    
}



