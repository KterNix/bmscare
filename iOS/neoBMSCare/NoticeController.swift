//
//  NoticeController.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit


class NoticeController: MasterViewController {

    
    
    
    @IBOutlet weak var myTableView: UITableView!
    
    
    
    var noticeList = [Notice]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myTableView.delegate = self
        self.myTableView.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func setupUI() {
        self.title = "Thông báo"
        setBackButton()
        if (self.tabBarController as! MasterController).accountType == .Manager {
            createRightBarItem(title: "+", selector: #selector(self.didPressCreateNew))

        }
    }
    
    override func setupDataSource() {
        let n = Notice(title: "Bạn đã đăng ký tiện ích Trường mẫu giáo", isUserPost: true, date: "2017-02-23 09:10:11")
        let b = Notice(title: "Bạn đã đăng ký tiện ích Hồ Bơi", isUserPost: true, date: "2017-05-23 09:10:11")
        let c = Notice(title: "Bạn đã đăng ký tiện ích Khu vui chơi trẻ em", isUserPost: true, date: "2017-07-23 09:10:11")
        self.noticeList.append(n)
        self.noticeList.append(b)
        self.noticeList.append(c)
        
    }
    @objc func didPressCreateNew() {
        let vc = DetailNoticeController()
        vc.isCreateNew = true
        self.navigationController?.pushViewController(vc, animated: true)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension NoticeController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return noticeList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "noticeCell", for: indexPath) as! NoticeCell
        cell.configWithValue(n: noticeList[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailNoticeController()
        self.navigationController?.pushViewController(vc, animated: true)
        self.myTableView.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
