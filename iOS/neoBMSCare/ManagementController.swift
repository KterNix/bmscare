//
//  ManagementController.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import Eureka

class ManagementController: MasterFormController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }

    
    private func setupUI() {
        self.title = "Ban quản lý"
        setBackButton()
        
        LabelRow.defaultCellSetup = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 15)
            cell.tintColor = UIColor.mainFontColor()
        }
        ButtonRow.defaultCellSetup = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
            cell.tintColor = UIColor.mainFontColor()
        }
        
        
        
        form +++
            Section("Giới thiệu")
            <<< LabelRow() {
                $0.title = "Công ty TNHH Xây Dựng & Đầu Tư Thương Mại BVI"
                $0.cell.textLabel?.numberOfLines = 0
                $0.cellStyle = .value1
            }
            <<< LabelRow() {
                $0.title = "Địa chỉ"
                $0.value = "178 Trần Não, Phường Bình An, Quận 2, TP.Hồ Chí Minh"
                $0.cell.detailTextLabel?.numberOfLines = 0
            }
            <<< LabelRow() {
                $0.title = "Số điện thoại"
                $0.value = "(028) 99 333 178\n0988 142 178"
                $0.cell.detailTextLabel?.numberOfLines = 0
            }
            <<< LabelRow() {
                $0.title = "Email"
                $0.value = "cskh_imperia@gmail.com"
                $0.cell.detailTextLabel?.numberOfLines = 0
            }
            
        +++ Section("Quản lý")
            <<< ButtonRow(){
                $0.title = "Nhân sự"
                $0.cellStyle = .value1
                }.onCellSelection({ (cell, row) in
                    let vc = HumanResourceController(nibName: "HumanResourceController", bundle: nil)
                    vc.isPopup = false
                    self.navigationController?.pushViewController(vc, animated: true)
                })
            <<< ButtonRow(){
                $0.title = "Tài sản"
                $0.cellStyle = .value1
                }.onCellSelection({ (cell, row) in
                    let vcType = AssetController.self
                    let vc = vcType.getViewController()
                    self.navigationController?.pushViewController(vc, animated: true)
                })
           
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
