//
//  RegExtrasController.swift
//  ApartmentManager
//
//  Created by pro on 7/27/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import Eureka




class RegExtrasController: FormViewController {

    
    var itemExtras = [String]()
    
    var getItems = [String:AnyObject]()
    var postNotice = Notice()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    private func setupUI() {
        self.title = "Đăng ký Tiện ích"
        setBackButton()
        
        form +++
            Section("Ngày giờ tham gia")
            <<< DateTimeInlineRow() {
                $0.title = "Ngày giờ"
                $0.value = Date()
                self.getItems["date"] = $0.value as AnyObject
                self.postNotice.date = ($0.value?.description)!
                }.onChange({ [unowned self] (dates) in
                    guard let date = dates.value else { return }
                    self.getItems["date"] = date as AnyObject
                    self.postNotice.date = date.description
                })
            +++ Section("Thông tin tiện ích")
            <<< TextRow(){
                $0.title = "Tiện ích đã chọn"
                $0.value = "Hồ bơi"
                }.onChange({ [unowned self] (extras) in
                    guard let extra = extras.value else { return }
                    self.getItems["extras"] = extra as AnyObject
                    self.postNotice.title = extra
                })
            +++ Section("Ghi chú")
            <<< TextAreaRow() {
                $0.placeholder = "Cho chúng tôi biết bạn cần thêm gì?"
                }.onChange({ [unowned self] (text) in
                    guard let tex = text.value else { return }
                    self.getItems["text"] = tex as AnyObject
                })
            
            +++ Section(){section in
                var header = HeaderFooterView<UIView>(.callback({
                    let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
                    let button = StyleGhost(frame: CGRect(x: 0, y: 0, width: self.view.width * 0.8, height: 40))
                    button.text = "Đăng ký"
                    button.center = view.center
                    button.tag = 2
                    view.addSubview(button)
                    return view
                }))
                header.height = {50}
                header.onSetupView = {view, _ in
                    guard let button = view.viewWithTag(2) as? UIButton else {return}
                    button.addTarget(self, action: #selector(self.didPressRegister), for: .touchUpInside)
                }
                section.header = header
        }
        
        
        
    }
    @objc func didPressRegister(){
        Utils.showAlertWithCompletion(title: "Đã đăng ký", message: "Bạn đã đăng kí tiện ích \(String(describing: self.getItems["extras"]!))", viewController: self, completion: {
            self.postNotice.isUserPost = true
            self.postNotificationWithKey("postNotice", userInfo: ["notice":self.postNotice])
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
