//
//  SubmitComment.swift
//  neoBMSCare
//
//  Created by mac on 8/3/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class SubmitComment: UIView {

    
    //MARK:- Outlet
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btnSend: StyleGhost!
    @IBOutlet weak var titleText: UILabel!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutIfNeeded()
        btnSend.setupUI()
    }
    //MARK:- SubFunction
    func setupUI() {
        self.textView.borderWidth = 0.5
        self.textView.borderColor = .lightGray
        self.textView.cornerRadius = 5
        self.titleText.textColor = UIColor.init(hexString: "#8b8b8f")
        
    }
    func setupDataSource(textTitle :String) {
        titleText.text = textTitle.uppercased()
        
    }
    
    //MARK:- Outlet Actions
    @IBAction func abtnSend(_ sender: Any) {
    }
    
}
