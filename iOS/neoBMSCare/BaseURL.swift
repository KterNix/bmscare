//
//  BaseURL.swift
//  neoReal
//
//  Created by pro on 4/20/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation


class BaseURL {
    
    static let url = "http://clickfood.vn/neore/public"
    static let URL_SOCKET = "http://clickfood.vn:8080"
    static let URL_LOGIN = url + "/token"
    static let URL_REGISTER = url + "/register"
    static let URL_FORGOTPASS = url + "/forgotpassword"
    static let URL_VERIFY = url + "/verify"
    static let URL_GET_ZONES = url + "/zonesPublic"
    static let URL_GET_MEMBERS = url + "/members"
    static let URL_GET_ROLES = url + "/roles"
    static let URL_GET_PROFILE = url + "/profile"
    static let URL_GET_TYPE = url + "/type="
    static let URL_GET_TYPES = url + "/types"
    static let URL_GET_AGENCIES = url + "/agenciesPublic"
    static let URL_GET_MY_AGENCIES = url + "/myAgencies"
    static let URL_GET_PROJECT = url + "/projectPublic/"
    static let URL_GET_PROJECTS = url + "/projectPublic" //?zone=
    static let URL_GET_INVESTORS = url + "/investorsPublic"
    static let URL_GET_INVESTOR = url + "/investor/"
    static let URL_GET_CUSTOMERS = url + "/customers/"
    static let URL_GET_CUSTOMER = url + "/customer/"
    static let URL_GET_EVENTS = url + "/eventsPublic"
    static let URL_GET_EVENT = url + "/eventPublic/"
    static let URL_GET_HOUSE = url + "/housePublic/"
    static let URL_GET_HOUSES = url + "/housesPublic"
    static let URL_GET_AGENCY_MEMBERS = url + "/agencyMember/"
    static let URL_GET_INVESTOR_MEMBERS = url + "/investorMember/"
    static let URL_GET_PROMOTIONS = url + "/blogsPublic?category="
    static let URL_COMMENTS = url + "/comment/"
    static let URL_TRANSACTION = url + "/transaction/" // Dat coc
    static let URL_BOOKING = url + "/booking/" // Dat chỗ
    static let URL_SEARCH_MEMBER = url + "/searchMember/"
    static let URL_SEARCH_HOUSES = url + "/houses"
    static let URL_POST_CONTACT = url + "/contact/"
    static let URL_GET_EVENT_HOUSES = url + "/eventHouse/"
    static let URL_PUT_USER = url + "/member/"

}
