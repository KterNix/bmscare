//
//  LoginController.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
import Firebase


class LoginController: UIViewController {

    
    @IBOutlet weak var viewFB: UIView!
    @IBOutlet weak var viewGG: UIView!
    
    @IBOutlet weak var userNameTxt: UITextField!
    @IBOutlet weak var passordTxt: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var buildingLogo: UIImageView!
    
    
    @IBOutlet weak var ggImage: UIImageView!
    
    let loginManager = FBSDKLoginManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        GIDSignIn.sharedInstance().clientID = "616292141734-fl1po24prfqp59dcvm52h2vnmuon5iqj.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        userNameTxt.delegate = self
        passordTxt.delegate = self
        // Do any additional setup after loading the view.
    }

    private func setupUI() {
        viewFB.layer.cornerRadius = 3
        viewFB.layer.masksToBounds = true
        viewFB.layer.borderWidth = 1
        viewFB.layer.borderColor = UIColor.white.cgColor
        viewGG.layer.cornerRadius = 3
        viewGG.layer.masksToBounds = true
        viewGG.layer.borderWidth = 1
        viewGG.layer.borderColor = UIColor.white.cgColor
        ggImage.tintColor = UIColor.white
        ggImage.image = UIImage(named: "ggIcon")?.withRenderingMode(.alwaysTemplate)
        loginButton.layer.cornerRadius = 3
        loginButton.layer.masksToBounds = true
        loginButton.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        userNameTxt.attributedPlaceholder = NSAttributedString(string: " Tên đăng nhập",
                                                              attributes: [NSAttributedStringKey.foregroundColor: UIColor.darkGray])
        userNameTxt.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        userNameTxt.layer.cornerRadius = 3
        userNameTxt.layer.masksToBounds = true
        
        passordTxt.attributedPlaceholder = NSAttributedString(string: " Mật khẩu",
                                                              attributes: [NSAttributedStringKey.foregroundColor: UIColor.darkGray])
        passordTxt.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        passordTxt.layer.cornerRadius = 3
        passordTxt.layer.masksToBounds = true
        
        
        buildingLogo.image = UIImage(named: "imperialLogo")
    }
    
    
    
    
    @IBAction func loginAction(_ sender: Any) {
        let userName = userNameTxt.text
        let passWord = passordTxt.text
        if (userName?.isEmpty)! {
            Utils.showAlertWithCompletion(title: "Lỗi", message: "Vui lòng điền tên đăng nhập", viewController: self, completion: nil)
            return
        }
        if (passWord?.isEmpty)! {
            Utils.showAlertWithCompletion(title: "Lỗi", message: "Vui lòng điền tên mật khẩu", viewController: self, completion: nil)
            return
        }
    }
    
    
    
    
    @IBAction func loginFacebookAction(_ sender: Any) {
        var email:String = ""
        loginManager.logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, error) in
            if error == nil {
                let accessToken = FBSDKAccessToken.current()
                
                guard let accessTokenString = accessToken?.tokenString  else { return }
                // Đăng nhập thành công thì lưu token vào UserDefault
                let credentials = FacebookAuthProvider.credential(withAccessToken: accessTokenString)
                
                
                FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email"]).start { (connection, result, err) in
                    if err != nil {
                        Utils.showAlertWithCompletion(title: "Lỗi", message: (err?.localizedDescription)!, viewController: self, completion: nil)
                        return
                    }
                    let results:[String:AnyObject] = result as! [String:AnyObject]
                    email = results["email"]! as! String
//                    self.signInWithCredentials(credentials: credentials, email: email)
                }
            }
        }
    }
    
    
    @IBAction func loginGoogleAction(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    func doLogin() {
        
    }
 
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension LoginController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let tag = textField.tag
        switch tag {
        case self.userNameTxt.tag:
            self.passordTxt.becomeFirstResponder()
        case self.passordTxt.tag:
            dismissKeyboard()
            self.doLogin()
        default:
            break
        }
        return true
    }
}


extension LoginController : GIDSignInDelegate, GIDSignInUIDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil {
            let email = user.profile.email
            let accessToken = user.authentication.accessToken
            let idToken = user.authentication.idToken
//            let credentials = GoogleAuthProvider.credential(withIDToken: idToken!, accessToken: accessToken!)
//            signInWithCredentials(credentials: credentials, email: email!)
            
        }else{
            print(error.localizedDescription)
        }
    }
}









