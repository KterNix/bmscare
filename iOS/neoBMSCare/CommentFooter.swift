//
//  CommentFooter.swift
//  neoBMSCare
//
//  Created by mac on 7/24/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
private let idCell = "CommentCell"
class CommentFooter: UIView {

    
    //MARK:- Outlet
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        setupDataSource()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        indicator.color = .mainColor()
        indicator.hidesWhenStopped = true
        indicator.startAnimating()
        indicator.sizeToFit()
    }
    func setupDataSource() {
        
        table.register(UINib(nibName: idCell, bundle: nil), forCellReuseIdentifier: idCell)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.indicator.stopAnimating()
            self.table.delegate = self
            self.table.dataSource = self
            self.table.reloadData()
        }
        
    }
    
    //MARK:- Outlet Actions

}
extension  CommentFooter : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: idCell)
        as! CommentCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}
