//
//  InboxController.swift
//  neoBMSCare
//
//  Created by mac on 7/26/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
private let idCell = "InboxCell"
class InboxController: MasterViewController {

    
    //MARK:- Outlet
    @IBOutlet weak var collectionInbox: UICollectionView!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func setupUI() {
        self.title = "Tin nhắn"
        self.extendedLayoutIncludesOpaqueBars = true
        self.createRightBarItem(title: "+", selector: #selector(self.didPressNewMessage))
    }
    override func setupDataSource() {
        collectionInbox.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        collectionInbox.delegate = self
        collectionInbox.dataSource = self
    }
    //MARK:- SubFunction
    
    @objc func didPressNewMessage() {
        let vc = CreateNewMessageController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK:- Outlet Actions
    

}
extension InboxController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = ParentDetailInboxController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 10, height: 70)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}

