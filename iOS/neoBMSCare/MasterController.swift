//
//  MasterController.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
//import AKSideMenu

class MasterController: UITabBarController {

    
    
    
    
    
    
    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    var accountType :AccountType = .Manager
    
    //MARK:- Override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
        switch accountType {
        case .Manager:
            self.createVCForManager()

        case .Supplier:
            
            self.createVCForSupplier()
        case .User:
            
            self.createVCForCustomer()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.view.backgroundColor = UIColor.white
        self.tabBar.tintColor = UIColor.mainColor()
//        if #available(iOS 10.0, *) {
//            self.tabBar.unselectedItemTintColor = UIColor.black
//        } else {
//            // Fallback on earlier versions
//        }
        
    }
    func setupDataSource() {
        
    }
    func createVCForManager() {
        self.viewControllers = [dashboardController, bookingController, diagramController,inboxController,leftController]
        //        let
    }
    func createVCForCustomer() {
        self.viewControllers = [dashboardCustomer,contactCutommer,inboxController,leftController]
    }
    func createVCForSupplier() {
        
    }
        //MARK: Create Controller
    lazy var dashboardController:UIViewController = {
        let vc = self.createController(viewController: DashboardController.self, name: "dashboard")
        return vc
    }()
    lazy var bookingController:UIViewController = {
        let vc = self.createController(viewController: BookingController.self, name: "meeting")
        vc.title = "Dịch vụ"
        return vc
    }()
    lazy var inboxController:UIViewController = {
        let vc = InboxController()
        let navi = UINavigationController(rootViewController: vc)
        navi.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "message"), selectedImage: nil)
        navi.title = "Tin nhắn"
        return navi
    }()
    lazy var diagramController:UIViewController = {
        let vc = ParentBlocksController()
        let navi = UINavigationController(rootViewController: vc)
        navi.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "houses"), tag: 0)
        navi.title = "Toà nhà"
        return navi
    }()
    lazy var leftController:UIViewController = {
        let vc = self.createController(viewController: LeftController.self, name: "profile") as! UINavigationController
        (vc.viewControllers[0] as! LeftController).accountType = accountType
        vc.title = "Trang cá nhân"
        return vc
    }()
    lazy var dashboardCustomer:UIViewController = {
        let vc = DashboardCustomer()
        let navi = UINavigationController(rootViewController: vc)
        navi.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "dashboard"), selectedImage: nil)
        navi.title = "Trang chủ"
        return navi
    }()
    lazy var houseCutommer:UIViewController = {
        let vc = DetailHouseInfo()
        let navi = UINavigationController(rootViewController: vc)
        navi.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "house"), selectedImage: nil)
        navi.title = "Nhà bạn"
        return navi
    }()
    lazy var contactCutommer:UIViewController = {
        let vc = ConnectToManager(nibName: "ConnectToManager", bundle: nil)
        let navi = UINavigationController(rootViewController: vc)
        navi.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "contact"), selectedImage: nil)
        navi.title = "Dịch vụ"
        return navi
    }()
    lazy var bookingServiceCutommer:UIViewController = {
        let vc = createController(viewController: ExtrasController.self, name: "service")
//        let navi = UINavigationController(rootViewController: vc)
//        navi.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "service"), selectedImage: nil)
        vc.title = "Tiện ích"
        return vc
    }()
    lazy var shopCutommer:UIViewController = {
        let vc = ShopController()
        let navi = UINavigationController(rootViewController: vc)
        navi.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "cart"), selectedImage: nil)
        navi.title = "Shop"
        return navi
    }()
    func createController(viewController:UIViewController.Type,name image:String) -> UIViewController {
        let vc = viewController.getViewController()
        let navi = UINavigationController(rootViewController: vc)
        navi.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: image), tag: 0)
        return navi
    }
    
    //MARK:- Outlet Actions
}
