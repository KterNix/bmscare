//
//  NoteDashboardCell.swift
//  neoBMSCare
//
//  Created by mac on 7/23/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
private let idCell = "NoteCell"
class NoteDashboardCell: MasterTableViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var tableNote: UITableView!
    @IBOutlet weak var heightTable: NSLayoutConstraint!
    
    
    //MARK:- Declare
    var notes = [NoteModel]()
    var parentTable = UITableView()
    var heightSave:CGFloat = 0
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK:- SubFunction
    override func setupUI() {
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
//
//        tableNote.layoutSubviews()
//        tableNote.layoutIfNeeded()
//
//        DispatchQueue.main.async {
//            self.heightSave = self.tableNote.contentSize.height
//            self.heightTable.constant = self.heightSave
//        }
//        self.layoutIfNeeded()
//        self.parentTable.beginUpdates()
//        self.parentTable.endUpdates()


    }
    override func setupDataSource() {
        let textAppend = ["Gọi bác sĩ cho nhà A16 - 89", "Hẹn lịch sửa điện cho tầng 5", "In tài liệu phỏng vấn", "Vệ sinh căn hộ trống", "Tạo thông báo cho ngày mai"]
        for i in 0..<textAppend.count {
            var note = NoteModel()
            if i % 2 == 0 {
                note?.check = true
            }else {
                note?.check = false
            }
            note?.content = textAppend[i]
            notes.append(note!)
        }
        tableNote.register(UINib(nibName: "NoteCell", bundle: nil), forCellReuseIdentifier: idCell)
        tableNote.estimatedRowHeight = 70
        tableNote.bounces = false
        tableNote.delegate   = self
        tableNote.dataSource = self
        
    }
    //MARK:- Outlet Actions
    
}
extension  NoteDashboardCell : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableNote.dequeueReusableCell(withIdentifier: idCell) as! NoteCell
        cell.getData(note: notes[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.didSelect(tag: self.tag, indexSelected: indexPath)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = self.tableNote.contentSize.height
        if heightSave != height {
            self.layoutSubviews()
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    
}
