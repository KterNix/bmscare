//
//  HeaderDetailAsset.swift
//  neoBMSCare
//
//  Created by mac on 7/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class HeaderDetailAsset: UIView {

    
    //MARK:- Outlet
    @IBOutlet weak var imageAsset: UIImageView!
    @IBOutlet weak var viewAlpha: UIView!
    @IBOutlet weak var nameAsset: RecommentLabel!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.nameAsset.color = .white
        self.nameAsset.setupUI()
        self.viewAlpha.backgroundColor = .black
        self.viewAlpha.alpha = 0.4
    }
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions

}
