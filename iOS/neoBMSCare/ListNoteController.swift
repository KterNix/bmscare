//
//  ListNoteController.swift
//  neoBMSCare
//
//  Created by mac on 7/18/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import PopupDialog
private let reuseIdentifier = "NoteCell"

class ListNoteController: UITableViewController {
    
    
    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    var notes = [NoteModel]()
   
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as! NoteCell
        cell.getData(note: notes[indexPath.row] )
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        let segment =  UISegmentedControl(items: ["Của tôi","Tất cả"])
        segment.frame = CGRect(x: 0, y: 0, width: tableView.width - 10, height: 40)
        segment.tintColor = .mainColor()
        segment.setTitleTextAttributes([NSAttributedStringKey.foregroundColor : UIColor.mainColor() ], for: .normal)
        segment.setTitleTextAttributes([NSAttributedStringKey.foregroundColor : UIColor.white ], for: .selected)
        segment.selectedSegmentIndex = 1
        segment.center = view.center
        view.backgroundColor = .white
        view.addSubview(segment)
        view.dropShadow()
        return view
    }
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detail = DetailNoteController()
        detail.note = notes[indexPath.row]
        self.navigationController?.pushViewController(detail, animated: true)
    }
    //MARK:- SubFunction
    func setupUI() {
        self.title = "18/07/2018"
        self.setBackButton()
        self.createRightBarItem(title: "+", selector: #selector(self.didPressPlus))
    }
    func setupDataSource() {
        let textAppend = ["Gọi bác sĩ cho nhà A16 - 89 Tạo thông báo cho ngày mai Tạo thông báo cho ngày mai Tạo thông báo cho ngày mai Tạo thông báo cho ngày mai Tạo thông báo cho ngày mai Tạo thông báo cho ngày mai Tạo thông báo cho ngày mai Tạo thông báo cho ngày mai Tạo thông báo cho ngày mai Tạo thông báo cho ngày mai Tạo thông báo cho ngày mai Tạo thông báo cho ngày mai Tạo thông báo cho ngày mai Tạo thông báo cho ngày mai Tạo thông báo cho ngày mai ", "Hẹn lịch sửa điện cho tầng 5", "In tài liệu phỏng vấn", "Vệ sinh căn hộ trống", "Tạo thông báo cho ngày mai"]
        for i in 0..<textAppend.count {
            var note = NoteModel()
            if i % 2 == 0 {
                note?.check = true
            }else {
                note?.check = false
            }
            note?.content = textAppend[i]
            notes.append(note!)
        }
        self.tableView.register(UINib(nibName: reuseIdentifier, bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        
    }
    
    @objc func didPressPlus() {
        let vc = DetailNoteController()
        vc.titleText = "Tạo mới việc cần làm"
        self.navigationController?.pushViewController(vc, animated: true)
       
    }
    
    //MARK:- Outlet Actions
    
}
