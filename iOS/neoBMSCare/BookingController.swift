//
//  BookingController.swift
//  neoBMSCare
//
//  Created by mac on 7/5/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import FSCalendar
class BookingController: UIViewController {

    
    //MARK:- Outlet
    @IBOutlet weak var calendarView: FSCalendar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var topCollectionConstraint: NSLayoutConstraint!
    
    //MARK:- Declare
    let idCell = "BasicBookingCell"
    let idReuseView = "HeaderSectionCollection"
    var openCalendar = true
    var heightContraintForCalenedar:CGFloat = 300
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

//        self.changeHeightCalendar()
//        self.addSwipeGetureFor(view: self.view
    }
    
    //MARK:- SubFunction
    func addPanGetureFor(view: UIView) {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.wasDragged(gestureRecognizer:)))
        view.addGestureRecognizer(pan)
    }
    @objc func wasDragged(gestureRecognizer: UIPanGestureRecognizer) {
        if gestureRecognizer.state == UIGestureRecognizerState.began || gestureRecognizer.state == UIGestureRecognizerState.changed {
            let translation = gestureRecognizer.translation(in: self.view)
            print(gestureRecognizer.view!.center.y)
            if(gestureRecognizer.view!.center.y < 555) {
                gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
            }else {
                gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: 554)
            }
            
            gestureRecognizer.setTranslation(CGPoint(x: 0, y: 0), in: self.view)
        }
        
    }
    func setupUI() {
        self.title = "Dịch vụ"
//        self.calendarView.isHidden = true
//        self.calendarView.alpha = 0
        self.topCollectionConstraint.constant = 0
        self.collectionView.backgroundColor = .groupTableViewBackground
        self.createRightBarButton()
        self.calendarView.dropShadow()
        calendarView.appearance.selectionColor = .mainColor()
        calendarView.appearance.titleWeekendColor = UIColor(hexString: Config.brokenColor)
        calendarView.appearance.headerTitleColor = .mainColor()
        calendarView.appearance.weekdayTextColor = UIColor(hexString: Config.brokenColor)
        calendarView.dataSource = self
        calendarView.delegate = self
        let setting = UIBarButtonItem(badge: nil, image: (UIImage(named: "settings")?.resize(width: 24, height: 24)?.withRenderingMode(.alwaysTemplate))!, target: self, action: #selector(self.didPressSetting))
        self.navigationItem.leftBarButtonItem = setting
    }
    @objc func didPressSetting() {
        let vc = DepositSettingController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func createRightBarButton() {
        let title = openCalendar ? "Lịch" : "Đóng"
        let btn = UIBarButtonItem(badge: nil, title: title, target: self, action: #selector(changeHeightCalendar))
        btn.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font: UIFont.recommentFont], for: .normal)
        self.navigationItem.rightBarButtonItem = btn
    }
    @objc func changeHeightCalendar() {
        let height:CGFloat = openCalendar ? 300 : 0
        self.topCollectionConstraint.constant = height//heightContraintForCalenedar
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 2, options: .curveLinear, animations: {
//            self.calendarView.isHidden = self.openCalendar ? false : true
//            self.calendarView.alpha = self.openCalendar ? 1 : 0
            self.view.layoutIfNeeded()
            self.openCalendar = !self.openCalendar
            self.createRightBarButton()
        }) { (bool) in
            
        }

    }
    func setupDataSource() {
        self.calendarView.delegate = self
        self.calendarView.dataSource = self
        self.collectionView.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        self.collectionView.register(UINib(nibName: idReuseView, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: idReuseView)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
    }
    
    //MARK:- Outlet Actions

}
extension BookingController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = DetailBookingController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 10, height: 80)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: idReuseView, for: indexPath) as! HeaderSectionCollection
        view.text = "Ngày: 11/07/2018"
        view.btnMore.isHidden = true
        view.setupDataSource()
        return view
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 70)
    }
    
    
}
extension BookingController : FSCalendarDelegate, FSCalendarDataSource {
    
}
