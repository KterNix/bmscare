//
//  UserModel.swift
//
//  Created by pro on 7/13/17
//  Copyright (c) . All rights reserved.
//

import UIKit
import ObjectMapper

public struct UserModel: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let displayName = "displayName"
    static let avatar = "avatar"
    static let email = "email"
    static let provider = "provider"
    static let isOnline = "isOnline"
    static let userID = "userID"
    static let phone = "phone"
    static let buildingCode = "buildingCode"
  }

  // MARK: Properties
    public var displayName: String?
    public var avatar: String?
    public var email: String?
    public var provider: String?
    public var isOnline: String?
    public var userID: String?
    public var phone: String?
    public var buildingCode: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public init?(map: Map){

  }
    public init?(){}

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public mutating func mapping(map: Map) {
    displayName <- map[SerializationKeys.displayName]
    avatar <- map[SerializationKeys.avatar]
    email <- map[SerializationKeys.email]
    provider <- map[SerializationKeys.provider]
    isOnline <- map[SerializationKeys.isOnline]
    userID <- map[SerializationKeys.userID]
    phone <- map[SerializationKeys.phone]
    buildingCode <- map[SerializationKeys.buildingCode]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = displayName { dictionary[SerializationKeys.displayName] = value }
    if let value = avatar { dictionary[SerializationKeys.avatar] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = provider { dictionary[SerializationKeys.provider] = value }
    if let value = isOnline { dictionary[SerializationKeys.isOnline] = value }
    if let value = userID { dictionary[SerializationKeys.userID] = value }
    if let value = phone { dictionary[SerializationKeys.phone] = value }
    if let value = buildingCode { dictionary[SerializationKeys.buildingCode] = value }
    return dictionary
  }

}

