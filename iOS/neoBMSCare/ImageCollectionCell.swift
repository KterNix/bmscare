//
//  ImageCollectionCell.swift
//  neoBMSCare
//
//  Created by mac on 8/2/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ImageCollectionCell: MasterCollectionViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var imageView: UIImageView!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutIfNeeded()
        imageView.cornerRadius = imageView.height/2
        imageView.borderColor = .mainColor()
        imageView.borderWidth = 0.5
    }
    override func setupUI() {
        
    }
    func setupDataSource() {
        
    }
    //MARK:- SubFunction
   
    
    //MARK:- Outlet Actions

}
