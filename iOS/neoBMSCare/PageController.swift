//
//  PageController.swift
//  neoBMSCare
//
//  Created by pro on 7/31/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit

class PageController: UIPageViewController {

    
    var imageSlides = ["apartment1.jpg","apartment2.jpg","apartment3.jpeg","apartment1.jpg","apartment2.jpg","apartment3.jpeg"]
    
    
    
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDataSource()
        // Do any additional setup after loading the view.
    }
    
    private func setupDataSource() {
        self.dataSource = self
        if let StartView = self.viewControllerAtIndex(0) {
            self.setViewControllers([StartView], direction: .forward, animated: false, completion: nil)
            self.storyboard?.instantiateViewController(withIdentifier: "ContentController")
        }
    }
    
    
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let scrollView = view.subviews.filter({ $0 is UIScrollView }).first,
            let pageControl = view.subviews.filter({ $0 is UIPageControl }).first {
            scrollView.frame = view.bounds
            view.bringSubview(toFront:pageControl)
        }
    }
    
    
    
    func viewControllerAtIndex(_ index: Int) -> ContentController? {
        
        if ((index == NSNotFound) || (index < 0)) {
            return nil
        }
        
        if let pageview = storyboard?.instantiateViewController(withIdentifier: "ContentController") as? ContentController {
            pageview.images = imageSlides[index]
            pageview.index = index
            return pageview
        }
        return nil
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension PageController : UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! ContentController).index
        index = index - 1
        
        if (index == NSNotFound) {
            return nil
        }
        return self.viewControllerAtIndex(index)
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! ContentController).index
        index = index + 1
        
        if (index == NSNotFound) {
            return nil
        }
        if (index == imageSlides.count) {
            return nil
        }
        return self.viewControllerAtIndex(index)
    }
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        
        return self.imageSlides.count
    }
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
}










