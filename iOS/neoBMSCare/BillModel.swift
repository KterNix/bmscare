//
//  BillModel.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit

struct BillModel {
    
    var date:String         = ""
    var serviceFee:Int      = 0
    var electricPrice:Int   = 0
    var waterPrice:Int      = 0
    var totalPrice:Int      = 0
    var billStatus:Bool     = false
}
