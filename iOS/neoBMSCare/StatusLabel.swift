//
//  StatusLabel.swift
//  POS
//
//  Created by mac on 4/4/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class StatusLabel: UILabel {
    var color : UIColor?{
        didSet{
            setupUI()
        }
    }
    var bgColor:UIColor?{
        didSet{
            setupUI()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    func setupUI() {
        self.textColor = color ?? .white 
        self.font = UIFont.recommentFont
        self.textAlignment = .center
        self.layer.cornerRadius = self.frame.height/2
//        self.dropShadow()
        self.backgroundColor = bgColor ?? UIColor.mainColor().withAlphaComponent(0.5)
        self.clipsToBounds = true
    }
  
}
