//
//  DashboardCustomer.swift
//  neoBMSCare
//
//  Created by mac on 8/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class DashboardCustomer: UITableViewController {

    //MARK:- Outlet
    
    
    //MARK:- Declare
    let idRequestTaskCell = "RequestTaskCell"
    let idNoticeCell = "NoticeDashboardCell"
    let idHeader = "HeaderTitle"
    let idNoteDashboardCell = "NoteDashboardCell"
    let headerSection = ["Thông báo","Yêu cầu"]
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: TableView Func
    override func numberOfSections(in tableView: UITableView) -> Int {
        return headerSection.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: idNoticeCell) as! NoticeDashboardCell
            cell.delegate = self
            cell.tag = indexPath.section
            return cell
        } else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: idRequestTaskCell) as! RequestTaskCell
            cell.tag = indexPath.section
            cell.delegate = self
            return cell
        }
        return UITableViewCell()
        
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 2 ? self.view.frame.height - (Constants.HeightTool.tabbar + 80): UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let dashboardHeader = HeaderTitle()
        
        dashboardHeader.text = headerSection[section]
        dashboardHeader.setupUI()
        dashboardHeader.setupDataSource()
        dashboardHeader.btnMore.tag = section
        dashboardHeader.btnMore.addTarget(self, action: #selector(self.didSelectMore(sender:)), for: .touchUpInside)
        //        dashboardHeader.btnMore.isHidden = section == 2 ?  true : false
        
        return dashboardHeader
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    //MARK:- SubFunction
    @objc func didSelectMore(sender:UIButton) {
        var vc = UIViewController()
        switch sender.tag {
        case 0:
            vc = NoticeController.getViewController()
        case 1:
            vc = TaskController()
        case 2:
            vc = ListNoteController()
        default:
            break
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setupUI() {
        self.title = "DashBoard"
        self.tableView.separatorStyle = .none
        self.tableView.backgroundColor = UIColor.groupTableViewBackground
    }
    
    func setupDataSource() {
        self.tableView.register(UINib(nibName: idRequestTaskCell, bundle: nil), forCellReuseIdentifier: idRequestTaskCell)
        self.tableView.register(UINib(nibName: idNoticeCell, bundle: nil), forCellReuseIdentifier: idNoticeCell)
    }
    
    
    //MARK:- Outlet Actions
    
}

extension DashboardCustomer: CellDelegate{
    func didSelect(tag: Int, indexSelected: IndexPath) {
        switch tag {
        case 0:
            let vc = DetailNoticeController()
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 1:
            let vc = DetailTaskController()
            self.navigationController?.pushViewController(vc, animated: true)
            
        default:
            
            break
        }
        
    }
    
    
}
