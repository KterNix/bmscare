//
//  AppDelegate.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import Firebase
import GoogleSignIn
import GoogleMaps
import GooglePlaces
import SystemConfiguration.CaptiveNetwork

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    override init() {
        FirebaseApp.configure()
    }
    
    func getInterfaces() -> Bool {
        guard let unwrappedCFArrayInterfaces = CNCopySupportedInterfaces() else {
            print("this must be a simulator, no interfaces found")
            return false
        }
        guard let swiftInterfaces = (unwrappedCFArrayInterfaces as NSArray) as? [String] else {
            print("System error: did not come back as array of Strings")
            return false
        }
        for interface in swiftInterfaces {
            print("Looking up SSID info for \(interface)") // en0
            guard let unwrappedCFDictionaryForInterface = CNCopyCurrentNetworkInfo(interface as CFString) else {
                print("System error: \(interface) has no information")
                return false
            }
            guard let SSIDDict = (unwrappedCFDictionaryForInterface as NSDictionary) as? [String: AnyObject] else {
                print("System error: interface information is not a string-keyed dictionary")
                return false
            }
            for d in SSIDDict.keys {
                print("\(d): \(SSIDDict[d]!)")
            }
        }
        
        return true
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let Nav = UINavigationBar.appearance()
        Nav.isTranslucent = false
        Nav.backgroundColor = UIColor.mainColor()
        Nav.barTintColor = UIColor.mainColor()
        Nav.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        Nav.tintColor = UIColor.white
        
        // Kiểm tra nếu chưa đăng nhập thì show login, ngược lại show Home
//        if Utils.checkLogedIn() {
            let vc = ChoiceAccount()
            window?.frame =  UIScreen.main.bounds
            window?.makeKeyAndVisible()
            window?.rootViewController = vc
            window?.screen = UIScreen.main
//        }else{
//            let loginVC = mainStoryBoard.instantiateViewController(withIdentifier: "LoginController")
//            window?.rootViewController = loginVC
//        }
        
        
//        AIzaSyCHBzUrQkGLo9X-wAePK-XXETFV6BzHomA
        GMSServices.provideAPIKey("AIzaSyAQ1nwUKLrplUKi1NqtncxGrAVH1CgySyE")
        GMSPlacesClient.provideAPIKey("AIzaSyAQ1nwUKLrplUKi1NqtncxGrAVH1CgySyE")
//        AIzaSyAQ1nwUKLrplUKi1NqtncxGrAVH1CgySyE
        FBSDKApplicationDelegate.sharedInstance().application(application,
                                                              didFinishLaunchingWithOptions: launchOptions)
        
        return true
    }
    
    
    
    func application(_ app: UIApplication,
                     open url: URL,
                     options: [UIApplicationOpenURLOptionsKey : Any] = [:])
        -> Bool {
            
            var handled = FBSDKApplicationDelegate.sharedInstance().application(app,
                                                                                open: url,
                                                                                sourceApplication: options[.sourceApplication] as! String,
                                                                                annotation: options[.annotation])
            handled =  GIDSignIn.sharedInstance().handle(url,
                                                         sourceApplication:options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                         annotation: [:])
            
            return handled
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: sourceApplication,
                                                 annotation: annotation)
    }
    
    // Dùng để chuyển view khi login hoặc logout
    func changeRootView(_ vc : UIViewController){
        if let a = window {
            a.rootViewController = vc
            
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        FBSDKAppEvents.activateApp()

        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

