//
//  State.swift
//  neoBMSCare
//
//  Created by pro on 7/28/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import GoogleMaps

struct State {
    let name: String
    let lati: CLLocationDegrees
    let long: CLLocationDegrees
}
