//
//  ViewController.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import FontAwesome_swift
import CoreLocation

class HomeViewController : UIViewController {

    
    
    @IBOutlet weak var view_1: UIView!
    @IBOutlet weak var view_2: UIView!
    @IBOutlet weak var view_3: UIView!
    @IBOutlet weak var view_4: UIView!
    @IBOutlet weak var view_5: UIView!
    @IBOutlet weak var view_6: UIView!
    
    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var extrasImage: UIImageView!
    @IBOutlet weak var warningImage: UIImageView!
    
    @IBOutlet weak var noticeImage: UIImageView!
    @IBOutlet weak var tradingImage: UIImageView!
    
    @IBOutlet weak var positionImage: UIImageView!
    
    
    
    
    
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    var locationManager = CLLocationManager()
    
    
    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    var listNotice = [Notice]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        setupUI()
        NotificationCenter.default.addObserver(self, selector: #selector(self.getNotifi(_:)), name: NSNotification.Name(rawValue: "postNotice"), object: nil)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        authorizeLocation()
    }
    
    @objc func getNotifi(_ notifi: Notification) {
        let model = notifi.userInfo?["notice"] as! Notice
        self.listNotice.append(model)
        self.navigationItem.rightBarButtonItem?.badgeString = "\(self.listNotice.count)"
    }
    
    private func setupUI() {
        self.title = "neoBMSCare"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.tintColor = UIColor.white
//        
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(badge: nil, image: UIImage.fontAwesomeIcon(name:.bars, textColor: UIColor.white, size: CGSize(width: 26, height: 26)), target: self, action:#selector(self.presentLeftMenuViewController(_:)))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(badge: nil, image: UIImage.fontAwesomeIcon(name:.bell, textColor: UIColor.white, size: CGSize(width: 26, height: 26)), target: self, action:#selector(bellClick))
        
        self.navigationItem.backBarButtonItem = backWithoutTitle()
        view_1.layer.cornerRadius = 10
        view_1.layer.masksToBounds = true
        view_1.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        view_2.layer.cornerRadius = 10
        view_2.layer.masksToBounds = true
        view_2.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        view_3.layer.cornerRadius = 10
        view_3.layer.masksToBounds = true
        view_3.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        view_4.layer.cornerRadius = 10
        view_4.layer.masksToBounds = true
        view_4.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        view_5.layer.cornerRadius = 10
        view_5.layer.masksToBounds = true
        view_5.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        view_6.layer.cornerRadius = 10
        view_6.layer.masksToBounds = true
        view_6.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        
        eventImage.tintColor = UIColor.init(red: 236, green: 239, blue: 241)
        eventImage.image = UIImage(named: "event")?.withRenderingMode(.alwaysTemplate)
        
        positionImage.tintColor = UIColor.init(red: 236, green: 239, blue: 241)
        positionImage.image = UIImage(named: "position")?.withRenderingMode(.alwaysTemplate)
        
        extrasImage.tintColor = UIColor.init(red: 236, green: 239, blue: 241)
        extrasImage.image = UIImage(named: "extras")?.withRenderingMode(.alwaysTemplate)
        
        warningImage.tintColor = UIColor.init(red: 236, green: 239, blue: 241)
        warningImage.image = UIImage(named: "warning")?.withRenderingMode(.alwaysTemplate)
        
        noticeImage.tintColor = UIColor.init(red: 236, green: 239, blue: 241)
        noticeImage.image = UIImage(named: "notice")?.withRenderingMode(.alwaysTemplate)

        tradingImage.tintColor = UIColor.init(red: 236, green: 239, blue: 241)
        tradingImage.image = UIImage(named: "trading")?.withRenderingMode(.alwaysTemplate)

        
    }
    
    func authorizeLocation() {
        if (CLLocationManager.locationServicesEnabled())
        {
            if CLLocationManager.authorizationStatus() == .notDetermined {
                locationManager.requestWhenInUseAuthorization()
            }
        }
//        if CLLocationManager.locationServicesEnabled() {
//            
//            switch(CLLocationManager.authorizationStatus()) {
//            //check if services disallowed for this app particularly
//            case .restricted, .denied:
//                print("No access")
//                Utils.showAlertWithCompletion(title: "Định vị đã tắt", message: "Bạn cần bật định vị trong phần cài đặt", viewController: self, completion: { (action) in
//                    guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
//                        return
//                    }
//                    //UIApplicationOpenSettingsURLString
//                    // prefs:root=LOCATION_SERVICES
//                    if UIApplication.shared.canOpenURL(settingsUrl) {
//                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
//                        })
//                    }
//                })
//                
//                
//            //check if services are allowed for this app
//            case .authorizedAlways, .authorizedWhenInUse:
//                print("Access! We're good to go!")
//            //check if we need to ask for access
//            case .notDetermined:
//                print("asking for access...")
//                locationManager.requestAlwaysAuthorization()
//            }
//            //location services are disabled on the device entirely!
//        } else {
//            print("Location services are not enabled")
//            
//        }
    }
    
    @objc func bellClick() {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "NoticeController") as! NoticeController
        vc.noticeList = self.listNotice
        self.navigationController?.pushViewController(vc, animated: true)
        self.navigationItem.rightBarButtonItem?.badgeString = nil

    }
    
    
    
    @IBAction func eventAction(_ sender: Any) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "EventController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func positionAction(_ sender: Any) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "PositionController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func extrasAction(_ sender: Any) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ExtrasController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func BSCAction(_ sender: Any) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ReportController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func BVNAction(_ sender: Any) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "AbsentController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func MBAction(_ sender: Any) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "TradeController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
extension HomeViewController : CLLocationManagerDelegate {
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    }
}



















