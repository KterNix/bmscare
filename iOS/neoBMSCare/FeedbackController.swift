//
//  FeedbackController.swift
//  neoBMSCare
//
//  Created by pro on 7/28/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import  Eureka



class FeedbackController: MasterFormController {

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        self.title = "Góp ý"
        setBackButton()
        
        form +++
            Section()
            <<< TextAreaRow() {
                $0.title = "Góp ý"
                $0.placeholder = "Ý kiến của bạn"
        }
            +++ Section(){section in
                var header = HeaderFooterView<UIView>(.callback({
                    let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
                    let button = StyleGhost(frame: CGRect(x: 0, y: 0, width: self.view.width * 0.8, height: 40))
                    button.text = "Gửi"
                    button.center = view.center
                    button.tag = 2
                    view.addSubview(button)
                    return view
                }))
                header.height = {50}
                header.onSetupView = {view, _ in
                    guard let button = view.viewWithTag(2) as? UIButton else {return}
                    button.addTarget(self, action: #selector(self.didPressSend), for: .touchUpInside)
                }
                section.header = header
        }
    }
    @objc func didPressSend() {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
