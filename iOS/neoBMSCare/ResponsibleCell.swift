//
//  ResponsibleCell.swift
//  neoBMSCare
//
//  Created by mac on 7/30/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ResponsibleCell: MasterCollectionViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var avatarResponsible: UIImageView!
    @IBOutlet weak var nameResponsible: RecommentLabel!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutIfNeeded()
        avatarResponsible.makeCircle = true

    }
    //MARK:- SubFunction
    override func setupUI() {
        self.backgroundColor = .white
        self.dropShadow()
    }
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions

    
}
