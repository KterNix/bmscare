//
//  BVNController.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import Eureka

class AbsentController: MasterFormController {

    var getInfomation = [String:AnyObject]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }

    private func setupUI() {
        self.title = "Báo vắng nhà"
        setBackButton()
        
        
        form +++
            Section("Thông tin của bạn")
            <<< TextRow() {
                $0.title = "Điện thoại khẩn cấp"
                $0.placeholder = "Số điện thoại"
        }.onChange({ [unowned self] (text) in
            guard let phone = text.value else { return }
            self.getInfomation["phone"] = phone as AnyObject
        })
            <<< TextRow() {
                $0.title = "Người liên hệ"
                $0.placeholder = "Tên người liên hệ"
        }.onChange({ [unowned self] (text) in
            guard let name = text.value else { return }
            self.getInfomation["name"] = name as AnyObject
        })
            +++ Section("Thời gian")
            <<< DateInlineRow() {
                $0.title = "Từ ngày"
                $0.value = Date()
        }.onChange({ [unowned self] (dates) in
            guard let date = dates.value else { return }
            self.getInfomation["fromDate"] = date as AnyObject
        })
            <<< DateInlineRow() {
                $0.title = "Đến ngày"
                $0.value = Date()
        }.onChange({ [unowned self] (dates) in
            guard let date = dates.value else { return }
            self.getInfomation["toDate"] = date as AnyObject
        })
            +++ Section("Lời nhắn")
            <<< TextAreaRow() {
                $0.title = "Để lại lời nhắn"
                $0.placeholder = "Nhập lời nhắn"
        }.onChange({ (text) in
            guard let message = text.value else { return }
            self.getInfomation["message"] = message as AnyObject
        })
            +++ Section(){section in
                var header = HeaderFooterView<UIView>(.callback({
                    let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
                    let button = StyleGhost(frame: CGRect(x: 0, y: 0, width: self.view.width * 0.8, height: 40))
                    button.text = "Gửi"
                    button.center = view.center
                    button.tag = 2
                    view.addSubview(button)
                    return view
                }))
                header.height = {50}
                header.onSetupView = {view, _ in
                    guard let button = view.viewWithTag(2) as? UIButton else {return}
                    button.addTarget(self, action: #selector(self.didPressSend), for: .touchUpInside)
                }
                section.header = header
        }
        
    }
    
    @objc func didPressSend() {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}







