//
//  ComplainController.swift
//  neoBMSCare
//
//  Created by mac on 8/9/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka
class ComplainController: MasterFormController {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func setDefault() {
        form
        +++ Section("Nhập nội dung của bạn")
            <<< TextAreaRow()
        +++ Section(){section in
                var header = HeaderFooterView<UIView>(.callback({
                    let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
                    let button = StyleGhost(frame: CGRect(x: 0, y: 0, width: self.view.width * 0.8, height: 40))
                    button.text = "Gửi"
                    button.center = view.center
                    button.tag = 2
                    view.addSubview(button)
                    return view
                }))
                header.height = {50}
                header.onSetupView = {view, _ in
                    guard let button = view.viewWithTag(2) as? UIButton else {return}
                    button.addTarget(self, action: #selector(self.didPressSend), for: .touchUpInside)
                }
                section.header = header
        }
    }
    //MARK:- SubFunction
    @objc func didPressSend() {
        
    }
    func setupUI() {
        self.title = "Phàn nàn"
        setBackButton()
    }
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions
}
