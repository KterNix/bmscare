//
//  ExtrasDetailController.swift
//  ApartmentManager
//
//  Created by pro on 7/28/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import Eureka
class ExtrasDetailController: MasterFormController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI() {
        title = "Chi tiết dịch vụ"
//        createRightBarItem(title: "Đăng ký", selector: #selector(regExtras), fontSize: 14)
        setBackButton()
        form
        +++ Section("Thông tin tiện ích")
            <<< LabelRow(){
                $0.title = "Loại"
                $0.value = "Hồ bơi"
            }
            <<< LabelRow(){
                $0.title = "Ngày khai trương"
                $0.value = "17/07/2014"
            }
            <<< LabelRow(){
                $0.title = "Giá trẻ em"
                $0.value = "khoảng 18.000 đồng/lượt bơi"
            }
            <<< LabelRow(){
                $0.title = "Giá người lớn"
                $0.value = "khoảng 28.000 đồng/lượt bơi"
            }
        +++ Section("Mô tả tiện ích")
            <<< TextAreaRow(){
                $0.value = "Theo các trang hướng dẫn cho khách du lịch nước ngoài như Saigoneer, Vietnamcoracle…, hồ bơi Lam Sơn đang xếp vị trí thứ nhất trong bảng xếp hạng Top 5 hồ bơi công cộng tại TP. HCM bởi giá rẻ và hồ lớn, sạch, hiện đại. Hồ bơi Lam Sơn gồm 3 hồ: hồ lớn sâu 2m và dài đến 50m theo chuẩn hồ bơi Olympic, hồ trung bình sâu từ 0,5m – 1,4m, dài 25m và một hồ bơi dành cho trẻ em."
        }
        +++ Section("Thời gian")
            <<< LabelRow(){
                $0.title = "Mở cửa"
                $0.value = "06:30"
            }
            <<< LabelRow(){
                $0.title = "Đóng cửa"
                $0.value = "20:30"
            }
        +++ Section(){section in
                var header = HeaderFooterView<UIView>(.callback({
                    let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
                    let button = StyleGhost(frame: CGRect(x: 0, y: 0, width: self.view.width * 0.8, height: 40))
                    button.text = "Đăng ký"
                    button.center = view.center
                    button.tag = 2
                    view.addSubview(button)
                    return view
                }))
                header.height = {50}
                header.onSetupView = {view, _ in
                    guard let button = view.viewWithTag(2) as? UIButton else {return}
                    button.addTarget(self, action: #selector(self.regExtras), for: .touchUpInside)
                }
                section.header = header
        }
    }
    
    @objc func regExtras() {
        let vc = RegExtrasController.getViewController()
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}





