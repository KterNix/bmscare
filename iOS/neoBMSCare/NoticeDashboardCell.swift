//
//  NoticeDashboardCell.swift
//  neoBMSCare
//
//  Created by mac on 7/4/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class NoticeDashboardCell: MasterTableViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var heightTable: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK:- Declare
    let idCell  = "noticeCell"
    var notices = [Notice]()
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        setupDataSource()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK:- SubFunction
    override func setupUI() {
        
    }
    override func setupDataSource() {
        let noticeVC = NoticeController()
        noticeVC.setupDataSource()
        self.notices = noticeVC.noticeList
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "NoticeCell", bundle: nil), forCellReuseIdentifier: idCell)
        self.heightTable.constant = Constants.HeightCell.notice * CGFloat(notices.count)
    }
    
    //MARK:- Outlet Actions
    
}
extension  NoticeDashboardCell : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: idCell) as? NoticeCell else {return UITableViewCell()}
        cell.configWithValue(n: notices[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.HeightCell.notice
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.didSelect(tag: self.tag, indexSelected: indexPath)
    }
    
}
