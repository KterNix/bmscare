//
//  ProfileController.swift
//  ApartmentManager
//
//  Created by pro on 7/27/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import Eureka
import GoogleSignIn
import FBSDKLoginKit
import Firebase
import SVProgressHUD
import SDWebImage


protocol ProfileControllerDelegate: class {
    func changedImage(sender: ProfileController, imageChange: UIImage)
}


class ProfileController: MasterFormController {

    
    weak var delegate: ProfileControllerDelegate?
    
    var userModel = UserModel()
    
    let facebookManager = FBSDKLoginManager()
    var imageChanged = UIImageView()

    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 170
        }else {return 50}
    }
    private func setupUI() {
//        guard let user =  else { return <#return value#> }
        self.title = "Trang cá nhân"
        setBackButton()
        
        
        form +++
            Section()
            <<< ProfileRow { row in
                row.cell.selectionStyle = .none
                guard let avatar = userModel?.avatar else { return }
                row.value = Profile(title: "Avatar", avatarUser: avatar)
                self.imageChanged.sd_setImage(with: URL(string: avatar), completed: nil)
            }.onCellSelection({ [unowned self] (cell, row) in
                self.showActionSheet()
            }).cellUpdate({ (cell, row) in
//                cell.avatarUser.image = self.imageChanged.image
//                self.delegate?.changedImage(sender: self, imageChange: self.imageChanged.image!)
            })
            +++ Section()
            <<< LabelRow("name") {
                $0.title = "Tên"
                $0.value = (self.userModel?.displayName ).asString
                }
            <<< TextRow() {
                $0.title = "Số điện thoại"
                $0.value = self.userModel?.phone ?? "Chưa có"
                }
            <<< TextRow() {
                $0.title = "Mã số căn hộ"
                $0.value = self.userModel?.buildingCode ?? "Chưa có"
                }
            // Change Pass
        +++ Section(){section in
                var header = HeaderFooterView<UIView>(.callback({
                    let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
                    let button = StyleGhost(frame: CGRect(x: 0, y: 0, width: self.view.width * 0.8, height: 40))
                    button.text = "Đổi mật khẩu"
                    button.center = view.center
                    button.tag = 2
                    view.addSubview(button)
                    return view
                }))
                header.height = {50}
                header.onSetupView = {view, _ in
                    guard let button = view.viewWithTag(2) as? UIButton else {return}
                    button.addTarget(self, action: #selector(self.didPressChangePassword), for: .touchUpInside)
                }
                section.header = header
        }
        
            +++ Section(){section in
                var header = HeaderFooterView<UIView>(.callback({
                    let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
                    let button = StyleGhost(frame: CGRect(x: 0, y: 0, width: self.view.width * 0.8, height: 40))
                    button.text = "Thoát"
                    button.color = .red
                    button.center = view.center
                    button.tag = 2
                    view.addSubview(button)
                    return view
                }))
                header.height = {50}
                header.onSetupView = {view, _ in
                    guard let button = view.viewWithTag(2) as? UIButton else {return}
                    button.addTarget(self, action: #selector(self.didPressSignOut), for: .touchUpInside)
                }
                section.header = header
        }
       
    }
    @objc func didPressChangePassword()  {
        
    }
    @objc func didPressSignOut() {
        Utils.alertWithAction(title: "Thoát tài khoản",cancelTitle: "Huỷ", message: "Bạn có chắc chắn không ?", actionTitles: ["OK"], actions: [{ action1 in
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let loginView = mainStoryBoard.instantiateViewController(withIdentifier: "LoginController")
            appDelegate.changeRootView(loginView)
            GIDSignIn.sharedInstance().signOut()
            self.facebookManager.logOut()
            let userID = Utils.getUserID()
            //                Utils.ref.child("User").child("\(userID)").child("isOnline").setValue(false)
            //
            Utils.userDefault.removeObject(forKey: "loggedUserId")
            }], viewController: self, style: UIAlertControllerStyle.alert)
    }
    private func changePass() {
        let alert = UIAlertController(title: "Đổi mật khẩu", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alert.addTextField(configurationHandler: { (text) in
            text.placeholder = "Nhập mật khẩu"
        })
        
        let cancel = UIAlertAction(title: "Huỷ", style: UIAlertActionStyle.cancel, handler: nil)
        let changePass = UIAlertAction(title: "Đổi mật khẩu", style: UIAlertActionStyle.default, handler: { [weak alert] (_) in
            let textfield = alert?.textFields![0]
            SVProgressHUD.show(withStatus: "Đang đổi mật khẩu")
            Auth.auth().currentUser?.updatePassword(to: (textfield?.text)!, completion: { (error) in
                if error != nil {
                    Utils.showAlertWithCompletion(title: "Lỗi", message: (error?.localizedDescription)!, viewController: self, completion: nil)
                    return
                }
                SVProgressHUD.showSuccess(withStatus: "Đổi mật khẩu thành công !")
                SVProgressHUD.dismiss(withDelay: 2)
            })
        })
        alert.addAction(cancel)
        alert.addAction(changePass)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    private func showActionSheet() {
        Utils.alertWithAction(title: "Đổi Avatar", message: "", actionTitles: ["Camera","Thư viện ảnh"], actions: [{ [weak self] action1 in
                self?.chooseCamera()
            },{ [weak self] action2 in
                self?.choosePhoto()
            }], viewController: self, style: UIAlertControllerStyle.actionSheet)
    }
    
    
    private func chooseCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
            imagePicker.isEditing = false
            present(imagePicker, animated: true, completion: nil)
        } else {
            Utils.showAlertWithCompletion(title: "Lỗi", message: "Camera không khả dụng", viewController: self, completion: nil)
        }

    }
    
    private func choosePhoto() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.isEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    deinit {
        print("DEINIT")
    }

}
extension ProfileController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.dismiss(animated: true, completion: nil)
        var data = Data()
        let chooseImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.imageChanged.image = chooseImage
        data = UIImageJPEGRepresentation(chooseImage, 0.5)!
        let imageRef = Utils.storageRef.child("avartars/\(Utils.getUserID())\(Date()).jpg")
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpeg"
        
        _ = imageRef.putData(data, metadata: metaData, completion: { (metadata, error) in
            if (error != nil) {
                Utils.showAlertWithCompletion(title: "Lỗi", message: (error?.localizedDescription)!, viewController: self, completion: nil)
            } else {
//                let downloadURL = (metadata!.downloadURL()?.absoluteString)!
//                Utils.ref.child("User").child(Utils.getUserID()).updateChildValues(["avatar" : downloadURL])
            }
        })
    }
}











