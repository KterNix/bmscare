//
//  DashboardCell.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit

class DashboardCell: UICollectionViewCell {
    
    
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var iconDash: UIImageView!
    
    
    
    override func awakeFromNib() {
        setupUI()
    }
    private func setupUI() {
        self.layer.cornerRadius = 3
        self.layer.masksToBounds = true
    }
    
    
}
