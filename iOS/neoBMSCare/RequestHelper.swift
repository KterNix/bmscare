////
////  RequestHelper.swift
////  neoReal
////
////  Created by pro on 4/20/18.
////  Copyright © 2018 pro. All rights reserved.
////
//
//import Foundation
//import UIKit
//import TSMessages
//import Alamofire
//import SVProgressHUD
//import ObjectMapper
//
//typealias RequestHandle = (_ values: NSDictionary?,_ error: ErrorString) -> Void
//
//class RequestHelper: NSObject {
//    
//    
//
//    
//    static let shared = RequestHelper()
//    
//    var header = [String: String]()
//    
//    
////    fileprivate lazy var header: [String: String] = {
////        var h = [String: String]()
//////         h = ["Authorization" :"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MjQyMDc3NDMsImV4cCI6MTUzOTc1OTc0MywianRpIjoiNmlqdkRubmExSTZveTQ5aW16Q2J5WSIsInN1YiI6IiJ9.J6XLNpaLtj_3TecedjftIPN22AJWwt-VGWh56AvBSU8"]
////        h = ["Authorization": token]
////        return h
////    }()
//
//    func loginWith(username: String, password: String, viewController: UIViewController, completionHandler: @escaping RequestHandle) {
//        
//        let param = ["username": username,
//                     "password": password]
//        
//        
//        requestMethod(url: BaseURL.URL_LOGIN, method: .post, param: param, header: nil, encoding:JSONEncoding.default , viewController: viewController, completionHandler: completionHandler)
//        
//    }
//    
//    func register(model: UserModel, viewController: UIViewController, completionHandler: @escaping RequestHandle) {
//        
//        let param = ["fullname": model.fullname.asString,
//                     "email": model.email.asString,
//                     "password": model.password.asString,
//                     "mobile": model.mobile.asString,
//                     "type": model.type.asString,
//                     "identity": model.identity.asString,
//                     "agency" : model.agencyCode.asString,
//                     "fcm": model.fcm ?? ""
//                    ]
//        
//        requestMethod(url: BaseURL.URL_REGISTER, method: .post, param: param, header: nil, encoding: JSONEncoding.default, viewController: viewController, completionHandler: completionHandler)
//        
//    }
//    
//    func forgotPass(email: String, viewController: UIViewController, completionHandler: @escaping RequestHandle) {
//        
//        let param = ["email": email]
//        
//        requestMethod(url: BaseURL.URL_FORGOTPASS, method: .post, param: param, header: nil, encoding: JSONEncoding.default, viewController: viewController, completionHandler: completionHandler)
//        
//    }
//    
//    func verifyCode(email: String, code: String, password: String, viewController: UIViewController, completionHandler: @escaping RequestHandle) {
//        
//        let param = ["email": email,
//                     "code": code,
//                     "password": password]
//
//        requestMethod(url: BaseURL.URL_FORGOTPASS, method: .post, param: param, header: nil, encoding: JSONEncoding.default, viewController: viewController, completionHandler: completionHandler)
//        
//    }
//    
//    func getZones(viewController: UIViewController, completionHandler: @escaping RequestHandle) {
//        requestMethod(url: BaseURL.URL_GET_ZONES, method: .get, param: nil, header: header, encoding: JSONEncoding.default, viewController: viewController, completionHandler: completionHandler)
//    }
//    
//    func getProjects(zone: String ,viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        let paramater = ["zone": zone]
//        
//        requestMethod(url: BaseURL.URL_GET_PROJECTS, method: .get, param: paramater, header: header, encoding: URLEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    func getAgencies(viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {        
//        requestMethod(url: BaseURL.URL_GET_AGENCIES, method: .get, param: nil, header: header, encoding: URLEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    func getMyAgencies(viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        requestMethod(url: BaseURL.URL_GET_MY_AGENCIES, method: .get, param: nil, header: header, encoding: URLEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    func getDetailProjectWith(id: String,viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        
//        requestMethod(url: BaseURL.URL_GET_PROJECT + id, method: .get, param: nil, header: header, encoding: URLEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    func getInvestors(viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        requestMethod(url: BaseURL.URL_GET_INVESTORS, method: .get, param: nil, header: header, encoding: URLEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    func getEventHouses(eventId : String ,action:String,type:String,viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        let param = ["type" : type, "action" : action]
//        requestMethod(url: BaseURL.URL_GET_EVENT_HOUSES + eventId, method: .get, param: param, header: header, encoding: URLEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    func getListInvestors(viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        requestMethod(url: BaseURL.URL_GET_INVESTORS, method: .get, param: nil, header: header, encoding: URLEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//
//    }
//    
//    func getInvestor(id: String, viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        requestMethod(url: BaseURL.URL_GET_INVESTOR + id, method: .get, param: nil, header: header, encoding: URLEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    
//    func getEvents(viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        requestMethod(url: BaseURL.URL_GET_EVENTS, method: .get, param: nil, header: header, encoding: JSONEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    
//    func getEvent(id: String, viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        requestMethod(url: BaseURL.URL_GET_EVENT + id, method: .get, param: nil, header: header, encoding: JSONEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    
//    func getPromotions(viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        requestMethod(url: BaseURL.URL_GET_PROMOTIONS, method: .get, param: nil, header: header, encoding: JSONEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    
//    func putUser(userModel: UserModel,viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        guard let currentUser =  Utils.getCurrentUser() else {return}
//        let param = [
//                     "fullname": userModel.fullname ?? "",
//                     "address": userModel.address ?? "",
//                     "identity" : userModel.identity ?? "",
//                     "identityPlace" : userModel.identityPlace ?? "",
//                     "identityDate" : userModel.identityDate ?? "",
//                     "birthday" : userModel.birthday ?? "",
//                     "email" : userModel.email ?? ""
//                     ]
//        
//        requestMethod(url: BaseURL.URL_PUT_USER + currentUser.id.asString ,method: .put, param: param, header: self.header, encoding: JSONEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    
//    func getCustomers(viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        
////        guard let currentUser = Utils.getcurrentUser()
//        guard let user = Utils.getCurrentUser() else {return}
//        
//        requestMethod(url: BaseURL.URL_GET_CUSTOMERS + user.id.asString, method: .get, param: nil, header: header, encoding: JSONEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    
//    func getCustomer(cusID: String, viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        requestMethod(url: BaseURL.URL_GET_CUSTOMER + cusID, method: .get, param: nil, header: header, encoding: URLEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//
//        
//    }
//    func putBooking(id: String, status:String , viewController: UIViewController, complettionHandler: @escaping RequestHandle)  {
//        let param = ["status" : status, "remark" : ""]
//        requestMethod(url: BaseURL.URL_BOOKING + id, method: .put, param: param, header: header, encoding: URLEncoding.default, viewController: viewController, completionHandler: complettionHandler)
//    }
//    func getHouses(investor: String, area: String, price: String, numBed: String, direction: String, project: String, viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        
//        let url = BaseURL.URL_GET_HOUSES + "?price=\(price)&keyword=&bed=\(numBed)&projectCode=\(project)&direction=\(direction)&area=\(area)&investorCode=\(investor)"
////        print(url)
////        let param = ["price" : price,
////                     "bed" : numBed,
////                     "projectCode" : project,
////                     "direction" : direction,
////                     "area" : area,
////                     ]
//        let encodedUrl = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
//
//        requestMethod(url: encodedUrl!, method: .get, param: nil, header: header, encoding: JSONEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    
//    func getDetailHouse(id: String, viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        requestMethod(url: BaseURL.URL_GET_HOUSE + id, method: .get, param: nil, header: header, encoding: JSONEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    func getDetailBooking(id: String, viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        requestMethod(url: BaseURL.URL_BOOKING + id, method: .get, param: nil, header: header, encoding: JSONEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    
//    func getAgencyMembers(id: String, viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        requestMethod(url: BaseURL.URL_GET_AGENCY_MEMBERS + id, method: .get, param: nil, header: header, encoding: JSONEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    
//    func getInvestorMembers(id: String, viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        requestMethod(url: BaseURL.URL_GET_INVESTOR_MEMBERS + id, method: .get, param: nil, header: header, encoding: JSONEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    
//    
//    func getProfile(viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        requestMethod(url: BaseURL.URL_GET_PROFILE, method: .get, param: nil, header: header, encoding: JSONEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    
//    func getCommentOf(id: String , limit: String, viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        
//        let url = BaseURL.URL_COMMENTS + "\(id)?from=0&limit=\(limit)"
//        
//        requestMethod(url: url, method: .get, param: nil, header: header, encoding: JSONEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    
//    func postCommentFor(id: String, title: String, contents: String, rate: String, viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
//        
//        let param = ["title": title,
//                     "contents": contents,
//                     "rate": rate]
//        requestMethod(url: BaseURL.URL_COMMENTS + id, method: .post, param: param, header: header, encoding: JSONEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    func postContact(memberId : String, title: String, content: String, projectId: String, viewcontroller: UIViewController, complettionHandler: @escaping RequestHandle) {
//        let param = ["title": title, "contents" : content,"projectId" : projectId,"customerId" : ""]
//        requestMethod(url: BaseURL.URL_POST_CONTACT + memberId, method: .post, param: param, header: header, encoding: URLEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    func postContact(memberId:String, projectId: String, customerId: String, viewcontroller: UIViewController, complettionHandler: @escaping RequestHandle) {
//        let param = ["title": "", "contents" : "", "customerId" : customerId , "projectId" : projectId]
//        
//        requestMethod(url: BaseURL.URL_POST_CONTACT + memberId, method: .post, param: param, header: header, encoding: URLEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    func postBooking(type: PostType, model:BookingModel, viewcontroller : UIViewController ,complettionHandler: @escaping RequestHandle ) {
//        var url = ""
//        
//        var param :[String:Any] = ["customerCode" : model.cusCode == nil ? "" : model.cusCode.asString,
//                     "fullname":  model.cusFullname.asString,
//                     "address":  model.cusAddress.asString,
//                     "district":  model.district.asString,
//                     "city":  model.city.asString,
//                     "country":  model.country.asString,
//                     "mobile":  model.mobile.asString,
//                     "status":  model.status.asString,
//                     "identity":  model.identity.asString,
//                     "remark":  model.remark.asString,
//                     "email":  model.email.asString,
//                     "agencyId":  model.agencyId.asString,
//                     "amount":  model.amount.asString,
//                     "balanceAmount":  model.balanceAmount.asString,
//                     "identityCreatedDate":  model.identityCreatedDate.asString,
//                     "identityCreatedAt":  model.identityCreatedAt.asString,
//                     "cusBirthday":  model.cusBirthday.asString,
//                     "paymentPolicy":  model.paymentPolicy.asString,
//                     
//                     ]
//        if let signagure = model.signatureImage {
//            if let data =  UIImageJPEGRepresentation(signagure, 0.5) {
//                param["signature"] = data.base64EncodedString()
//            }
//        }
//        if let eventId =  model.eventId {
//            param["eventId"] = eventId
//        }
//        param["status"] = "1"
//
//        switch type {
//        case .Booking:
//            url = BaseURL.URL_BOOKING
//            break
//        case .Deposit:
//            url = BaseURL.URL_TRANSACTION
//            param["bookingId"] =  model.id.asString
//            param["type"] = "2"
//            break
//        case .NewDeposit:
//            url = BaseURL.URL_TRANSACTION
//            param["type"] = "1"
//
//            break
//        }
//        requestMethod(url:  url + model.houseId.asString, method:.post, param: param, header: self.header, encoding: URLEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    
//    
//    func searchMember(info: String, viewcontroller : UIViewController , complettionHandler: @escaping RequestHandle) {
////        print(header)
//         requestMethod(url: BaseURL.URL_SEARCH_MEMBER + info, method: .get, param: nil, header: header, encoding: JSONEncoding.default, viewController: viewcontroller, completionHandler: complettionHandler)
//    }
//    
//}
//
//extension RequestHelper {
//    //MARK: - BASE FUNCTION
//    private func requestMethod(url: String, method: HTTPMethod, param: [String: Any]?, header: [String: String]?,encoding:ParameterEncoding, viewController: UIViewController, completionHandler: @escaping RequestHandle) {
//        
//        
//        self.header = ["Authorization": Utils.getCurrentToken()]
//        
//        //MARK: CHECK INTERNET CONNECTION
//        guard checkInternetConnection(viewController: viewController) else {
//            SVProgressHUD.dismiss()
//            return
//        }
//        
//        loadingOnStatusBar(isShow: true)
//        SVProgressHUD.show()
//        ManagerNetworking.shared.manager.request(url, method: method, parameters: param, encoding: encoding, headers: self.header).responseJSON { (response) in
////            print(response.result.value)
//            SVProgressHUD.dismiss()
//            self.loadingOnStatusBar(isShow: false)
//            //            print(response.result.value)
//            switch response.result {
//            case .failure:
//                //MARK: - REQUEST ERROR: internet connection, timeout...
//                completionHandler(nil, .CANT_REQUEST)
//                self.showErrorCantRequest(viewController: viewController)
//                break
//            case .success:
//                
//                let values = response.result.value as? NSDictionary
//                guard let apiERROR = values?.value(forKey: "error") as? Bool else {
//                    //MARK: API ERROR
//                    completionHandler(values, .CANT_REQUEST)
//                    return
//                }
//                if apiERROR {
//                    SVProgressHUD.dismiss()
//                    completionHandler(values, .MESSAGE)
//                    return
//                }
//                completionHandler(values, .NONE)
//
//            }
//        }
//    }
//    func showErrorCantRequest(viewController :UIViewController) {
//        Utils.showAlertWithCompletion(title: "Truyền dữ liệu thất bại", message: "Đường truyền mạng không ổn định vui lòng thử lại sau", viewController: viewController, completion: nil)
//    }
//    
//    private func checkInternetConnection(viewController: UIViewController) -> Bool {
//        guard viewController.internetAvailable() else {
//            showTSMessage(inView: viewController, title: Constants.ERROR.INTERNET_TITLE, subTitle: Constants.ERROR.INTERNET_SUB_TITLE)
//            return false
//        }
//        return true
//    }
//    
//    
//    
//    func showTSMessage(inView: UIViewController, title: String, subTitle: String) {
//        TSMessage.showNotification(in: inView, title: title, subtitle: subTitle, type: TSMessageNotificationType.error, duration: 999, canBeDismissedByUser: true)
//    }
//    
//    func loadingOnStatusBar(isShow: Bool) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = isShow
//    }
//}
//
//
//
//
//
//
//
//
//
//
//
