//
//  ParentBlocksController.swift
//  neoBMSCare
//
//  Created by mac on 7/11/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
private let topConstantsForBlocks:CGFloat = 242

class ParentBlocksController: MasterViewController {

    
    //MARK:- Outlet
    @IBOutlet weak var collectionAbsent: UICollectionView!
    @IBOutlet weak var viewController: UIView!
    @IBOutlet weak var headerView: HeaderTitle!
    @IBOutlet weak var topContraintViewController: NSLayoutConstraint!
    @IBOutlet weak var heightColllectionAbsent: NSLayoutConstraint!
    @IBOutlet weak var bottomContraintViewController: NSLayoutConstraint!
    
    
    //MARK:- Declare
    let idCell = "AbsentCell"
    var isLayoutCollectionAbsent = false
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    override func setupUI() {
        
        self.headerView.text = "Danh sách vắng mặt"
        self.headerView.setupDataSource()
        self.headerView.isEnableResizingBtnMore = true
        self.headerView.btnMore.addTarget(self, action: #selector(self.didPressViewMoreAbsent), for: .touchUpInside)
        self.viewController.dropShadow()
        self.title = "Toà nhà"
        let vc = PageBlocks()
        addChildViewController(vc)
        vc.view.frame = self.viewController.bounds
        self.viewController.addSubview(vc.view)
        vc.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        vc.didMove(toParentViewController: self)
        /*
         Tạo nhân sự
        */
        
//        let humanResource = UIBarButtonItem(badge: nil, image: (UIImage(named: "placeHolder")?.resize(width: 24, height: 24)?.withRenderingMode(.alwaysTemplate))!, target: self, action: #selector(self.didPressHumanResource))
//        self.navigationItem.leftBarButtonItem = humanResource
    }
    func addGesture() {
        let panGes = UIPanGestureRecognizer(target: self, action: #selector(self.didDragging(panGesture:)))
        self.viewController.addGestureRecognizer(panGes)
    }
    override func setupDataSource() {
        self.collectionAbsent.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        self.collectionAbsent.delegate = self
        self.collectionAbsent.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.addGesture()
    }
    //MARK:- SubFunction
    @objc func didPressViewMoreAbsent() {
        isLayoutCollectionAbsent = !isLayoutCollectionAbsent
        self.topContraintViewController.constant = isLayoutCollectionAbsent ? self.view.height - ( Constants.HeightTool.tabbar) : topConstantsForBlocks
        self.heightColllectionAbsent.constant = isLayoutCollectionAbsent ? self.view.height - ( Constants.HeightTool.tabbar + 50)  : 180
        self.bottomContraintViewController.constant = isLayoutCollectionAbsent ? -140 : 0
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .layoutSubviews, animations: {
            self.collectionAbsent.collectionViewLayout.invalidateLayout()
            self.view.layoutIfNeeded()
            
            if let layout = self.collectionAbsent.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = self.isLayoutCollectionAbsent ? .vertical : .horizontal
            }
        }) { (bool) in
            
        }
    }
    @objc func didPressHumanResource() {
        let vc = HumanResourceController(nibName: "HumanResourceController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func createPopUpDetailAbsent() {
        let vc = PopUpDetailAbsent()
        self.navigationController?.tabBarController?.addChildViewController(vc)
        vc.view.backgroundColor = .white
        vc.view.layer.cornerRadius = 15
        self.navigationController?.tabBarController?.view.addSubview(vc.view)

        let frame = self.view.frame
        vc.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: frame.width, height: 600 )
        vc.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        vc.didMove(toParentViewController: self.navigationController?.tabBarController)
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: .curveLinear, animations: {
            vc.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - 500, width: frame.width, height: 600 )
        }, completion: nil)
    }
    @objc func didDragging(panGesture:UIPanGestureRecognizer) {
        let point = panGesture.location(in: self.view)
        if point.y <= topConstantsForBlocks && point.y >= 0 {
            let time = point.y - self.topContraintViewController.constant > 50 ? 0.3 : 0.1
            let yPoint = point.y - self.topContraintViewController.constant > 50 ? self.topContraintViewController.constant + 30 : point.y
            self.topContraintViewController.constant = yPoint
            self.viewController.isUserInteractionEnabled = false
            UIView.animate(withDuration: time, animations: {
                self.view.layoutIfNeeded()
            }) { (bool) in
                self.viewController.isUserInteractionEnabled = true
            }
        }
    }
    //MARK:- Outlet Actions
}
extension ParentBlocksController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.createPopUpDetailAbsent()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width =  isLayoutCollectionAbsent ? (collectionView.frame.width - 15)/2 : 200
        return CGSize(width: width, height: 170)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
