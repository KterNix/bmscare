//
//  AbsentCell.swift
//  neoBMSCare
//
//  Created by mac on 7/12/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class AbsentCell: MasterCollectionViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var timeLbl: RecommentLabel!
    @IBOutlet weak var codeHouse: TitleLabel!
    @IBOutlet weak var nameCustomer: NormalLabel!
    @IBOutlet weak var phoneCustomer: NormalLabel!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK:- SubFunction
    override func setupUI() {
        self.layer.cornerRadius = 3
        self.backgroundColor = .mainColor()
        self.timeLbl.color = .white
        self.timeLbl.setupUI()
        self.codeHouse.color = .white
        self.codeHouse.setupUI()
        self.nameCustomer.color = .white
        self.nameCustomer.setupUI()
        self.phoneCustomer.color = .white
        self.phoneCustomer.setupUI()
    }
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions

}
