//
//  HistoryDetailController.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import Eureka

class HistoryDetailController: MasterFormController {

    
    
    
    var billDetails = BillModel()
    
    
    
    
    
    var isPay:Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    private func setupUI() {
        self.title = "Tháng 7 - 2018"
        setBackButton()
        
        
        form +++
            Section("Chi tiết")
            <<< LabelRow() {
                $0.title = "Dịch vụ bảo vệ"
                $0.value = "200.000 VNĐ (!)"
                }.cellUpdate({ (cell, row) in
                    cell.detailTextLabel?.textColor = .red
                })
            <<< LabelRow() {
                $0.title = "Tiền điện"
                $0.value = "(300Kw) - 560.780 VNĐ"
                }
            <<< LabelRow() {
                $0.title = "Tiền nước"
                $0.value = "(200m3) - 100.500 VNĐ"
                }
            <<< LabelRow() {
                $0.title = "Tiền rác"
                $0.value = "50.000 VNĐ (!)"
                }.cellUpdate({ (cell, row) in
                    cell.detailTextLabel?.textColor = .red
                })
            <<< LabelRow() {
                $0.title = "Tiền gửi xe"
                $0.value = "100.000 VNĐ"
            }
            +++ Section("Tổng tiền phải thanh toán")
            <<< LabelRow() {
                $0.title = "Tổng tiền"
                $0.value = "1.100.500 VNĐ"
                }
//            +++ Section("Trạng thái")
//            <<< LabelRow() {
//                $0.title = "Trạng thái"
//                if self.billDetails.billStatus {
//                    $0.value = "Đã thanh toán"
//                    self.isPay = true
//                }else{
//                    $0.value = "Chưa "
//                    self.isPay = false
//                }
//                }.cellUpdate({ (cell, row) in
//                    cell.detailTextLabel?.textColor = self.isPay ? .mainColor() : .red
//
//                })
            +++ Section(){section in
                var header = HeaderFooterView<UIView>(.callback({
                    let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
                    let button = StyleGhost(frame: CGRect(x: 0, y: 0, width: self.view.width * 0.8, height: 40))
                    button.text = "Thanh toán"
                    button.center = view.center
                    button.tag = 2
                    view.addSubview(button)
                    return view
                }))
                header.height = {50}
                header.onSetupView = {view, _ in
                    guard let button = view.viewWithTag(2) as? UIButton else {return}
                    button.addTarget(self, action: #selector(self.didPressPayment), for: .touchUpInside)
                }
                section.header = header
        }
            +++ Section("Bình luận") { section in
                var footer =  HeaderFooterView<CommentFooter>(.nibFile(name: "CommentFooter", bundle: nil))
                footer.height = {self.view.height - (Constants.heightSubmitComment + Constants.HeightTool.tabbar)}
                section.footer = footer
                
                var header = HeaderFooterView<SubmitComment>(.nibFile(name:"SubmitComment",bundle:nil))
                header.onSetupView = {view, _ in
                    view.setupDataSource(textTitle: "Bình luận")

                }
                header.height = {Constants.heightSubmitComment}
                section.header = header
        }
        
        
        
    }
    @objc func didPressPayment() {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}













