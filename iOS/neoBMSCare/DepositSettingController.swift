//
//  DepositSettingController.swift
//  neoBMSCare
//
//  Created by mac on 7/19/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka
private let tagMaterials = "Materials"
private let tagElectronicDevice = "ElectronicDevice"
class DepositSettingController: MasterFormController {

        //MARK:- Outlet
        
        
        //MARK:- Declare
        
        
        //MARK:- Override
        override func viewDidLoad() {
            super.viewDidLoad()
            setupUI()
            setupDataSource()
            setUpForm()
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
        override func setUpForm() {
            form
                +++ Section("Vật liệu xây dựng"){
                    $0.tag = tagMaterials
                }
                <<< DecimalRow(){
                    $0.title = "Ống nước"
                    $0.value =  500000
                }
                <<< DecimalRow(){
                    $0.title = "Cửa sổ"
                    $0.value =  15000000
                }
                <<< DecimalRow(){
                    $0.title = "Sàn gạch"
                    $0.value =  5000000
                }
                <<< DecimalRow(){
                    $0.title = "Cả căn hộ"
                    $0.value =  50000000
                }
                +++ Section("Thiết bị điện tử"){
                    $0.tag = tagElectronicDevice
                }
                
                <<< DecimalRow(){
                    $0.title = "Điện thoại"
                    $0.value =  500000
                }
                <<< DecimalRow(){
                    $0.title = "Quạt"
                    $0.value =  250000
                }
                <<< DecimalRow(){
                    $0.title = "Tủ lạnh"
                    $0.value =  1000000
            }
        }
        //MARK:- SubFunction
        func setupUI() {
            self.title = "Cài đặt chi phí đặt cọc"
            setBackButton()
        }
        func setupDataSource() {
            
        }
        
        //MARK:- Outlet Actions
        
}

    


