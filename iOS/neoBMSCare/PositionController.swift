//
//  PositionController.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import GoogleMaps
import PopupDialog
import GooglePlaces
import Alamofire


class PositionController: UIViewController {

    
    
    
    
    
    @IBOutlet weak var mapView: GMSMapView!
    
    let states = [
        State(name: "Q1", lati: 10.7757, long: 106.7004),
        State(name: "Q2", lati: 10.7873, long: 106.7498),
        State(name: "Q4", lati: 10.7578, long: 106.7013),
        State(name: "Q5", lati: 10.7540, long: 106.6634),
        State(name: "Q6", lati: 10.7481, long: 106.6352),
        State(name: "Q7", lati: 10.7340, long: 106.7216),
        State(name: "Q8", lati: 10.7241, long: 106.6286),
        State(name: "Q9", lati: 10.8428, long: 106.8287),
        State(name: "Q3", lati: 10.7844, long: 106.6844),
        ]
    
    var locationManager = CLLocationManager()
    
    private lazy var currentPositionMarker = UIImage(named: "currentLocation")?.withRenderingMode(.alwaysOriginal)
    private lazy var resPotisionMarker = UIImage(named: "currentLocation")?.withRenderingMode(.alwaysOriginal)
    lazy var camera = GMSCameraPosition.camera(withLatitude: 14.0583, longitude: 108.2772, zoom: 10)

    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    var currentLocation = CLLocationCoordinate2D()
    var destinationLocation = CLLocationCoordinate2D()
    var polyLine = GMSPolyline()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupMarker()
        setupGoogleMap()
        mapView.delegate = self
        configAutoComplete()
//        self.currentLocation = CLLocationCoordinate2D(latitude: 10.7757, longitude: 106.7004)
    }
    
    
    
    
    
    
    private func setupUI() {
        self.title = "Địa điểm"        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(badge: nil, image: UIImage.fontAwesomeIcon(name:.filter, textColor: UIColor.white, size: CGSize(width: 26, height: 26)), target: self, action:#selector(filterClick))
        
    }

    
    private func setupDataSource() {
    
    }
    private func setupGoogleMap() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.distanceFilter = 500
        locationManager.delegate = self
    }
    
    private func setupMarker() {
        DispatchQueue.main.async {
            self.mapView.camera = GMSCameraPosition.camera(withLatitude: 10.7757, longitude: 106.7004, zoom: 12.0)
            
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: 10.8231, longitude: 106.6297)
            marker.iconView = UIImageView(image: self.currentPositionMarker)
            marker.title = "Viet Nam"
            marker.snippet = "Ho Chi Minh"
            marker.map = self.mapView
            
            for state in self.states {
                let resMarker = GMSMarker()
                resMarker.position = CLLocationCoordinate2D(latitude: state.lati, longitude: state.long)
                resMarker.title = state.name
                resMarker.snippet = "Hey, this is \(state.name)"
                resMarker.iconView = UIImageView(image: self.resPotisionMarker)
                resMarker.map = self.mapView
            }
        }
    }
    
    
    func drawLineWith(startPoint: CLLocationCoordinate2D, endPoint: CLLocationCoordinate2D) {
        let sessionManager = SessionManager()
        sessionManager.requestDirections(from: startPoint, to: endPoint, completionHandler: { (path, error) in
            
            if let error = error {
                print("Something went wrong, abort drawing!\nError: \(error)")
            } else {
                // Create a GMSPolyline object from the GMSPath
                self.polyLine = GMSPolyline(path: path!)
                
                // Add the GMSPolyline object to the mapView
                self.polyLine.map = self.mapView
                
                // Move the camera to the polyline
                let bounds = GMSCoordinateBounds(path: path!)
                let cameraUpdate = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 40, left: 15, bottom: 10, right: 15))
                self.mapView.animate(with: cameraUpdate)
            }
        })
    }
    
    func configAutoComplete() {
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        let subView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 45.0))
        
        subView.addSubview((searchController?.searchBar)!)
        view.addSubview(subView)
        searchController?.searchBar.sizeToFit()
        searchController?.hidesNavigationBarDuringPresentation = true
        searchController?.searchBar.tintColor = UIColor.white
        searchController?.searchBar.barTintColor = UIColor.mainColor()
        searchController?.searchBar.placeholder = "Nhập điểm đến"
        searchController?.delegate = self
        definesPresentationContext = true
        navigationController?.navigationBar.isTranslucent = false
        
    }


    
    
    @objc func filterClick() {
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension PositionController : CLLocationManagerDelegate, GMSMapViewDelegate {
    
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationManager.startUpdatingLocation()
        
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        self.polyLine.map = nil
        
        let positionDetail = PositionDetailController(nibName: "PositionDetail", bundle: nil)
        let popup = PopupDialog(viewController: positionDetail, buttonAlignment: .vertical, transitionStyle: .fadeIn, gestureDismissal: true, completion: nil)
        let buttonOne = CancelButton(title: "HUỶ", height: 44) {}
        let directionButton = DefaultButton(title: "ĐẾN ĐÂY", height: 44, dismissOnTap: true) {
            // Draw Line
            self.drawLineWith(startPoint: self.currentLocation, endPoint: marker.position)
        }
        popup.addButtons([directionButton, buttonOne])
        
        
        present(popup, animated: true, completion: nil)
        return false
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        mapView.moveCamera(GMSCameraUpdate.setTarget(CLLocationCoordinate2D(latitude: 10.8231, longitude: 106.6297), zoom: 12))
        return true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        self.currentLocation = location.coordinate
        locationManager.stopUpdatingLocation()
    }
}




extension PositionController: GMSAutocompleteResultsViewControllerDelegate, UISearchControllerDelegate {
    
    func willPresentSearchController(_ searchController: UISearchController) {
        self.polyLine.map = nil
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        // Do something with the selected place.
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(place.coordinate)")
        //Draw Line
        self.drawLineWith(startPoint: self.currentLocation, endPoint: place.coordinate)
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
