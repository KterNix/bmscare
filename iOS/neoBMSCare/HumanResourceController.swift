//
//  HumanResourceController.swift
//  neoBMSCare
//
//  Created by mac on 7/19/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka
private let idCell = "HumanResourceCell"

class HumanResourceController: UICollectionViewController {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    var type:TypeControllerForHumanResource = .humanResource
    var didSelect: ((_ index:IndexPath) -> Void)?
    var didDeSelect: ((_ index:IndexPath) -> Void)?
    var didCreateSearch = false
    var isPopup = true
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! HumanResourceCell
        switch type  {
        case .housesMembers:
            cell.position.text = "Con"
        case .createMessage:
            cell.setUpUIForCreateMessage()
        case .humanResource:
            break
        }
        return cell
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !collectionView.allowsMultipleSelection{
            collectionView.deselectItem(at: indexPath, animated: false)
        }
        guard let selected = self.didSelect else {
            let vc = DetailHumanResource()
            self.navigationController?.pushViewController(vc, animated: true)
            return}
        selected(indexPath)
    }
    override func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        guard let selected = self.didDeSelect else {return}
        selected(indexPath)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if !didCreateSearch {
            createSearch()
        }
    }
    //MARK:- SubFunction
    func setupUI() {
        self.extendedLayoutIncludesOpaqueBars = true
        self.title = "Nhân sự"
        setBackButton()
        self.collectionView?.backgroundColor = UIColor.groupTableViewBackground
        self.createRightBarItem(title: "+", selector: #selector(self.didPressCreateNew))


    }
    func setupDataSource() {
        self.collectionView?.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
    }
    @objc func didPressCreateNew() {
        let vc = DetailHumanResource()
        vc.isCreateNew = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func createSearch() {
        let y:CGFloat = !isPopup ? 64 : 0
        let search = UISearchBar(frame: CGRect(x:0, y: y, width: self.view.width, height: 50))
        search.tintColor = .white
        search.setShowsCancelButton(true, animated: false)
        search.showsSearchResultsButton =  true
        search.backgroundColor = .mainColor()
        search.delegate = self
        search.placeholder = "Tên"
        self.view.addSubview(search)
        didCreateSearch = !didCreateSearch
    }
    //MARK:- Outlet Actions

}
extension HumanResourceController :  UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height:CGFloat = type == .createMessage ? 50 : 80
        return CGSize(width: collectionView.frame.width - 10, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
}
extension HumanResourceController: UISearchBarDelegate{
    
}
