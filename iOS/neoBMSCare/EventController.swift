//
//  EventController.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit

class EventController: UIViewController {

    
    @IBOutlet weak var myTableView: UITableView!
    
    var listEvent = [Event]() {
        didSet {
            self.myTableView.reloadData()
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
        // Do any additional setup after loading the view.
    }
    private func setupUI() {
        self.title = "Sự kiện"
        setBackButton()
    }
    
    
    private func setupDataSource() {
        let item_1 = Event(name: "MercedesBenz tổ chức triển lãm giới thiệu xe mới", address: "Ton That Tung", time: "Từ 30/04/2015 -> 30/05/1017", image: "apartment1.jpg")
        let item_2 = Event(name: "MercedesBenz tổ chức triển lãm giới thiệu xe mới", address: "Ton That Tung", time: "Từ 30/04/2015 -> 30/05/1017", image: "apartment2.jpg")
        let item_3 = Event(name: "MercedesBenz tổ chức triển lãm giới thiệu xe mới", address: "Ton That Tung", time: "Từ 30/04/2015 -> 30/05/1017", image: "apartment3.jpeg")
        let item_4 = Event(name: "MercedesBenz tổ chức triển lãm giới thiệu xe mới", address: "Ton That Tung", time: "Từ 30/04/2015 -> 30/05/1017", image: "apartment4.jpg")

        self.listEvent.append(item_1)
        self.listEvent.append(item_2)
        self.listEvent.append(item_3)
        self.listEvent.append(item_4)
    }
    
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
}
extension EventController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listEvent.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell", for: indexPath) as! EventCell
        cell.configWithValue(e: listEvent[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.myTableView.deselectRow(at: indexPath, animated: true)
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "EventDetailController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lineColor = UIColor.init(red: 228, green: 227, blue: 229)
        if indexPath.row == 0 {
            let line = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 1))
            line.backgroundColor = lineColor
            cell.addSubview(line)
            cell.bringSubview(toFront: line)
        }else if indexPath.row == self.listEvent.count - 1 {
            let line = UIView(frame: CGRect(x: 8, y: 0, width: self.view.frame.size.width, height: 1))
            line.backgroundColor = lineColor
            cell.addSubview(line)
            cell.bringSubview(toFront: line)
            
            let lineBottom = UIView(frame: CGRect(x: 0, y: 119, width: self.view.frame.size.width, height: 1))
            lineBottom.backgroundColor = lineColor
            cell.addSubview(lineBottom)
            cell.bringSubview(toFront: lineBottom)
            
        }else{
            let line = UIView(frame: CGRect(x: 8, y: 0, width: self.view.frame.size.width, height: 1))
            line.backgroundColor = lineColor
            cell.addSubview(line)
            cell.bringSubview(toFront: line)
        }
    }
}








