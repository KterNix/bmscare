//
//  HistoryMaintenceController.swift
//  neoBMSCare
//
//  Created by mac on 7/26/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
private let idCell = "HistoryMaintenceBasicCell"
class HistoryMaintenceController: MasterViewController {

    
    //MARK:- Outlet
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    //MARK:- Declare
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func setupUI() {
        self.title = "Lịch sử sửa chữa"
        self.setBackButton()
        self.createRightBarItem(title: "+", selector: #selector(self.didPressCreate))
    }
    override func setupDataSource() {
        
        collectionView.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    //MARK:- SubFunction
    @objc func didPressCreate() {
        let vc = DetailHistoryMaintence()
        vc.titleText = "Tạo lịch sửa chữa"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Outlet Actions
}
extension HistoryMaintenceController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! HistoryMaintenceBasicCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = DetailHistoryMaintence()
        vc.times = indexPath.row + 1
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.width - 10, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}

