//
//  HeaderTitle.swift
//  neoBMSCare
//
//  Created by mac on 7/4/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class HeaderTitle: UICustomView {

    
    //MARK:- Outlet
    @IBOutlet var viewLine: [UIView]!
    @IBOutlet weak var titleLbl: TitleLabel!
    @IBOutlet weak var btnMore: StyleGhost!
    @IBOutlet weak var widthContraintBtnMore: NSLayoutConstraint!
    
    
    
    //MARK:- Declare
    var text = "" {
        didSet{
            setupDataSource()
        }
    }
    var isPressedBtnMore = false
    /**
        Resize width for more Button
    */
    var isEnableResizingBtnMore = false
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.viewLine.forEach { (view) in
            view.backgroundColor = .black
        }
        self.backgroundColor = .groupTableViewBackground
    }
    func setupDataSource() {
        self.titleLbl.text = text
    }
    
    //MARK:- Outlet Actions
    @IBAction func abtnMore(_ sender: Any) {
        guard isEnableResizingBtnMore else {return}
        isPressedBtnMore = !isPressedBtnMore
        let width:CGFloat = isPressedBtnMore ? 120 : 100
        let title = isPressedBtnMore ? "Thu nhỏ" : "Xem thêm"
        self.widthContraintBtnMore.constant = width
        UIView.animate(withDuration: 0.5, animations: {
            self.layoutIfNeeded()
        }) { (bool) in
            self.btnMore.text = title
            self.btnMore.setupUI()
        }
    }
    
}
