//
//  BasicBookingCell.swift
//  neoBMSCare
//
//  Created by mac on 7/7/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class BasicBookingCell: UICollectionViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var titleLbl: RecommentLabel!
    @IBOutlet weak var nameCustomerLbl: RecommentLabel!
    @IBOutlet weak var statusLbl: StatusLabel!
    @IBOutlet weak var timeLbl: NormalLabel!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
        setupDataSource()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.nameCustomerLbl.color = .black
        self.timeLbl.color = UIColor.red
        self.layer.cornerRadius = 5
    }
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions

}
