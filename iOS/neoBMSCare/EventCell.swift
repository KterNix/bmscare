//
//  EventCell.swift
//  neoBMSCare
//
//  Created by pro on 7/28/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit

class EventCell: UITableViewCell {

    
    @IBOutlet weak var imageEvent: UIImageView!
    @IBOutlet weak var nameEvent: UILabel!
    @IBOutlet weak var timeEvent: UILabel!
    @IBOutlet weak var addressEvent: UILabel!
    @IBOutlet weak var startTime: UIImageView!
    @IBOutlet weak var positionIcon: UIImageView!
    
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   
    
    func configWithValue(e: Event) {
        self.imageEvent.image = UIImage(named: "\(e.image)")
        self.startTime.image = self.startTime.image?.withRenderingMode(.alwaysTemplate)
        self.positionIcon.image = self.positionIcon.image?.withRenderingMode(.alwaysTemplate)
        self.nameEvent.text = e.name
        self.timeEvent.text = e.time
        self.addressEvent.text = e.address
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
