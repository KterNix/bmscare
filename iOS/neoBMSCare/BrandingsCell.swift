//
//  BrandingsCell.swift
//  neoBMSCare
//
//  Created by mac on 8/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
private let idCell = "CategoryProductCell"
class BrandingsCell: MasterCollectionViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var collectionView: UICollectionView!

    
    //MARK:- Declare
    let logos = ["apple.png","nokia.jpg","sony.jpg","samsung.png","oppo.png"]
    let names = ["Apple","Nokia","Sony","Samsung","Oppo"]
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupDataSource()
    }
    
    //MARK:- SubFunction
    override func setupUI() {
        
    }
    func setupDataSource() {
        collectionView.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    //MARK:- Outlet Actions

}
extension BrandingsCell : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return logos.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! CategoryProductCell
        cell.imageCategoryProduct.image = UIImage(named: logos[indexPath.row])
        cell.nameCategory.isHidden = true//text = names[indexPath.row]
        cell.numberProuct.text = "(22 Sản phẩm)"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: collectionView.frame.height * 0.9)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}

