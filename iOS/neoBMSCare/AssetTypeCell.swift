//
//  AssetTypeCell.swift
//  neoBMSCare
//
//  Created by mac on 7/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class AssetTypeCell: UICollectionViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var imageAsset: UIImageView!
    @IBOutlet weak var codeAsset: RecommentLabel!
    @IBOutlet weak var nameAsset: TitleLabel!
    @IBOutlet weak var quantityAsset: NormalLabel!
    
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var timeMaintenance: RecommentDiscountLabel!
    @IBOutlet weak var iconClock: UIImageView!
    
    //MARK:- Declare
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
//        setupDataSource()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.imageAsset.layer.cornerRadius = 5
        self.iconClock.tintColor = .black
        self.iconClock.image = self.iconClock.image?.withRenderingMode(.alwaysTemplate)
    }
    func setupDataSource(under:Bool) {
        self.iconClock.isHidden = !under
        self.timeMaintenance.isHidden = !under
    }
    
    //MARK:- Outlet Actions
    @IBAction func abtnMore(_ sender: Any) {
    }
    
}
