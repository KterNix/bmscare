//
//  TaskController.swift
//  neoBMSCare
//
//  Created by mac on 7/9/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import DropDown
import PopupDialog
class TaskController: UITableViewController {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    var idCell = "TaskCell"
    let dropDown = DropDown()
    
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: idCell) as! TaskCell
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.HeightCell.task
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailTaskController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.title = "Yêu cầu cư dân"
        setBackButton()
        if (self.tabBarController as! MasterController).accountType == .Manager {
            self.createRightBarItem(title: "＋", selector: #selector(self.didPressMore))
            self.title = "Yêu cầu của bạn"
        }
        self.createRightBarItem(title: "⇣", selector: #selector(self.didPressFilter))
        //DropDown
        
        dropDown.anchorView = self.navigationItem.rightBarButtonItem?.plainView
        
        dropDown.direction = .bottom
        dropDown.textColor = .mainColor()
        dropDown.textFont = .recommentFont
        
        dropDown.backgroundColor = .white
        dropDown.selectionBackgroundColor = .groupTableViewBackground
        dropDown.animationduration = 0.2
        dropDown.animationEntranceOptions = .transitionFlipFromRight
        dropDown.animationExitOptions = .transitionFlipFromLeft
        dropDown.bottomOffset = CGPoint(x: -100, y: (dropDown.anchorView?.plainView.height)! + 15 )
        dropDown.dataSource = ["Ngày","Nhân viên","Loại dịch vụ","Trạng thái"]
    }
    func setupDataSource() {
        self.tableView?.register(UINib(nibName: idCell, bundle: nil), forCellReuseIdentifier: idCell)

    }
    @objc func didPressFilter() {
        
        dropDown.selectionAction = {index, text in
            
            
        }
        dropDown.show()
    }
    @objc func didPressMore() {
        let vc = FormCreateService()
        let popUp = PopupDialog(viewController: vc, buttonAlignment: .horizontal, transitionStyle: .zoomIn, preferredWidth: self.view.width, gestureDismissal: true, hideStatusBar: true) {
            
        }
        
        let okBtn =  DefaultButton(title: "Xong") {
            
        }
        okBtn.backgroundColor = .mainColor()
        okBtn.setTitleColor(.white, for: .normal)
        let cancelBtn = CancelButton(title: "Huỷ") {
            
        }
        cancelBtn.backgroundColor = UIColor.groupTableViewBackground
        cancelBtn.setTitleColor(.gray, for: .normal)
        popUp.addButtons([cancelBtn,okBtn])
        self.present(popUp, animated: true, completion: nil)
    }
    //MARK:- Outlet Actions

}
