//
//  CreateNewMessageController.swift
//  neoBMSCare
//
//  Created by mac on 8/2/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka
private let idCellJoiner = "ImageCollectionCell"
class CreateNewMessageController: MasterViewController {

    
    //MARK:- Outlet
    @IBOutlet weak var collectionJoiner: UICollectionView!
    @IBOutlet weak var textFieldGroupName: UITextField!
    @IBOutlet weak var titleGroupName: NormalLabel!
    @IBOutlet weak var viewForHumanResource: UIView!
    @IBOutlet weak var heightTextField: NSLayoutConstraint!
    @IBOutlet weak var lineViewForTextField: UIView!
    @IBOutlet weak var widthLineViewContraint: NSLayoutConstraint!
    @IBOutlet weak var heightLineViewContraint: NSLayoutConstraint!
    
    
    //MARK:- Declare
    let vc = HumanResourceController(nibName: "HumanResourceController", bundle: nil)
    var joiner = [UserModel]()
    var joinerCount = 0
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupForHumanResource()
    }
    override func setupUI() {
        self.widthLineViewContraint.constant = 7
        self.heightLineViewContraint.constant = 7
        self.lineViewForTextField.backgroundColor = .mainColor()
        
        self.title = "Tạo tin nhắn"
        self.setBackButton()
        self.textFieldGroupName.font = UIFont.recommentFont
        self.textFieldGroupName.textColor = .black
        self.titleGroupName.color = .lightGray
        self.heightTextField.constant = 0
        self.viewForHumanResource.dropShadow()
        
    }
    override func setupDataSource() {
        self.collectionJoiner.register(UINib(nibName: idCellJoiner, bundle: nil), forCellWithReuseIdentifier: idCellJoiner)
        self.collectionJoiner.delegate = self
        self.collectionJoiner.dataSource = self
    }
    //MARK:- SubFunction
    //Ẩn hiện nút tạo
    func createNewChatButton(hidden:Bool) {
        UIView.animate(withDuration: 1) {
            guard !hidden else {
                self.navigationItem.rightBarButtonItems = nil
                return}
            guard self.navigationItem.rightBarButtonItems == nil else {return}
            self.createRightBarItem(title: "Tạo", selector: #selector(self.didCreateMessage), fontSize: 15)
        }
       
    }
    // Kiểm tra số lượng người đc chọn
    func checkJoinerCount() {
        if joiner.count > 1 {
            guard let text = textFieldGroupName.text, !text.isEmpty else {
                
                createNewChatButton(hidden: true)
                return
            }
            createNewChatButton(hidden: false)
        }else if joiner.count == 1{
            createNewChatButton(hidden: false)
        }else {
            createNewChatButton(hidden: true)
        }
    }
    @objc func didCreateMessage() {
        
    }
    //Đã chọn người từ HumanResource
    func didSelected(index:IndexPath) {
        let user = UserModel()
        joiner.append(user!)
//        animateAvatar(index: index)
        checkJoinerCount()
        updateActionSelect(add: true)

    }
    //Đã huỷ chọn người từ HumanResource
    func didDeSelect(index:IndexPath) {
        joiner.removeFirst()
        checkJoinerCount()
        updateActionSelect(add: false)
    }
    //Cập nhật nếu có thay đổi chọn hay bỏ chọn
    func updateActionSelect(add:Bool) {
        
        collectionJoiner.performBatchUpdates({
            if add {
                self.collectionJoiner.insertItems(at: [IndexPath(row: 0, section: 0)])
                joinerCount += 1
            }else {
                
                self.collectionJoiner.deleteItems(at: [IndexPath(row: 0, section: 0)])
                joinerCount -= 1
            }
            
        }, completion: nil)
        updateLayoutForTextField()
        

    }
    func setupForHumanResource() {
        self.lineViewForTextField.makeCircle = true
        vc.type = .createMessage
        addChildViewController(vc)
        vc.view.frame = self.viewForHumanResource.bounds
        vc.collectionView?.frame = CGRect(x: 0, y: 0, width: self.viewForHumanResource.width, height: self.viewForHumanResource.height)
        self.viewForHumanResource.addSubview(vc.view)
        vc.collectionView?.allowsMultipleSelection = true
        vc.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        vc.didMove(toParentViewController: self)
        vc.didSelect = didSelected(index:)
        vc.didDeSelect = didDeSelect(index:)
    }
    func animateAvatar(index:IndexPath) {
        let cell = vc.collectionView?.cellForItem(at: index) as! HumanResourceCell
        let imageView = UIImageView(image: cell.avatar.image)
        let avatar = cell.avatar
        imageView.frame = CGRect(x: 13, y: (avatar?.y)! + 211, width: (avatar?.width)!  , height: (avatar?.height)!)
        imageView.cornerRadius = imageView.height/2
        imageView.layer.masksToBounds = true
        self.view.addSubview(imageView)
        let point =  -(viewForHumanResource.y - 50)
        UIView.animate(withDuration: 0.5, animations: {
            imageView.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0, point, 0)

        }) { (bool) in
            imageView.removeFromSuperview()
        }
    }
    func updateLayoutForTextField() {
        let heightTF:CGFloat = joiner.count > 1 ? 50 : 0
        let heightVL:CGFloat = joiner.count > 1 ? 1 : 7
        let widthVL:CGFloat = joiner.count > 1 ? self.textFieldGroupName.width : 7
        heightTextField.constant = heightTF
        heightLineViewContraint.constant = heightVL
        widthLineViewContraint.constant = widthVL
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        }) { (bool) in
            self.lineViewForTextField.makeCircle = true
        }
    }
    //MARK:- Outlet Actions

}
extension CreateNewMessageController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return joinerCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCellJoiner, for: indexPath) as! ImageCollectionCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 50)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}

