//
//  PromotionsSliderProtocol.swift
//  neoReal
//
//  Created by pro on 4/29/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import UIKit
public typealias WillDisplayHanlder = ((_ index :Int) -> Void)
class PromotionsSliderProtocol: NSObject {
    let idCell = "ImageCell"
    var datasource = [UIImage]()
    var path:String = ""
    var timer:Timer?
    init(datasrc: [UIImage], path: String) {
        self.path = path
        self.datasource = datasrc.isEmpty ? [UIImage(named:"placeholderImage")!] : datasrc
    }
    var willDisplay : WillDisplayHanlder?
    var collectionView: UICollectionView?
    
}


extension PromotionsSliderProtocol: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func turnOnSlider() {
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.runSlider), userInfo: nil, repeats: true)
    }
    @objc func runSlider() {
        guard var indexPath = self.collectionView?.indexPathsForVisibleItems.last else {return}
        indexPath.row += 1

        if indexPath.row > datasource.count - 1 {
            indexPath.row = 0
        }
        self.collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        timer?.invalidate()
        timer = nil
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.turnOnSlider()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.collectionView = collectionView
        return datasource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! ImageCell
        let item = datasource[indexPath.row]
        cell.imageBackground.image = item
        
//        let fullPath = path + "\(item.path ?? "")"
//        if item.title == nil {
//            cell.imageBackground.image = UIImage(named: item.path.asString)
//        }else {
//        cell.imageBackground.setImageWith(path: fullPath, imageType: .NORMAL_IMAGE)
//        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.00
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.00
    }
   
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let currentIndexpath = self.collectionView?.indexPathsForVisibleItems.first else {return}
        self.willDisplay?(currentIndexpath.row)
    }
}
