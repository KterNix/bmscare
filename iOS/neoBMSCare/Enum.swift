//
//  Enum.swift
//  neoBMSCare
//
//  Created by mac on 7/11/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
enum AccountType{
    case User
    case Supplier
    case Manager
}
enum PositionMessage {
    case top
    case middle
    case bottom
}
enum TypeControllerForHumanResource{
    case housesMembers
    case humanResource
    case createMessage
}
