//
//  ExtrasController.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit

class ExtrasController: UIViewController {

    
    
    
    @IBOutlet weak var myTableView: UITableView!
    
    var extrasList = ["Hồ bơi người lớn và hồ bơi trẻ em","Trường mầm non","Siêu thị","Khu vui chơi trẻ em","Hệ thống xe bus đưa đón cư dân","Sân tennis"]
    var extrasImage = ["hbImage","tmnImage","stImage","kvcImage","busImage","tennisImage"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
        // Do any additional setup after loading the view.
    }
    
    private func setupUI() {
        self.title = "Tiện ích"        
        
        setBackButton()
        
    }
    private func setupDataSource() {
        
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ExtrasController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return extrasList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "extrasCell", for: indexPath) as! ExtrasCell
        cell.selectionStyle = .none
        
        cell.titleExtras.text = self.extrasList[indexPath.row]
        cell.extrasImage.image = UIImage(named: "\(self.extrasImage[indexPath.row])")
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.myTableView.deselectRow(at: indexPath, animated: true)
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "ExtrasDetailController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}







