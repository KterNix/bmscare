//
//  TradeDetailsController.swift
//  ApartmentManager
//
//  Created by pro on 7/27/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit

class TradeDetailController: UIViewController {

    
    
    
    @IBOutlet weak var nameMB: UILabel!
    @IBOutlet weak var addressMB: UILabel!
    @IBOutlet weak var phoneMB: UILabel!
    
    
    @IBOutlet weak var contactButton: UIButton!
    
    
    @IBOutlet weak var priceLBL: UILabel!
    
    @IBOutlet weak var textView: UITextView!
    
    
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var addressIcon: UIImageView!
    @IBOutlet weak var mobileIcon: UIImageView!
    
    @IBOutlet weak var priceIcon: UIImageView!
    
    
    
    
    var tradingDetail = Trading()
    
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
        // Do any additional setup after loading the view.
    }
    
    private func setupUI() {
        self.title = "Chi tiết"
        setBackButton()
        self.contactButton.layer.cornerRadius = 3
        self.contactButton.layer.masksToBounds = true
        userIcon.tintColor = UIColor.lightGray
        userIcon.image = UIImage(named: "person")?.withRenderingMode(.alwaysTemplate)
        addressIcon.tintColor = UIColor.lightGray
        addressIcon.image = UIImage(named: "adress")?.withRenderingMode(.alwaysTemplate)
        mobileIcon.tintColor = UIColor.lightGray
        mobileIcon.image = UIImage(named: "mobile")?.withRenderingMode(.alwaysTemplate)
        priceIcon.tintColor = UIColor.lightGray
        priceIcon.image = UIImage(named: "money")?.withRenderingMode(.alwaysTemplate)
    }
    
    private func setupDataSource() {
        self.nameMB.text = tradingDetail.name
        self.addressMB.text = tradingDetail.address
        self.phoneMB.text = "\(tradingDetail.phone)"
        self.priceLBL.text = "\(tradingDetail.price)đ"
    }
    
    
    @IBAction func contactAction(_ sender: Any) {
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pageView" {
//            let vc = segue.destination as! PageController
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}














