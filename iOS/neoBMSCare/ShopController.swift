//
//  ShopController.swift
//  neoBMSCare
//
//  Created by mac on 8/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

private let promotionCell = "PromotionsCell"
private let brandingsCell = "BrandingsCell"
private let listNewProductsCell = "ListProductWithTitleCell"
private let listProductSeenCell = "ListProductSeenCell"
private let productCell = "ProductCell"
private let idReuseView = "HeaderSectionCollection"

private let heightNewProductsCell:CGFloat = 80 + 195*2
private let heightProductsSeenCell:CGFloat = 80 + 195

class ShopController: MasterViewController {

    
    //MARK:- Outlet
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var categoryView: CategoryView!
    @IBOutlet weak var viewAlphaForCateView: UIView!
    
    //MARK:- Declare
    var didCheckPressCategory = true
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        didPressCategory()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func setupUI() {
        self.title = "Shop"
        setBackButton()
        createRightBarItem(title: "Danh mục", selector: #selector(didPressCategory), fontSize: 15)
        createTapGesForView()
        collectionView.backgroundColor = .groupTableViewBackground
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

    }
    override func setupDataSource() {
        
        self.collectionView.register(UINib(nibName: promotionCell, bundle: nil), forCellWithReuseIdentifier: promotionCell)
        self.collectionView.register(UINib(nibName: brandingsCell, bundle: nil), forCellWithReuseIdentifier: brandingsCell)
        self.collectionView.register(UINib(nibName: listNewProductsCell, bundle: nil), forCellWithReuseIdentifier: listNewProductsCell)
        self.collectionView.register(UINib(nibName: productCell, bundle: nil), forCellWithReuseIdentifier: productCell)
        self.collectionView.register(UINib(nibName: idReuseView, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: idReuseView)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    //MARK:- SubFunction
    func createTapGesForView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.didTapView))
        self.view.addGestureRecognizer(tap)
    }
    @objc func didTapView() {
        self.didCheckPressCategory = true
        self.didPressCategory()
    }
    @objc func didPressCategory() {
        didCheckPressCategory = !didCheckPressCategory
        let titleRightBar = didCheckPressCategory ? "Đóng" : "Danh mục"
        UIView.animate(withDuration: 0.25, animations: {
            if self.didCheckPressCategory {
                self.collectionView.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                
                self.viewAlphaForCateView.transform = CGAffineTransform.identity
            }else {
                self.categoryView.transform = CGAffineTransform(translationX: self.view.width , y: 0)
                
            }
            self.navigationItem.rightBarButtonItems = nil
            self.createRightBarItem(title: titleRightBar, selector: #selector(self.didPressCategory), fontSize: 15)
        }) { (bool) in
            UIView.animate(withDuration: 0.25, animations: {
                if self.didCheckPressCategory {
                    self.categoryView.transform = CGAffineTransform.identity
                }else {
                    self.collectionView.transform = CGAffineTransform.identity
                    self.viewAlphaForCateView.transform =
                        CGAffineTransform(translationX: self.view.width, y: 0)
                }
            }, completion: { (bool1) in
            })
        }
    }
    @objc func didSelectMore(btn:UIButton) {
        let tag = btn.tag
    }
   
    //MARK:- Outlet Actions
    

  

}
extension ShopController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return section == 4 ? 6 : 1
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: promotionCell, for: indexPath)
            return cell
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: brandingsCell, for: indexPath)
            return cell
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: listNewProductsCell, for: indexPath) as! ListProductWithTitleCell
            cell.setupDataSource(title: "Sản phẩm mới nhất",sizeItem: nil , scrollDirection: .horizontal)
            return cell
        case 3:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: listNewProductsCell, for: indexPath) as! ListProductWithTitleCell
            cell.setupDataSource(title: "Sản phẩm đã xem",sizeItem: nil , scrollDirection: .horizontal)
            return cell
        case 4:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: productCell, for: indexPath)
            return cell
        default:
            return UICollectionViewCell()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.collectionView.width
        switch indexPath.section {
        case 0:
            return CGSize(width: width, height: 200)
        case 1:
            return CGSize(width: width, height: 120)
        case 2:
            return CGSize(width: width, height: heightNewProductsCell)
        case 3:
            return CGSize(width: width, height: heightProductsSeenCell)
        case 4:
            return CGSize(width: (width - 15)/2, height: 185)
        default:
            return CGSize(width: 0, height: 0)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let top:CGFloat = section == 0 ? 0 : 5
        return UIEdgeInsetsMake(top, 5, 5, 5)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
            switch kind {
            case UICollectionElementKindSectionHeader:
                let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: idReuseView, for: indexPath) as! HeaderSectionCollection
                view.text = "Sản phẩm tương tự"
                view.isEnableResizingBtnMore = true
                view.btnMore.tag = indexPath.section
                view.btnMore.addTarget(self, action: #selector(self.didSelectMore(btn:)), for: .touchUpInside)
                view.setupDataSource()
                return view
            default:
                return  UICollectionReusableView()
            }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return section == 4 ? CGSize(width: collectionView.width, height: 80) : CGSize(width: 0, height: 0)
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return section == 0 ? 0 : 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return section == 0 ? 0 : 5
    }
}

