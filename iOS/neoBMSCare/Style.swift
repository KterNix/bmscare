//
//  StyleDefault.swift
//  POS
//
//  Created by mac on 4/3/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class StyleDefault: UIButton {
    var textColor :UIColor?
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func setUpWithBackground(color: UIColor) {
        self.setAttributedTitle(NSAttributedString(string: (self.titleLabel?.text).asString, attributes: [NSAttributedStringKey.font : UIFont.buttonFont, NSAttributedStringKey.foregroundColor : textColor ?? UIColor.white ]), for: .normal)
        self.tintColor = textColor ?? UIColor.white
        self.backgroundColor = color
        self.layer.cornerRadius = 5
        self.imageEdgeInsets = UIEdgeInsetsMake(10,10,10,10)
        self.setImage(self.currentImage?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.layer.masksToBounds = true
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
}
class StyleRecomment: UIButton {
    var text :String?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
    }
    override init(frame: CGRect) {
        super.init(frame:frame )
        self.setupUI()
    }
    func setupUI() {
        self.setAttributedTitle(NSAttributedString(string: text == nil ? (self.titleLabel?.text).asString : text.asString, attributes: [NSAttributedStringKey.font : UIFont.recommentFont, NSAttributedStringKey.foregroundColor : UIColor.mainColor()]), for: .normal)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
class BackButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
    }
    override init(frame: CGRect) {
        super.init(frame:frame )
        self.setupUI()
    }
//    @IBInspectable
//    var radius :CGFloat = 2.0 {
//        didSet{
//            self.layer.cornerRadius = self.radius
//        }
//    }
    func setupUI() {
        self.setAttributedTitle(NSAttributedString(string: "←", attributes: [NSAttributedStringKey.font : UIFont.backButtonFont, NSAttributedStringKey.foregroundColor : UIColor.mainColor() ]), for: .normal)
    }
    func setupDataSource() {
        
    }
    convenience init() {
        self.init()
        self.setupUI()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
class StyleGhost: UIButton {
    var text :String?{
        didSet{
            setupUI()
        }
    }
    var color:UIColor?{
        didSet{
            setupUI()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    override init(frame: CGRect) {
        super.init(frame:frame )
        self.setupUI()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutIfNeeded()
        setupUI()
    }
    func setupUI() {
        self.setAttributedTitle(NSAttributedString(string: text == nil ? (self.titleLabel?.text).asString : text.asString, attributes: [NSAttributedStringKey.font : UIFont.recommentFont, NSAttributedStringKey.foregroundColor : color ?? .secondColor()]), for: .normal)
        self.layer.borderWidth = 0.5
        self.layer.borderColor = color?.cgColor ?? UIColor.secondColor().cgColor
        self.layer.cornerRadius = self.frame.height/2
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
