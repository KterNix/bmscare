//
//  DetailNoteController.swift
//  neoBMSCare
//
//  Created by mac on 7/30/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka
import IQKeyboardManagerSwift
import PopupDialog
private let idCell = "ResponsibleCell"
class DetailNoteController: MasterFormController {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    var note: NoteModel?
    var titleText = ""
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpForm()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func setUpForm() {
        form
            +++ Section("Thông tin")
            <<< TextAreaRow() {
                $0.title = "Nội dung"
                $0.value = note?.content
            }
            <<< PickerInputRow<String>(){
                $0.title = "Trạng thái"
                $0.value = self.isCreateNote() ? "Chưa hoàn thành" : "Đã hoàn thành"
                $0.options = ["Đã hoàn thành","Chưa hoàn thành"]
            }
            <<< PickerInputRow<String>() {
                $0.title = "Thư mục"
                $0.value = "Toà nhà"
                $0.options = ["Toà nhà","Dịch vụ","Riêng tôi","Tạo mới"]
                }.onCellSelection({ (cell, row) in
                    
                }).onChange({ (row) in
                    if row.value == "Tạo mới" {
                        self.createViewForAddFolder()
                    }
                })
            +++ Section(){section in
                //Footer Người đảm nhiệm
                var footer = HeaderFooterView<DetailHouseInforHeader>(.nibFile(name:"DetailHouseInforHeader",bundle:nil))
                footer.onSetupView = {view, _ in
                    view.title.text = "Người đảm nhiệm"
                    view.setupUI()
                    view.btnMore.text = "Thêm"
                    view.btnMore.addTarget(self, action: #selector(self.didPressAddMoreResponsible), for: .touchUpInside)
                    view.collectionView.backgroundColor = UIColor.groupTableViewBackground
                    view.collectionView.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
                    view.collectionView.delegate = self
                    view.collectionView.dataSource = self
                    
                }
                footer.height = {200}
                section.footer = footer
            }
            <<< LabelRow() {
                $0.title = "Ngày tạo"
                $0.value = "10:30 10/07/2018"
                $0.hidden = self.isCreateNote() ? true : false
                $0.evaluateHidden()
            }
            <<< DateInlineRow(){
                $0.title = "Hạn chót"
                $0.value = Date()
            }
            <<< PickerInputRow<String>(){
                $0.title = "Nhắc nhở"
                $0.options = ["Có","Không"]
                $0.value = "Có"
                
                }.cellUpdate({ (cell, row) in
                    guard let rowByTag = self.form.rowBy(tag: "remindTime") else {return}
                    rowByTag.hidden = row.value == "Có" ? false : true
                    rowByTag.evaluateHidden()
                })
            <<< TimeRow(){
                $0.tag = "remindTime"
                $0.title = "Thời gian"
                $0.value = Date()
            }
        
        
    }
    //MARK:- SubFunction
    @objc func  didPressAddMoreResponsible() {
        
        let vc = HumanResourceController(nibName: "HumanResourceController", bundle: nil)
        vc.collectionView?.frame = CGRect(x: 0, y: 0, width: 335, height: 400)
        let heightContraint = NSLayoutConstraint(item:vc.view , attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 400)
        vc.view.addConstraint(heightContraint)
        vc.collectionView?.allowsMultipleSelection = true
        let cancel = CancelButton(title: "Huỷ") {
            
        }
        let accept = DefaultButton(title: "Xong") {
            
        }
        cancel.setTitleColor(.lightGray, for: .normal)
        cancel.backgroundColor = UIColor.groupTableViewBackground
        accept.setTitleColor(.white, for: .normal)
        accept.backgroundColor = .mainColor()
        let popUp = PopupDialog(viewController: vc)
        popUp.buttonAlignment = .horizontal
        popUp.addButtons([cancel,accept])
        self.present(popUp, animated: true, completion: nil)
    }
    func setupUI() {
        self.title = titleText.count <= 0 ? "Chi tiết việc cần làm" : titleText
        self.setBackButton()
        self.createRightBarItem(title: "✓", selector: #selector(saveAcction))
    }
    func setupDataSource() {
        
    }
    @objc func saveAcction() {
        
    }
    func createViewForAddFolder() {
        let alphaView = UIView(frame: (self.tabBarController?.view.frame)!)
        alphaView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        alphaView.tag = 10
        let view = UIView(frame: CGRect(origin: CGPoint(x: 0, y: -15), size: CGSize(width: self.view.width, height: 200)))
        view.backgroundColor = .white
        
        let textField = UITextField(frame: CGRect(x: (self.view.width - (self.view.width * 0.9))/2 , y: (200/2) - 25, width: self.view.width * 0.9, height: 50))
        textField.borderStyle = .roundedRect
        textField.backgroundColor = UIColor.groupTableViewBackground
        textField.placeholder = "Tên thư mục"
        textField.textColor = .mainColor()
        textField.font = .recommentFont
        textField.setPlaceHolderTextColor(.lightGray)
        textField.delegate = self
        let exitButton = UIButton(frame: CGRect(x: self.view.width - 45, y: 200 - 45, width: 30, height: 30))
        exitButton.setAttributedTitle(NSAttributedString(string: "×", attributes: [.font : UIFont.recommentFont, .foregroundColor: UIColor.red ]), for: .normal)
        exitButton.borderColor = .red
        exitButton.cornerRadius = exitButton.height/2
        exitButton.borderWidth = 0.5
        exitButton.addTarget(self, action: #selector(self.removeViewAddMore), for: .touchUpInside)
        view.cornerRadius = 15
        view.dropShadow()
        view.tag = 11
        view.addSubview(textField)
        view.addSubview(exitButton)
        alphaView.addSubview(view)
        self.navigationController?.view.addSubview(alphaView)
        view.transform = CGAffineTransform.init(translationX: 0, y: -300)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            view.transform = CGAffineTransform.identity
            Utils.isDefaultBarStatus = true
        }, completion: { (bool) in
            IQKeyboardManager.sharedManager().enable = true
            textField.becomeFirstResponder()
            
        })
    
    }
    @objc func removeViewAddMore() {
        guard let view  = self.tabBarController?.view.viewWithTag(10) else {return}
        IQKeyboardManager.sharedManager().enable = false
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.5, animations: {
            view.viewWithTag(11)?.transform = CGAffineTransform(translationX: 0, y: -300)
            Utils.isDefaultBarStatus = false

        }) { (bool) in
            UIView.animate(withDuration: 0.5, animations: {
                view.alpha = 0
            }, completion: { (bool) in
                view.removeFromSuperview()
            })
        }
        
    }
    func isCreateNote() -> Bool {
        let bool =  titleText.contains("Tạo")
        return bool
    }
    //MARK:- Outlet Actions

}
extension DetailNoteController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.isCreateNote() ? 0 : 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! ResponsibleCell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: collectionView.frame.height * 0.8)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
extension DetailNoteController:UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        removeViewAddMore()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        removeViewAddMore()
        return true
    }

}
