//
//  HeaderSectionCollection.swift
//  neoBMSCare
//
//  Created by mac on 7/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class HeaderSectionCollection: UICollectionReusableView {

    
    //MARK:- Outlet
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var titleLbl: TitleLabel!
    @IBOutlet weak var btnMore: StyleGhost!
    @IBOutlet weak var heightButtonMore: NSLayoutConstraint!
    
    
    //MARK:- Declare
    var text = ""
    var isSelected = false
    var isEnableResizingBtnMore = false
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
       setupUI()

        // Initialization code
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.viewLine.backgroundColor = .black
        self.backgroundColor = .groupTableViewBackground
    }
    func setupDataSource() {
        self.titleLbl.text = text
        guard let title = returnTitleForBtnMore() else {return}
        UIView.animate(withDuration: 0.5, animations: {
            self.layoutIfNeeded()
        }) { (bool) in
            self.btnMore.text = title
        }
    }
    
    //MARK:- Outlet Actions
    @IBAction func abtnMore(_ sender: Any) {
        guard isEnableResizingBtnMore else {return }
        isSelected = !isSelected
        
    }
    
    func returnTitleForBtnMore() -> String? {
        let width:CGFloat = isSelected ? 120 : 100
        let title = isSelected ? "Thu nhỏ" : "Xem thêm"
        self.heightButtonMore.constant = width
        return title

    }
}
