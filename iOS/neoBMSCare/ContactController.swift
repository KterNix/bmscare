//
//  ContactController.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import Eureka

class ContactController: MasterFormController {

    
    
    
    
    
    
    
    
    var getContact = [String:AnyObject]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    private func setupUI() {
        self.title = "Liên hệ"
        setBackButton()
        
        
        form +++
            Section("Tiêu đề:")
                <<< TextRow() {
                    $0.title = "Tiêu đề"
                    $0.placeholder = "Nhập tiêu đề"
            }.onChange({ [unowned self] (text) in
                guard let name = text.value else { return }
                self.getContact["name"] = name as AnyObject
            })
        +++ Section("Nội dung:")
                <<< TextAreaRow() {
                $0.title = "Nội dung"
                $0.placeholder = "Nhập nội dung"
            }.onChange({ [unowned self] (text) in
                guard let content = text.value else { return }
                self.getContact["content"] = content as AnyObject
            })
        +++ Section()
        <<<     ButtonRow() {
                $0.title = "Gửi"
            }.onCellSelection({ [unowned self] (cell, row) in
                print(self.getContact)
                Utils.showAlertWithCompletion(title: "Đã gửi", message: "Cám ơn bạn đã quan tâm đến chúng tôi, chúng tôi sẽ phản hồi sớm nhất có thể", viewController: self, completion: {
                    //Action
                })
            })
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
