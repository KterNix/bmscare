//
//  HistoryCell.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit

class HistoryCell: UITableViewCell {

    
    
    
    @IBOutlet weak var dateLBL: UILabel!
    @IBOutlet weak var totalLBL: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var lastPay: UILabel!
    
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    
    func configWithValue(bill: BillModel) {
        self.dateLBL.text = bill.date
        self.totalLBL.text = "Tổng cộng: \(bill.totalPrice)đ"
        if bill.billStatus {
            self.status.textColor = UIColor.mainColor()
            self.status.text = "Đã thanh toán"
        }else{
            self.status.textColor = UIColor.red
            self.status.text = "Chưa thanh toán"
        }
    }
    
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
