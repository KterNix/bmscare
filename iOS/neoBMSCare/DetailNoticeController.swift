//
//  DetailNoticeController.swift
//  neoBMSCare
//
//  Created by mac on 7/10/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka
class DetailNoticeController: MasterFormController {
    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    var isCreateNew = false
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func setUpForm() {
        form
            +++ Section("Thông báo")
            <<< LabelRow() {
                $0.title = "Mã"
                $0.value =  "A45 - 10"
                $0.hidden = isCreateNew ? true : false
                $0.evaluateHidden()
            }
            <<< PickerInputRow<String>() {
                $0.title = "Loại"
                $0.value = isCreateNew ? nil : "Cá nhân"
                $0.options = ["Những người được nhận","Tất cả mọi người"]
                $0.disabled = isCreateNew ? false : true
            }
            +++ Section("Nội dung")
            <<< TextAreaRow() {
                //                $0.title = ""
                
                $0.value = isCreateNew ? nil : "Bạn đã đăng ký tiện ích Hồ Bơi"
                
            }
            <<< DecimalRow() {
                $0.title = "Phí"
                $0.value = isCreateNew ? nil : 1000000
            }
            +++ Section("Thời gian")
            <<< DateTimeInlineRow() {
                $0.title = "Ngày đăng ký"
                $0.value = isCreateNew ? nil : Date()
                $0.disabled = isCreateNew ? false : true
            }
            <<< DateInlineRow() {
                $0.title = "Ngày hiệu lực"
                $0.value = isCreateNew ? nil : Date()
                $0.disabled = isCreateNew ? false : true
            }
            <<< DateInlineRow() {
                $0.title = "Ngày hết hạn"
                $0.value = isCreateNew ? nil : Date()
                $0.disabled = isCreateNew ? false : true
        }
    }
    
    //MARK:- SubFunction
    func setupUI() {
        setBackButton()
        self.title = isCreateNew ? "Tạo thông báo" :"Chi tiết thông báo"
        
    }
    func setupDataSource() {
        setUpForm()
    }
    
    //MARK:- Outlet Actions

    
    

}
