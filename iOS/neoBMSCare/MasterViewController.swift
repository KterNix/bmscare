//
//  MasterViewController.swift
//  neoBMSCare
//
//  Created by mac on 7/10/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class MasterViewController: UIViewController {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        
    }
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions

}
