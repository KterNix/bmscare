//
//  NoteCell.swift
//  neoBMSCare
//
//  Created by mac on 7/18/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class NoteCell: MasterTableViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var deadLine: NormalLabel!
    @IBOutlet weak var content: RecommentLabel!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var nameAssign: RecommentLabel!
    @IBOutlet weak var iconAlarm: UIImageView!
    @IBOutlet weak var iconShare: UIImageView!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    override func setupUI() {
//        time.color = .white
//        time.backgroundColor = .gray
//        time.layer.cornerRadius = time.height/2
//        time.layer.masksToBounds =  true
//        viewForLabel.layer.cornerRadius = 5
//        viewForLabel.dropShadow()
        deadLine.color = .lightGray
        nameAssign.color = .mainColor()
        iconAlarm.tintColor = .lightGray
        iconShare.tintColor = .lightGray
        iconAlarm.image = iconAlarm.image?.withRenderingMode(.alwaysTemplate)
        iconShare.image = iconShare.image?.withRenderingMode(.alwaysTemplate)
        content.color = .black
        btnCheck.layer.cornerRadius = btnCheck.height/2
        
        btnCheck.layer.borderWidth = 1
        btnCheck.setTitleColor(.mainColor(), for: .normal)

    }
    override func setupDataSource() {
        
    }
    //MARK:- SubFunction
    
    func getData(note:NoteModel)  {
        content.text = note.content
        var color = UIColor()
        if note.check! {
            btnCheck.setTitle("●", for: .normal)
           color = .mainColor()
        }else {
            color = .lightGray
        }
        btnCheck.layer.borderColor = color.cgColor

    }
    //MARK:- Outlet Actions
    @IBAction func abtnCheck(_ sender: Any) {
    }
    

}
