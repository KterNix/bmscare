//
//  Trading.swift
//  ApartmentManager
//
//  Created by pro on 7/27/17.
//  Copyright © 2017 pro. All rights reserved.
//

import Foundation

struct Trading {
    var image:String = ""
    var title:String = ""
    var name:String = ""
    var price:Int = 0
    var date:String = ""
    var phone:Int = 0
    var address:String = ""
    var type:String = ""
}
