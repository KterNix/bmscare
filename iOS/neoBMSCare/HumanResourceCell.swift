//
//  HumanResourceCell.swift
//  neoBMSCare
//
//  Created by mac on 7/19/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class HumanResourceCell: MasterCollectionViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var name: RecommentLabel!
    @IBOutlet weak var phone: NormalLabel!
    @IBOutlet weak var position: NormalLabel!
    @IBOutlet weak var viewTick: UIView!

    
    //MARK:- Declare
    let shapeLayer = CAShapeLayer()
    var isAddCheckMark = false
    var path : UIBezierPath = {
        let newPath = UIBezierPath()
        
        newPath.move(to: CGPoint(x: 5 , y: 25))
        newPath.addLine(to: CGPoint(x: 10, y: 30))
        newPath.addLine(to: CGPoint(x: 30, y: 10))
        newPath.miterLimit = 4
        return newPath
    }()
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layoutIfNeeded()
        self.avatar.layer.cornerRadius = self.avatar.height/2
        self.avatar.layer.masksToBounds = true
        self.avatar.layer.borderColor = UIColor.mainColor().cgColor
        self.avatar.layer.borderWidth = 0.5
    }
    override func setupUI() {
        self.name.color = .black
        self.position.color = .mainColor()
        self.dropShadow()
        self.backgroundColor = .white
        
    }
    func setupDataSource() {
        
    }
    override var isSelected: Bool{
        didSet{
            if isSelected {
                if !self.isAddCheckMark {
                    self.drawCheckMark()

                }
            }else {
                self.removeCheckMark()
            }
        }
    }
    
    //MARK:- SubFunction

    func drawCheckMark() {
        guard self.viewTick.layer.sublayers == nil else {
            self.addAnimationForShape(reverse: false)
            return
        }
        shapeLayer.strokeColor = UIColor.mainColor().cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = 2.0
        shapeLayer.path = path.cgPath
        shapeLayer.lineCap = kCALineCapRound
        self.viewTick.layer.addSublayer(shapeLayer)
        self.addAnimationForShape(reverse: false)
        


    }
    func addAnimationForShape(reverse : Bool) {
        let strokeEndAnimation = CABasicAnimation(keyPath: "strokeEnd")
        
        strokeEndAnimation.fromValue = reverse ? 1 : 0.0
        strokeEndAnimation.duration = 0.3
        strokeEndAnimation.toValue = reverse ? 0 : 1.0
        strokeEndAnimation.fillMode = reverse ? kCAFillModeRemoved : kCAFillModeForwards
        strokeEndAnimation.isRemovedOnCompletion = reverse ? true : false
        strokeEndAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        shapeLayer.add(strokeEndAnimation, forKey: "drawLineAnimation")
        isAddCheckMark = !reverse
    }
    func removeCheckMark() {
        shapeLayer.removeAllAnimations()
        shapeLayer.strokeEnd = 0
        addAnimationForShape(reverse: true)

    }
    func setUpUIForCreateMessage() {
        self.phone.isHidden = true
        self.position.isHidden = true
    }
    //MARK:- Outlet Actions

}
