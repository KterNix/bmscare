//
//  ConnectCell.swift
//  neoBMSCare
//
//  Created by mac on 8/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ConnectCell: MasterCollectionViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var titleConnect: TitleLabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var viewCorner: UIView!
    @IBOutlet weak var descriptionLabel: RecommentLabel!
    @IBOutlet weak var viewParent: UIView!
    @IBOutlet weak var btnView: StyleGhost!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setupUI() {
        self.titleConnect.color = .mainColor()
        self.viewParent.borderColor = .mainColor()
        self.viewParent.borderWidth = 1
        self.icon.tintColor = .white
        self.descriptionLabel.color =  .lightGray
        self.viewCorner.backgroundColor = .mainColor()
        self.viewParent.cornerRadius = 5

    }
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutIfNeeded()
        viewCorner.cornerRadius = viewCorner.height/2
    }
    func setupDataSource(title:String,image:String,description:String,titleButton:String) {
        
        self.btnView.text = titleButton
        self.titleConnect.text = title
        self.icon.image = UIImage(named: image)?.withRenderingMode(.alwaysTemplate)
       self.descriptionLabel.text = description
    }
    
    //MARK:- SubFunction
    
    
    //MARK:- Outlet Actions

}
