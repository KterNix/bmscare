//
//  UIViewController.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

extension UIViewController {
    
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    func createRightBarItem(title :String, selector: Selector,fontSize:CGFloat = 30) {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        button.setAttributedTitle(NSAttributedString(string: title, attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: fontSize), NSAttributedStringKey.foregroundColor: UIColor.white]), for: .normal)
        button.addTarget(self, action: selector, for: .touchUpInside)
        let bar = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItems == nil ? self.navigationItem.rightBarButtonItems = [bar] : self.navigationItem.rightBarButtonItems?.append(bar)
    }
    static func getViewController () -> UIViewController{
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: String(describing: self))
        return vc
    }
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func postNotificationWithKey(_ name: String,userInfo: [AnyHashable : Any]?) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: name), object: nil, userInfo: userInfo)
    }
    
    func observerNotificationWithKey(_ name: String,selectors: String) {
        NotificationCenter.default.addObserver(self, selector: Selector("\(selectors)"), name: NSNotification.Name(rawValue: name), object: nil)
    }
    
    func createSearchBar(placeHolder: String, doneSelector: Selector) {
        let searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 150, height: 30))
        searchBar.placeholder = placeHolder
        searchBar.backgroundColor = .clear
        searchBar.tintColor = .white
        searchBar.showsSearchResultsButton = true
        searchBar.setShowsCancelButton(true, animated: true)
        searchBar.tintColorDidChange()
        searchBar.transform = CGAffineTransform(translationX: 0, y: -50)
        self.navigationItem.titleView = searchBar
        self.navigationItem.rightBarButtonItems = nil
//        createRightBarItem(title: "Tìm", selector: doneSelector, fontSize: 20)
        searchBar.delegate = self as? UISearchBarDelegate
    }
    
    func doRegister(user: UserModel) {
//        Utils.ref.child("User").child(Utils.userDefault.value(forKey: "loggedUserId") as! String).setValue(user.dictionaryRepresentation(), withCompletionBlock: { (error, data) in
//            if error != nil {
//                Utils.showAlertWithCompletion(title: "Lỗi", message: (error?.localizedDescription)!, viewController: self, completion: nil)
//            }else{
//                Utils.userDefault.set(user.dictionaryRepresentation(), forKey: "UserInfo")
//                Utils.userDefault.synchronize()
//            }
//        })
    }
    
    
    func updateUserToDatabase(User: UserModel) {
//        Utils.ref.child("User").child(Utils.userDefault.value(forKey: "loggedUserId") as! String).updateChildValues(User.dictionaryRepresentation(), withCompletionBlock: { (error, data) in
//            if let err = error {
//                Utils.showAlertWithCompletion(title: "Lỗi", message: err.localizedDescription, viewController: self, completion: nil)
//            } else {
//                Utils.userDefault.set(User.dictionaryRepresentation(), forKey: "UserInfo")
//                Utils.userDefault.synchronize()
//            }
//        })
    }
    
    
    
//    func signInWithCredentials(credentials: AuthCredential,email: String) {
//        SVProgressHUD.show(withStatus: "Đang đăng nhập...")
//        Auth.auth().signIn(with: credentials) { (user, err) in
//            if err != nil {
//                Utils.showAlertWithCompletion(title: "Lỗi", message: (err?.localizedDescription)!, viewController: self, completion: nil)
//                return
//            }
//
//            // Kiểm tra nếu user đã có trong database thì chỉ update không tạo mới
//            // Ngược lại tạo mới add vào database
//            var userModel = UserModel()
//            Utils.ref.child("User").observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
//                if !snapshot.hasChild((user?.uid)!) {
//                    userModel?.displayName = user?.displayName
//                    userModel?.provider = user?.providerID
//                    userModel?.avatar = user?.photoURL?.absoluteString
//                    userModel?.email = email
//                    self.loginWith(token: (user?.uid)!)
//                    userModel?.userID = (user?.uid)!
//                    self.doRegister(user: userModel!)
//                }else{
//                    userModel?.provider = user?.providerID
//                    userModel?.displayName = user?.displayName
//                    self.loginWith(token: (user?.uid)!)
//                    userModel?.userID = (user?.uid)!
//                    userModel?.email = email
//                    self.updateUserToDatabase(User: userModel!)
//                }
//            })
//        }
//    }
    
    func loginWith(token: String) {
        SVProgressHUD.dismiss()
        Utils.userDefault.set(token, forKey: "loggedUserId")
        let userID = Utils.getUserID()
//        Utils.ref.child("User").child(userID).child("isOnline").setValue(true)
        Utils.userDefault.synchronize()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let homeVC = mainStoryBoard.instantiateViewController(withIdentifier: "MasterController")
        appDelegate.changeRootView(homeVC)
    }
    
    
    func backWithoutTitle() -> UIBarButtonItem {
        let back = UIBarButtonItem()
        back.title = ""
        return back
    }
    
}
//
//  UIViewController.swift
//  neoLock
//
//  Created by pro on 9/30/17.
//  Copyright © 2017 bantayso.neoLock. All rights reserved.
//

import UIKit

enum gestureVisible {
    case visible,invisible
}


extension UIViewController {
    
    
    func internetAvailable() -> Bool {
        let networkStatus = NetworkStatus.sharedInstance
        return networkStatus.reachabilityManager?.networkReachabilityStatus == .notReachable ? false : true
    }
    
    func listenForKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: Notification.Name.UIKeyboardWillShow, object: nil)
    }
    
    
    @objc func keyboardWillDisappear() {
        hideKeyboardWhenTappedAround(gesture: .invisible)
    }
    
    @objc func keyboardWillAppear() {
        hideKeyboardWhenTappedAround(gesture: .visible)
    }
    
    func hideKeyboardWhenTappedAround(gesture: gestureVisible,completionHandler: (() -> Void)? = nil) {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        switch gesture {
        case .visible:
            view.addGestureRecognizer(tap)
        case .invisible:
            view.gestureRecognizers?.removeAll()
            completionHandler?()
        }
    }
    
    
    func hideBackButton() {
        self.navigationItem.hidesBackButton = true
    }
    
    func applyAttributedNAV() {
        self.navigationController?.navigationBar.tintColor = UIColor.mainColor()
        let nsAttributed = [NSAttributedStringKey.foregroundColor : UIColor.mainColor(),NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 20)]
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.titleTextAttributes = nsAttributed
            self.navigationController?.navigationBar.largeTitleTextAttributes = nsAttributed
        } else {
            self.navigationController?.navigationBar.titleTextAttributes = nsAttributed
        }
    }
    
    func setBackButton() {
        navigationItem.hidesBackButton = true
        navigationItem.leftBarButtonItem = backBTN()
    }
    
    
    
    @objc func popViewController() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.1, initialSpringVelocity: 1, options: .layoutSubviews, animations: {
            if self.isModal {
                self.dismiss(animated: true, completion: nil)
            }else{
                _ = self.navigationController?.popViewController(animated: true)
            }
        }, completion: nil)
        
    }
    
    var isModal: Bool {
        if let index = navigationController?.viewControllers.index(of: self), index > 0 {
            return false
        } else if presentingViewController != nil {
            return true
        } else if navigationController?.presentingViewController?.presentedViewController == navigationController  {
            return true
        } else if tabBarController?.presentingViewController is UITabBarController {
            return true
        } else {
            return false
        }
    }
    
    
    
    private func backBTN() -> UIBarButtonItem {
        let b = UIButton(type: .custom)
        b.setImage(UIImage(named: "arrowLeft")!.withRenderingMode(.alwaysTemplate), for: UIControlState.normal)
        let sizeB:CGFloat = 30
        b.frame = CGRect(x: 0, y: 0, width: sizeB, height: sizeB)
        b.addTarget(self, action: #selector(popViewController), for: UIControlEvents.touchUpInside)
        let backBTN = UIBarButtonItem(customView: b)
        return backBTN
    }
    
    
    
    func updateNAVTint(color: UIColor) {
        navigationController?.navigationBar.tintColor = color
    }
    
    
    //    func applyBackgroundGradient() {
    //        let colorTop = UIColor.init(red: 156, green: 219, blue: 204)
    //        let colorBottom = UIColor.init(red: 86, green: 197, blue: 238)
    //        view.applyGradient(withColours: [colorTop,colorBottom], gradientOrientation: .vertical)
    //    }
    
    
    func orderDayFromDate(inputDate:Date) -> NSInteger {
        
        let calendar:NSCalendar = NSCalendar(calendarIdentifier: .gregorian)!
        let timeZone:TimeZone = TimeZone(identifier: TimeZone.current.identifier)!
        
        calendar.timeZone = timeZone
        
        let calendarUnit = NSCalendar.Unit.weekday
        
        let theComponents:DateComponents = calendar.components(calendarUnit, from: inputDate)
        
        return theComponents.weekday!
    }
    
    
    func encodeDataWith<T>(object: T,filePath: String) {
        let manager = FileManager.default
        let url = manager.urls(for: .documentDirectory, in: .userDomainMask).first
        let filePaths = url!.appendingPathComponent(filePath).path
        NSKeyedArchiver.archiveRootObject(object, toFile: filePaths)
    }
    
    func decodeDataFrom<T>(filePath: String,completionHandler: @escaping (T)->()) {
        let manager = FileManager.default
        let url = manager.urls(for: .documentDirectory, in: .userDomainMask).first
        let filePaths = url!.appendingPathComponent(filePath).path
        if let ourData = NSKeyedUnarchiver.unarchiveObject(withFile: filePaths) as? T {
            completionHandler(ourData)
        }
    }
    
    func convertToCelsius(t:Float, source:String) -> Float? {
        switch source {
        case "Kelvin": return t - 273.15
        case "Celsius": return t
        case "Fahrenheit": return (t - 32) * 5 / 9
        case "Rankine": return (t - 491.67) * 5 / 9
        case "Delisle": return 100 - t * 2 / 3
        case "Newton": return t * 100 / 33
        case "Reaumur": return t * 5 / 4
        case "Romer": return (t - 7.5) * 40 / 21
        default: return nil
        }
    }
    
    func convertFromCelsius(t:Float, target:String) -> Float? {
        switch target {
        case "Kelvin":return t + 273.15
        case "Celsius": return t
        case "Fahrenheit": return t * 9 / 5 + 32
        case "Rankine": return (t + 273.15) * 9 / 5
        case "Delisle": return (100 - t) * 3 / 2
        case "Newton": return t * 33 / 100
        case "Reaumur": return t * 4 / 5
        case "Romer": return t * 21 / 40 + 7.5
        default: return nil
        }
    }
    
    func convertTemperatures(t:Float, source:String, target:String) -> Float {
        return convertFromCelsius(t: convertToCelsius(t: t, source: source)!, target: target)!
    }
    
    
    func pushDetail(_ viewControllerToPush: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionReveal
        transition.subtype = kCATransitionFromRight
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.pushViewController(viewControllerToPush, animated: false)
    }
    
    func dismissDetail() {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = kCATransitionReveal
        transition.subtype = kCATransitionFromLeft
        transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        
        self.navigationController?.popViewController(animated: false)
    }
    
    
    
    func clearNavigationColor() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
    }
    
    func whiteNavigationColor() {
        navigationController?.navigationBar.backgroundColor = UIColor.white
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = false
    }
    
    func lightStatusBar() {
        DispatchQueue.main.async {
            UIApplication.shared.statusBarStyle = .lightContent
        }
    }
    
    func defaultStatusBar() {
        DispatchQueue.main.async {
            UIApplication.shared.statusBarStyle = .default
        }
    }
    
    func drawLineSeparatorFor(cell: UITableViewCell,height: CGFloat) {
        let additionalSeparatorThickness = CGFloat(1)
        let additionalSeparator = UIView(frame: CGRect(x: 8, y: height - additionalSeparatorThickness, width: cell.frame.size.width - 16, height: additionalSeparatorThickness))
        additionalSeparator.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        cell.addSubview(additionalSeparator)
    }
    
    func animateCell(cell: UITableViewCell) {
        cell.transform = CGAffineTransform(translationX: 0, y: 50)
        cell.alpha = 0
        
        UIView.beginAnimations("rotation", context: nil)
        UIView.setAnimationDuration(0.5)
        cell.transform = CGAffineTransform(translationX: 0, y: 0)
        cell.alpha = 1
        UIView.commitAnimations()
    }
    
    
    func call(number: String) {
        if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    
    
    func customTabbarItem(title: String, image: String) -> UITabBarItem {
        return UITabBarItem(title: title, image: UIImage(named: image), selectedImage: nil)
    }
    
}
