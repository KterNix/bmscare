//
//  Config.swift
//  neoReal
//
//  Created by mac on 4/18/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
class Config {
    static let firstCustomerQueueColor = "#7d8b92"
    static let secondCustomerQueueColor = "#d39e00"
    static let thirdCustomerQueueColor = "#bd2130"
    static let absentColor = "FFC400"
    static let fixingColor = "FF9100"
    static let emptyColor = "9E9E9E"
    static let brokenColor = "DD2C00"
    static let liveInColor = "#0db498"

    //Socket Action
    static let Deposit = "DEPPOSIT"
    static let EndEvent = "END_EVENT"
    static let BookingCountdown = "BOOKING_COUNTDOWN"

    static let statusSend = "1"
    static let statusCancel = "-1"
    static let statusAccept = "2"
    static let statusDone = "3"
    static let statusEndEvent = "4"

}
