//
//  DetailHumanResource.swift
//  neoBMSCare
//
//  Created by mac on 7/26/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka
class DetailHumanResource: MasterFormController {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    var isCreateNew = false
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
        setUpForm()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func setUpForm() {
        form
            +++ Section("Thông tin")
            <<< TextRow(){
                $0.title = "Tên"
                $0.value = isCreateNew ? nil : "Nguyễn Kim Ngân"
            }
            <<< DateInlineRow(){
                $0.title = "Ngày sinh"
                $0.value = isCreateNew ? nil : Date()
            }
            <<< PhoneRow(){
                $0.title = "CMND/Hộ chiếu"
                $0.value = isCreateNew ? nil : "409817282"
            }
            <<< PhoneRow(){
                $0.title = "Điện thoại"
                $0.value = isCreateNew ? nil : "098271622"
            }
            <<< EmailRow(){
                $0.title = "Email"
                $0.value = isCreateNew ? nil : "kimngan@gmail.com"
            }
            +++ Section("Tuỳ chỉnh")
            <<< PickerInputRow<String>(){
                $0.title = "Chức vụ"
                $0.options = ["Giám đốc","Quản lý","Nhân viên"]
            }
            <<< PickerInlineRow<String>(){
                $0.title = "Khoá"
                $0.options = ["Có","Không"]
                $0.value = isCreateNew ? nil : "Không"
            }
            +++ Section("Quyền lợi"){section in
                    var footer = HeaderFooterView<UIView>(.callback({
                        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
                        let button = StyleGhost(frame: CGRect(x: 0, y: 0, width: self.view.width * 0.8, height: 40))
                        button.text = self.isCreateNew ? "Xong" : "Cập nhật"
                        button.center = view.center
                        button.tag = 2
                        view.addSubview(button)
                        return view
                    }))
                    footer.height = {50}
                    footer.onSetupView = {view, _ in
                        guard let button = view.viewWithTag(2) as? UIButton else {return}
                        button.addTarget(self, action: #selector(self.didPressDone), for: .touchUpInside)
                    }
                    section.footer = footer
            }
            <<< PickerInputRow<String>(){
                $0.title = "Bảo hiểm y tế năm"
                $0.value = isCreateNew ? nil : "Có"
                $0.options = ["Có","Không"]
            }
            <<< PickerInputRow<String>(){
                $0.title = "Bảo hiểm xã hội"
                $0.options = ["Có","Không"]
                $0.value = isCreateNew ? nil : "Có"
            }
            <<< PickerInputRow<String>(){
                $0.title = "Nghỉ phép năm"
                $0.options = Array(1...30).map{"\($0) ngày"}
                $0.value = isCreateNew ? nil : "12 ngày"
            }
        
    }
    //MARK:- SubFunction
    func setupUI() {
        setBackButton()
        self.title = isCreateNew ? "Tạo nhân sự":"Chi tiết nhân sự"
    }
    func setupDataSource() {
        
    }
    @objc func didPressDone() {
        
    }
    //MARK:- Outlet Actions
    

    

}
