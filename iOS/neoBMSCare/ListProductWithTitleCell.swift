//
//  ListProductWithTitleCell.swift
//  neoBMSCare
//
//  Created by mac on 8/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
private let idCell = "ProductCell"
class ListProductWithTitleCell: MasterCollectionViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var headerTitle: HeaderTitle!
    @IBOutlet weak var collectionProduct: UICollectionView!
    
    
    //MARK:- Declare
    var collectionProtocol:CollectionViewProtocol?
    var sizeItem: CGSize?
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setupUI() {
        collectionProduct.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        collectionProtocol = CollectionViewProtocol(sizeItem: sizeItem ?? CGSize(width: 200, height: 185), idCell: idCell)
        collectionProduct.delegate = collectionProtocol
        collectionProduct.dataSource = collectionProtocol
        
    }
    //MARK:- SubFunction
    
    func setupDataSource(title : String,sizeItem: CGSize? ,scrollDirection: UICollectionViewScrollDirection) {
        guard ((collectionProduct.collectionViewLayout as? UICollectionViewFlowLayout) != nil) else {return}
        (collectionProduct.collectionViewLayout as? UICollectionViewFlowLayout)!.scrollDirection = scrollDirection
        self.headerTitle.text = title
        self.headerTitle.setupDataSource()
        self.sizeItem = sizeItem
    }
    
    //MARK:- Outlet Actions

}

