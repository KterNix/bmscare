//
//  ProfileCell.swift
//  ApartmentManager
//
//  Created by pro on 7/27/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import Eureka
import SDWebImage



final class ProfileRow: Row<ProfileCell>, RowType {
    required init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<ProfileCell>(nibName: "ProfileCell")
    }
}

struct Profile: Equatable {
    var title: String
    var avatarUser: String
}
func ==(lhs: Profile, rhs: Profile) -> Bool {
    return lhs.title == rhs.title
}



class ProfileCell: Cell<Profile>, CellType {

    
    @IBOutlet weak var title: RecommentLabel!
    @IBOutlet weak var avatarUser: UIImageView!
    
    
    
    
    required init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    
    override func setup() {
        super.setup()


        avatarUser.clipsToBounds = true
        avatarUser.layer.cornerRadius = avatarUser.frame.height/2
        avatarUser.borderColor = .mainColor()
        avatarUser.borderWidth = 0.5
        title.color = .black
        height = CGFloat({return CFloat(115)}())
        // set a light background color for our cell
        backgroundColor = UIColor.white
    }

    
    override func update() {
        super.update()
        
        // we do not want to show the default UITableViewCell's textLabel
        textLabel?.text = nil
        
        // get the value from our row
        guard let user = row.value else { return }
        avatarUser.sd_setImage(with: URL(string: user.avatarUser), placeholderImage: nil, options: SDWebImageOptions.continueInBackground)
        // set the texts to the labels
        title.text = user.title
    }
    
    

}











