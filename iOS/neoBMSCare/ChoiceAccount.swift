//
//  ChoiceAccount.swift
//  neoBMSCare
//
//  Created by mac on 8/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ChoiceAccount: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func khach(_ sender: Any) {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let homeVC = mainStoryBoard.instantiateViewController(withIdentifier: "MasterController") as! MasterController
        homeVC.accountType = .User
        Utils.share.changeRootView(homeVC, completion_: nil)
    }
    @IBAction func quanly(_ sender: Any) {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let homeVC = mainStoryBoard.instantiateViewController(withIdentifier: "MasterController") as! MasterController
        homeVC.accountType = .Manager
        Utils.share.changeRootView(homeVC, completion_: nil)
    }
    
}
