//
//  DetailInboxController.swift
//  neoBMSCare
//
//  Created by mac on 8/2/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
private let idCellIn = "CommingCell"
private let idCellOut = "SendingCell"
class DetailInboxController: UITableViewController {
        
    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    let textIn = ["Hello there!", "I'm Nix","What's yr name?"]
    let textOut = ["Hi!" ,"I'm Biti","Nice too meet you! Nice too meet you! Nice too meet you!Nice too meet you!Nice too meet you!Nice too meet you!Nice too meet you!Nice too meet you!Nice too meet you!Nice too meet you!Nice too meet you!Nice too meet you!Nice too meet you!Nice too meet you!Nice too meet you!"]
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scrollToLastCell()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        let totalMessage = textIn + textOut
        return section == 0 ? textIn.count : textOut.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: idCellIn) as! CommingCell
            let text = textIn[indexPath.row]
            let index = textIn.index { (text1) -> Bool in
                text == text1
            }
            var position:PositionMessage = .middle
            if index == 0 {
                position = .top
            }else if index == textIn.count - 1{
                position = .bottom
            }
            cell.getData(chat: text, posotion: position)
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: idCellOut) as! SendingCell
            let text = textOut[indexPath.row]
            let index = textOut.index { (text1) -> Bool in
                text == text1
            }
            var position:PositionMessage = .middle
            if index == 0 {
                position = .top
            }else if index == textOut.count - 1{
                position = .bottom
            }
            cell.getData(chat: text, posotion: position)
            return cell
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.width, height: 60))
        headerView.backgroundColor = UIColor.white
        let statusTime = StatusLabel(frame: CGRect(x: 0, y: 0, width: 170, height: 25))
        statusTime.text = "13:20 27/06/2018"
        statusTime.color = UIColor.white
        statusTime.bgColor = UIColor.lightGray
        headerView.addSubview(statusTime)
        statusTime.center = headerView.center
        return headerView
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    //MARK:- SubFunction
    func setupUI() {
        title = "Nix"
        setBackButton()
        self.tableView.bounces = true
//        let footerField = FieldChatView(frame:CGRect(x: 0, y: view.height - 49
//            , width: view.width, height: 50))
//        self.navigationController?.view.addSubview(footerField)
//        self.navigationController?.view.bringSubview(toFront: footerField)
        self.tableView.tableFooterView?.height = 50
    }
    func setupDataSource() {
        self.tableView.register(UINib(nibName: idCellIn, bundle: nil), forCellReuseIdentifier: idCellIn)
        self.tableView.register(UINib(nibName: idCellOut, bundle: nil), forCellReuseIdentifier: idCellOut)
    }
    func scrollToLastCell() {
        let lastSection = 1
        let lastRow = textOut.count - 1
        self.tableView.scrollToRow(at: IndexPath(item: lastRow, section: lastSection), at: .bottom, animated: true)
    }
    //MARK:- Outlet Actions
    

    
}
