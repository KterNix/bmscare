//
//  PageBlocks.swift
//  neoReal
//
//  Created by pro on 4/19/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import DropDown



class PageBlocks: ButtonBarPagerTabStripViewController {

    
    var blocks : [BlockModel]?
    
    var blocksViewControllers = [UIViewController]()
    
    var dropDown = DropDown()

    var floorsList = [String]() {
        didSet {
            dropDown.dataSource = floorsList
            dropDown.reloadAllComponents()
        }
    }
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    
    lazy var floorImage = UIImage(named: "floorIcon")?.withRenderingMode(.alwaysTemplate)
    
    lazy var floorButton:UIButton = {
        let b = UIButton(type: UIButtonType.custom)
        //.init(hexString: "6F7179")
        b.setTitle("CHỌN TẦNG", for: UIControlState.normal)
        b.setTitleColor(UIColor.mainColor(), for: UIControlState.normal)
        b.titleLabel?.font = UIFont.boldSystemFont(ofSize: 10)
        b.tintColor = UIColor.mainColor()
        b.setImage(floorImage!, for: UIControlState.normal)
        let sizeB:CGFloat = 30
        b.frame = CGRect(x: 0, y: 0, width: 60, height: sizeB)
        b.addTarget(self, action: #selector(didTapFloor), for: UIControlEvents.touchUpInside)
        return b
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
//        setBackButton()
//        applyAttributedNAV()
//        whiteNavigationColor()
        title = "Blocks"
        
        let floor = UIBarButtonItem(customView: floorButton)
        navigationItem.rightBarButtonItems = [floor]
        // Do any additional setup after loading the view.
        setupDropdown()
    }
    
    
    fileprivate func setupDropdown() {
        dropDown.backgroundColor = UIColor.white
        dropDown.dataSource = floorsList
        
        dropDown.anchorView = floorButton
        let originMaxY = floorButton.frame.size.height
        dropDown.bottomOffset = CGPoint(x: 0, y: originMaxY)
        dropDown.separatorColor = UIColor.lightGray.withAlphaComponent(0.1)
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            let vc = (self.blocksViewControllers[self.currentIndex] as! BlockDetailController)
            vc.floor = item
            vc.dropdownHandle()
            self.dropDown.clearSelection()
        }
    }
    
    @objc fileprivate func didTapFloor() {
        dropDown.show()
    }
    
    fileprivate func setupViewControllers() {
        guard let blocks = Utils.currentBlocks else {return}
        self.blocks =  blocks
        guard blocks.count > 0 else {
            let blankController = BlockDetailController(nibName: "BlockDetailController", bundle: nil)
            blankController.itemInfo.title = "Không có dữ liệu"
            blocksViewControllers.append(blankController)
            return
        }
        blocksViewControllers.removeAll()
        for i in 0..<blocks.count {
            let blo = BlockDetailController(nibName: "BlockDetailController", bundle: nil)
            blocksViewControllers.append(blo)
            (blocksViewControllers[i] as! BlockDetailController).itemInfo.title = blocks[i].name
            (blocksViewControllers[i] as! BlockDetailController).block = blocks[i]
        }
        self.reloadPagerTabStripView()
    }
    
    private func configHouseForPage() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            guard let count = self.blocks?.count else {return}
            guard count > 0 else { return }
            self.floorsList.removeAll()
            if let floor = self.blocks![self.currentIndex].floors {
                for i in floor {
                    if let floorTitle = i.name {
                        
                        if !self.floorsList.contains(floorTitle) {
                            let all = "Tất cả"
                            if !self.floorsList.contains(all) {
                                self.floorsList.append(all)
                            }
                            self.floorsList.append(floorTitle)
                        }
                    }
                }
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        appDelegate.socketListen = nil
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupViewControllers()
    }
    func setupUI() {
        //MARK: Listen Socket
//        appDelegate.socketDelegate = self
//
//        
//        appDelegate.callBack = {}
//        
//        appDelegate.socketListen = {socketModel in
//            
//            if socketModel.payLoad?.projectCode == Utils.currentProject?.code && socketModel.status == Config.statusAccept {
//                guard var blocks = self.blocks else {return}
//                guard let indexBlock = blocks.index(where: { (block) -> Bool in
//                    block.code == socketModel.payLoad?.blockCode
//                }) else {return}
//                guard let indexFloor =  blocks[indexBlock].floors?.index(where: { (floor) -> Bool in
//                    floor.code == socketModel.payLoad?.floor
//                }) else {return}
//                guard let indexHouse = blocks[indexBlock].floors![indexFloor].houses?.index(where: { (house) -> Bool in
//                    house.id == socketModel.payLoad?.houseId
//                }) else {return}
//                blocks[indexBlock].floors![indexFloor].houses![indexHouse].status = HouseStatus.Deposited.rawValue
//                self.blocks = blocks
//                self.setupViewControllers()
//                self.reloadPagerTabStripView()
//                guard let vcPresent = self.presentingViewController as? ApartmentDetailPages else {return}
//                vcPresent.blocks = self.blocks
//            }
//        }
        self.buttonBarView.selectedBar.backgroundColor = UIColor.mainColor()
        self.buttonBarView.backgroundColor = UIColor.white
        self.settings.style.buttonBarBackgroundColor = UIColor.white
        self.settings.style.buttonBarItemBackgroundColor = UIColor.white
        self.settings.style.buttonBarItemTitleColor = UIColor.darkGray
        self.settings.style.buttonBarItemFont = UIFont.systemFont(ofSize: 12)
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
//            print(progressPercentage)
            guard changeCurrentIndex == true && progressPercentage == 1 else { return }
            self.configHouseForPage()
            
            oldCell?.label.textColor = UIColor.darkGray
            newCell?.label.textColor = UIColor.mainColor()
            if animated {
                UIView.animate(withDuration: 0.1, animations: { () -> Void in
                    newCell?.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                    oldCell?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                })
            }
        }
    }
    
    
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        return blocksViewControllers.isEmpty ? [self.createNewBlockViewController()] : blocksViewControllers
    }
    func createNewBlockViewController() -> UIViewController {
        let vc = BlockDetailController()
        vc.itemInfo = "Block A"
        return vc
    }
    deinit {
//        Utils.currentBlocks = nil
    }
    
}

//extension PageBlocks : SocketDelegate {
//    func listen(socketModel: SocketActionModel) {
//        if socketModel.payLoad?.projectCode == Utils.currentProject?.code && socketModel.status == Config.statusAccept {
//            guard var blocks = self.blocks else {return}
//
//            guard let indexBlock = blocks.index(where: { (block) -> Bool in
//                block.code == socketModel.payLoad?.blockCode
//            }) else {return}
//
//            guard let indexFloor =  blocks[indexBlock].floors?.index(where: { (floor) -> Bool in
//                floor.code == socketModel.payLoad?.floor
//            }) else {return}
//
//            guard let indexHouse = blocks[indexBlock].floors![indexFloor].houses?.index(where: { (house) -> Bool in
//                house.id == socketModel.payLoad?.houseId
//            }) else {return}
//
//            blocks[indexBlock].floors![indexFloor].houses![indexHouse].status = HouseStatus.Deposited.rawValue
//
//            self.blocks = blocks
//            self.setupViewControllers()
//            self.reloadPagerTabStripView()
//
//            guard let vcPresent = self.presentingViewController as? ApartmentDetailPages else {return}
//            vcPresent.blocks = self.blocks
//        }
//    }
//}
