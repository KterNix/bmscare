//
//  ReportController.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import Eureka

class ReportController: MasterFormController {

    
    
    
    
    
    
    
    var getReport = [String: String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setBackButton()
        // Do any additional setup after loading the view.
    }
    private func setupUI() {
        self.title = "Báo cáo sự cố"
        
        let component = ["Hệ thống điện","Hệ thống nước","Nội thất","Internet","Khác"]

        form +++
            Section("Chọn sự cố bạn đang gặp.")
            <<< PickerInlineRow<String>(){
                $0.title = "Sự cố"
                $0.options = component
                $0.value = $0.options.first
        }.onChange({ [unowned self] (picker) in
            
            guard let reportFromPicker = picker.value else { return }
            if reportFromPicker.contains("Khác") {
                self.sectionField(hidden: false)
                self.getReport["reportFromPicker"] = ""

            }else {
                self.sectionField(hidden: true)
                self.getReport["reportFromPicker"] = reportFromPicker
            }
            
        })
        +++ Section("Nhập thông tin sự cố."){
                $0.tag = "otherField"
                $0.hidden = true
                $0.evaluateHidden()
        }
            <<< TextAreaRow() {
                $0.title = "Sự cố"
                $0.placeholder = "Bạn gặp vấn đề gì ?"
            }.onChange({ [unowned self] (text) in
            guard let reportFromArea = text.value else { return }
            self.getReport["reportFromArea"] = reportFromArea
            })
        +++ Section(){section in
                var header = HeaderFooterView<UIView>(.callback({
                    let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
                    let button = StyleGhost(frame: CGRect(x: 0, y: 0, width: self.view.width * 0.8, height: 40))
                    button.text = "Gửi"
                    button.center = view.center
                    button.tag = 2
                    view.addSubview(button)
                    return view
                }))
                header.height = {50}
                header.onSetupView = {view, _ in
                    guard let button = view.viewWithTag(2) as? UIButton else {return}
                    button.addTarget(self, action: #selector(self.didPressSend), for: .touchUpInside)
                }
                section.header = header
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func didPressSend() {
        Utils.showAlertWithCompletion(title: "Đã gửi", message: "Thật tiếc vì sự cố này. Chúng tôi sẽ liên hệ lại trong thời gian sớm nhất", viewController: self, completion: {
            //Action
        })
    }
    func sectionField(hidden:Bool) {
        form.sectionBy(tag: "otherField")?.hidden = Condition(booleanLiteral: hidden)
        form.sectionBy(tag: "otherField")?.evaluateHidden()
        
    }


}













