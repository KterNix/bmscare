//
//  Notice.swift
//  ApartmentManager
//
//  Created by pro on 7/27/17.
//  Copyright © 2017 pro. All rights reserved.
//

import Foundation

struct Notice {
    var title:String = ""
    var isUserPost:Bool = false
    var date:String = ""
}
