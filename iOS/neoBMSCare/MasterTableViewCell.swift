//
//  MasterTableViewCell.swift
//  neoBMSCare
//
//  Created by mac on 7/10/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
protocol CellDelegate:class {
    func didSelect(tag:Int, indexSelected:IndexPath)
}
class MasterTableViewCell: UITableViewCell {
    var delegate:CellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        setupUI()
        setupDataSource()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setupUI() {
        
    }
    func setupDataSource() {
        
    }

}
