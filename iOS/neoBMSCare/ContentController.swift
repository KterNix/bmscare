//
//  ContentController.swift
//  neoBMSCare
//
//  Created by pro on 7/31/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit

class ContentController: UIViewController {

    
    
    @IBOutlet weak var imageSlide: UIImageView!
    
    
    var index:Int = 0
    var images = String()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDataSource()
        setupUI()
        // Do any additional setup after loading the view.
    }

    
    
    func setupUI() {
       setBackButton()
    }
    func setupDataSource() {
        self.imageSlide.image = UIImage(named: images)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
