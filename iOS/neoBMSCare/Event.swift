//
//  Event.swift
//  neoBMSCare
//
//  Created by pro on 7/28/17.
//  Copyright © 2017 pro. All rights reserved.
//

import Foundation

struct Event {
    var name:String = ""
    var address:String = ""
    var time:String = ""
    var image:String = ""
}
