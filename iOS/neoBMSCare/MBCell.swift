//
//  MBCell.swift
//  ApartmentManager
//
//  Created by pro on 7/27/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit

class MBCell: UITableViewCell {

    
    @IBOutlet weak var imageMB: UIImageView!
    @IBOutlet weak var titleMB: UILabel!
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var postDate: UILabel!
    @IBOutlet weak var priceMB: UILabel!
    @IBOutlet weak var typeMB: UILabel!
    
    
    
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var priceIcon: UIImageView!
    
    
    
    
    
    func configWithValue(t: Trading) {
        self.imageMB.image = UIImage(named: "\(t.image)")
        self.titleMB.text = t.title
        self.nameUser.text = t.name
        let getDate:Date = Utils.StringToDate(t.date)
        self.postDate.text = Utils.timeAgoSince(getDate)
        self.priceMB.text = "\(t.price)đ"
        if t.type.contains("Mua") {
            self.typeMB.text = "Cần Mua"
            self.typeMB.backgroundColor = UIColor.darkGray
            self.typeMB.textColor = UIColor.white
        }else if t.type.contains("Cần Bán") {
            self.typeMB.text = "Cần Bán"
            self.typeMB.backgroundColor = UIColor.red
            self.typeMB.textColor = UIColor.white
        }else if t.type.contains("Cho Thuê") {
            self.typeMB.text = "Cho Thuê"
            self.typeMB.backgroundColor = UIColor.mainColor()
            self.typeMB.textColor = UIColor.white
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        // Initialization code
    }
    private func setupUI() {
        self.userIcon.tintColor = UIColor.lightGray
        self.userIcon.image = UIImage(named: "person")?.withRenderingMode(.alwaysTemplate)
        self.priceIcon.tintColor = UIColor.lightGray
        self.priceIcon.image = UIImage(named: "money")?.withRenderingMode(.alwaysTemplate)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
