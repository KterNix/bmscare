//
//  DetailBookingController.swift
//  neoBMSCare
//
//  Created by mac on 7/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka
class DetailBookingController: MasterFormController {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        createRightBarItem(title: "✓", selector: #selector(self.didPressDone))
        setBackButton()
        self.title = "Chi tiết dịch vụ"
        form
        +++ Section("Dịch vụ")
            <<< LabelRow() {
                $0.title = "Loại"
                $0.value = "Hồ bơi"
            }
            <<< LabelRow() {
                $0.title = "Phí"
                $0.value = "300.000 VNĐ"
            }
            <<< LabelRow() {
                $0.title = "Gói"
                $0.value = "1 Năm"
            }
            <<< LabelRow() {
                $0.title = "Nhà tài trợ"
                $0.value = "Vinperl Land"
            }
            
        +++ Section("Khách")
            <<< LabelRow() {
                $0.title = "Tên"
                $0.value = "Huỳnh Kim Mến"
            }
            <<< LabelRow() {
                $0.title = "SĐT"
                $0.value = "098712322"
            }
            <<< LabelRow() {
                $0.title = "CMND/Hộ chiếu"
                $0.value = "280976156"
            }
            <<< LabelRow() {
                $0.title = "Căn hộ"
                $0.value = "Block A - 408"
            }
        +++ Section("Thời gian")
            <<< LabelRow() {
                $0.title = "Đặt dịch vụ lúc"
                $0.value = "18:30 27/06/2018"
            }
            <<< LabelRow() {
                $0.title = "Bắt đầu tham gia"
                $0.value = "01/07/2018"
            }
            <<< LabelRow() {
                $0.title = "Hết hạn"
                $0.value = "01/07/2019"
            }
            +++ Section(){section in
                var header = HeaderFooterView<UIView>(.callback({
                    let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
                    let button = StyleGhost(frame: CGRect(x: 0, y: 0, width: self.view.width * 0.8, height: 40))
                    button.text = "Chấp nhận"
                    button.center = view.center
                    button.tag = 2
                    view.addSubview(button)
                    return view
                }))
                header.height = {50}
                header.onSetupView = {view, _ in
                    guard let button = view.viewWithTag(2) as? UIButton else {return}
                    button.addTarget(self, action: #selector(self.didPressAccept), for: .touchUpInside)
                }
                section.header = header
        }
            +++ Section(){section in
                var header = HeaderFooterView<UIView>(.callback({
                    let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
                    let button = StyleGhost(frame: CGRect(x: 0, y: 0, width: self.view.width * 0.8, height: 40))
                    button.text = "Không chấp nhận"
                    button.color = .red
                    button.center = view.center
                    button.tag = 2
                    view.addSubview(button)
                    return view
                }))
                header.height = {50}
                header.onSetupView = {view, _ in
                    guard let button = view.viewWithTag(2) as? UIButton else {return}
                    button.addTarget(self, action: #selector(self.didPressCancel), for: .touchUpInside)
                }
                section.header = header
            }
            +++ Section("Lý do không chấp nhận"){
                $0.tag = "cancelReason"
                $0.hidden = true
                $0.evaluateHidden()
            }
            <<< TextAreaRow(){
                $0.tag = "fieldReason"
                }.onChange({ (text) in
                
            })
    }
    func setupDataSource() {
        
    }
    @objc func didPressAccept() {
        self.hidenCancelResonSection(value: true)
    }
    @objc func didPressCancel() {
        self.hidenCancelResonSection(value: false)
    }
    @objc func didPressDone() {
        
    }
    func hidenCancelResonSection(value: Bool) {
        let section = self.form.sectionBy(tag: "cancelReason")
        section?.hidden = Condition(booleanLiteral: value)
        section?.evaluateHidden()
        if !value {
            self.tableView.scrollToRow(at: IndexPath(row: 0, section: 5), at: .bottom, animated: true)
            let row = self.form.rowBy(tag: "fieldReason") as! TextAreaRow
            row.cell.textView.becomeFirstResponder()
        }
        
    }
    //MARK:- Outlet Actions
}
