//
//  PopUpDetailAbsent.swift
//  neoBMSCare
//
//  Created by mac on 7/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class PopUpDetailAbsent: MasterViewController {

    
    //MARK:- Outlet
    @IBOutlet weak var btnExit: StyleDefault!
    @IBOutlet weak var avatarCustomer: UIImageView!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var titleReason: NormalLabel!
    @IBOutlet weak var titleCodeHouse: NormalLabel!
    @IBOutlet weak var codeHouse: TitleLabel!
    @IBOutlet weak var timeBooking: NormalLabel!
    @IBOutlet weak var titileTimeGoOut: NormalLabel!
    @IBOutlet weak var timeGoOut: RecommentLabel!
    @IBOutlet weak var timeComeback: RecommentLabel!
    @IBOutlet weak var titleComeBack: NormalLabel!
    @IBOutlet weak var nameCustomer: RecommentLabel!
    @IBOutlet weak var phoneCustomer: NormalLabel!
    @IBOutlet weak var btnCancel: StyleDefault!
    @IBOutlet weak var btnAccept: StyleDefault!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.avatarCustomer.cornerRadius = self.avatarCustomer.height/2
        self.avatarCustomer.layer.borderColor = UIColor.mainColor().cgColor
        self.avatarCustomer.layer.borderWidth = 0.5
    }
    //MARK:- SubFunction
    override func setupUI() {
        self.phoneCustomer.color = UIColor.black
        self.timeBooking.color = .lightGray
        
        
        self.leftView.cornerRadius = 5
        self.leftView.dropShadow()
        self.titleCodeHouse.color = .black
        self.codeHouse.color = .mainColor()

        
        self.rightView.cornerRadius = 5
        self.rightView.dropShadow()
        self.timeComeback.color = .mainColor()
        self.titileTimeGoOut.color = .black
        
        self.centerView.cornerRadius = 5
        self.centerView.dropShadow()
        self.titleReason.color = .black
        self.textView.font = UIFont.recommentFont
        self.textView.textColor = .black
        
        
        self.view.dropShadow()
        
        self.btnExit.textColor = .mainColor()
        self.btnExit.setUpWithBackground(color: .white)
        self.btnCancel.setUpWithBackground(color: UIColor.lightGray)
        self.btnAccept.setUpWithBackground(color: .mainColor())
    }
    override func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions
    @IBAction func abtnExit(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    @IBAction func abtnCancel(_ sender: Any) {
        
    }
    @IBAction func abtnAccept(_ sender: Any) {
        
    }
    
}
