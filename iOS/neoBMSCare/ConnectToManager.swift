//
//  ConnectToManager.swift
//  neoBMSCare
//
//  Created by mac on 8/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import PopupDialog
private let idCell = "ConnectCell"
class ConnectToManager: UICollectionViewController {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    private var indexOfCellBeforeDragging = 0
    private var collectionViewFlowLayout: UICollectionViewFlowLayout {
        return collectionViewLayout as! UICollectionViewFlowLayout
    }
    var didLayoutSubView = false
    let titles = ["Báo vắng","Sửa chữa","Tiện ích"] //,"Shop"
    let icons = ["notice","settings","service"]//,"cart"
    let descriptions = ["Báo cho Ban Quản Lý bạn sẽ vắng nhà trong thời gian sắp tới.","Báo cho Ban Quản Lý bạn cần sửa chữa căn hộ của bạn.","Các tiện ích tại chung cư của bạn.","Với những sản phẩm giảm giá cực kỳ hấp dẫn, thân thuộc với mọi khách hàng."]
    let buttonTitle = ["Gửi ngay","Yêu cầu ngay","Đăng ký","Xem thêm"]
    //MARK:- Override
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titles.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! ConnectCell
        cell.setupDataSource(title: titles[indexPath.row], image: icons[indexPath.row],description: descriptions[indexPath.row],titleButton:buttonTitle[indexPath.row])
        cell.btnView.tag = indexPath.row
        cell.btnView.addTarget(self, action: #selector(self.didPress(button:)), for: .touchUpInside)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
//        collectionViewFlowLayout.minimumLineSpacing = 0
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if !didLayoutSubView {
            self.view.layoutIfNeeded()
            self.collectionView?.collectionViewLayout.invalidateLayout()
            didLayoutSubView = !didLayoutSubView
        }
        
//        configureCollectionViewLayoutItemSize()
    }
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        indexOfCellBeforeDragging = indexOfMajorCell()
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
//        // Stop scrollView sliding:
//        targetContentOffset.pointee = scrollView.contentOffset
//
//        // calculate where scrollView should snap to:
//        let indexOfMajorCell = self.indexOfMajorCell()
//
//        // calculate conditions:
//        let dataSourceCount = collectionView(collectionView!, numberOfItemsInSection: 0)
//        let swipeVelocityThreshold: CGFloat = 0.3 // after some trail and error
//        let hasEnoughVelocityToSlideToTheNextCell = indexOfCellBeforeDragging + 1 < dataSourceCount && velocity.x > swipeVelocityThreshold
//        let hasEnoughVelocityToSlideToThePreviousCell = indexOfCellBeforeDragging - 1 >= 0 && velocity.x < -swipeVelocityThreshold
//        let majorCellIsTheCellBeforeDragging = indexOfMajorCell == indexOfCellBeforeDragging
//        let didUseSwipeToSkipCell = majorCellIsTheCellBeforeDragging && (hasEnoughVelocityToSlideToTheNextCell || hasEnoughVelocityToSlideToThePreviousCell)
//
//        if didUseSwipeToSkipCell {
//            let snapToIndex = indexOfCellBeforeDragging + (hasEnoughVelocityToSlideToTheNextCell ? 1 : -1)
//            let indexPath = IndexPath(row: snapToIndex, section: 0)
//            UIView.animate(withDuration: 0.3) {
//                self.collectionViewLayout.collectionView!.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
//            }
//
//
//        } else {
//            // This is a much better way to scroll to a cell:
//            let indexPath = IndexPath(row: indexOfMajorCell, section: 0)
//            UIView.animate(withDuration: 0.3) {
//                self.collectionViewLayout.collectionView!.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
//            }
//        }
    }
    
    
    //MARK:- SubFunction
//    func calculateSectionInset() -> CGFloat { // should be overridden
//        return 30
//    }
    func setupUI() {
        self.title = "Dịch vụ"
        self.extendedLayoutIncludesOpaqueBars = true

    }
    func setupDataSource() {
        self.collectionView?.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
    }

//    private func configureCollectionViewLayoutItemSize() {
//        let inset: CGFloat = calculateSectionInset()
//        collectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
//
//        collectionViewFlowLayout.itemSize = CGSize(width: collectionViewLayout.collectionView!.frame.size.width - ((inset * 2) + 30), height: 350)
//    }
//
//    private func indexOfMajorCell() -> Int {
//        let itemWidth = collectionViewFlowLayout.itemSize.width
//        let proportionalOffset = collectionViewLayout.collectionView!.contentOffset.x / itemWidth
//        let index = Int(round(proportionalOffset))
//        let numberOfItems = collectionView?.numberOfItems(inSection: 0)
//        let safeIndex = max(0, min(numberOfItems! - 1, index))
//        return safeIndex
//    }
    @objc func didPress(button:UIButton) {
        var vc = UIViewController()
        let tag = button.tag
        switch tag {
        case 0:
            vc = AbsentController.getViewController()
        case 1:
            vc = ReportController.getViewController()
            
        case 2:
            vc = ExtrasController.getViewController()
        case 3:
            vc = ShopController()
        default:
            break
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func createFormService() {
        let vc = FormCreateService()
        let popUp = PopupDialog(viewController: vc, buttonAlignment: .horizontal, transitionStyle: .zoomIn, preferredWidth: self.view.width, gestureDismissal: true, hideStatusBar: true) {
            
        }
        
        let okBtn =  DefaultButton(title: "Xong") {
            
        }
        okBtn.backgroundColor = .mainColor()
        okBtn.setTitleColor(.white, for: .normal)
        let cancelBtn = CancelButton(title: "Huỷ") {
            
        }
        cancelBtn.backgroundColor = UIColor.groupTableViewBackground
        cancelBtn.setTitleColor(.gray, for: .normal)
        popUp.addButtons([cancelBtn,okBtn])
        self.present(popUp, animated: true, completion: nil)
    }
    
    //MARK:- Outlet Actions

}
extension ConnectToManager : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.width - 15)/2, height: (collectionView.height - (Constants.HeightTool.total + 15))/2)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}


