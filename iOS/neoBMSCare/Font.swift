//
//  Font.swift
//  POS
//
//  Created by mac on 4/3/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
extension UIFont{
    static let titleFont = UIFont.systemFont(ofSize: 20, weight: .thin)
    static let backButtonFont = UIFont.systemFont(ofSize: 30, weight: .thin)
    static let buttonFont = UIFont.systemFont(ofSize: 13, weight: .thin)
    static let textFont = UIFont.systemFont(ofSize: 12, weight: .thin)
    static let recommentFont = UIFont.systemFont(ofSize: 13, weight: .bold)
    static let recommentDiscountFont = UIFont.systemFont(ofSize: 15, weight: .thin)

}
