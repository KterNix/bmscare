//
//  SendingCell.swift
//  neoBMSCare
//
//  Created by mac on 8/2/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class SendingCell: MasterTableViewCell {

    
    //MARK:- Outlet
//    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var content: RecommentLabel!
    @IBOutlet weak var time: NormalLabel!
    @IBOutlet weak var widthSmallerThanContraint: NSLayoutConstraint!
    
    //MARK:- Declare
    var position:PositionMessage = .top
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    override func setupUI() {
//        avatar.cornerRadius = avatar.height/2
//        avatar.borderColor = .mainColor()
//        avatar.borderWidth = 0.5
        content.color = .white
        parentView.backgroundColor = .mainColor()
        time.color = .lightGray
        widthSmallerThanContraint.constant = width - 40
    }
    override func setupDataSource() {
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutIfNeeded()
        layoutView()
    }
    
    //MARK:- SubFunction
    func getData(chat:String, posotion:PositionMessage) {
        self.content.text = chat
        self.position = posotion
        self.time.isHidden = position == .bottom ? false : true
    }
    func layoutView() {
        parentView.cornerRadius = 10
        parentView.dropShadow()
        
//        switch self.position {
//        case .middle:
//            self.cornerForMiddle()
//        default:
//            self.cornerForTopAndButton(position: position)
//        }
       
//        self.avatar.isHidden = position == .top ? false : true


    }
    func cornerForTopAndButton(position: PositionMessage) {
        let corners:UIRectCorner = position == .top ? [.topLeft,.topRight] : [.bottomLeft,.bottomRight]
//        self.parentView.roundCorners(corners, radius: 15)
        

    }
    func cornerForMiddle() {
        self.parentView.cornerRadius = 10
//        self.avatar.isHidden = true
    }
    
    //MARK:- Outlet Actions
    
}
