//
//  LeftCell.swift
//  ApartmentManager
//
//  Created by pro on 7/26/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit

class LeftCell: UITableViewCell {

    
    
    @IBOutlet weak var imageList: UIImageView!
    @IBOutlet weak var itemList: RecommentLabel!
    

    //MARK:- Outlet
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setupUI()
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK:- SubFunction
    func setupUI() {
     self.itemList.color = .black
        self.accessoryType = .disclosureIndicator
    }
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions

}
