//
//  TaskCell.swift
//  neoBMSCare
//
//  Created by mac on 7/3/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class TaskCell: MasterTableViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var codeHouseLBL: RecommentLabel!
    @IBOutlet weak var titleTask: RecommentLabel!
    @IBOutlet weak var statusTask: StatusLabel!
    @IBOutlet weak var nameEmployer: RecommentLabel!
    @IBOutlet weak var createDate: RecommentLabel!
    
    
    //MARK:- Declare
    let colors:[UIColor] = [
        UIColor.init(hexString: Config.absentColor),
        UIColor.init(hexString: Config.emptyColor),
        UIColor.init(hexString: Config.fixingColor),
        UIColor.init(hexString: Config.brokenColor),
        UIColor.init(hexString: Config.liveInColor)
    ]
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK:- SubFunction
    override func setupUI() {
       self.codeHouseLBL.color = .black
        self.nameEmployer.color = .black
        self.createDate.color = .lightGray
        self.statusTask.bgColor = colors.shuffled().first
        
    }
    override func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions
    
}
