//
//  ProductCell.swift
//  neoBMSCare
//
//  Created by mac on 8/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ProductCell: MasterCollectionViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var nameProduct: RecommentLabel!
    @IBOutlet weak var priceProduct: NormalLabel!
    @IBOutlet weak var priceDiscountProduct: RecommentDiscountLabel!
    @IBOutlet weak var percentDiscount: RecommentLabel!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setupUI() {
        percentDiscount.color = .white
        priceProduct.color = .lightGray
        let attributeString = NSMutableAttributedString(string: "10.000.000 VNĐ")
        attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 1, range: NSRange(location: 0, length: attributeString.length))
        attributeString.addAttribute(NSAttributedStringKey.strikethroughColor, value: UIColor.lightGray, range: NSRange(location: 0, length: attributeString.length))
        priceProduct.attributedText = attributeString
        dropShadow()
        
    }
    //MARK:- SubFunction
    
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions

}
