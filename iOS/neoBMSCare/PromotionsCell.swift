//
//  PromotionsCell.swift
//  neoBMSCare
//
//  Created by mac on 8/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class PromotionsCell: MasterCollectionViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    //MARK:- Declare
    let idCell = "ImageCell"
    var imageDataSource:PromotionsSliderProtocol?
    let namePromotion = ["promotion1.jpg","promotion2.jpg","promotion3.jpg"]
    var images:[UIImage]?{
        didSet{
            collectionView.reloadData()
        }
    }
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    override func setupUI() {
        self.pageControl.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        images = namePromotion.map{UIImage(named: $0)!}
        self.pageControl.pageIndicatorTintColor = UIColor.white
        self.pageControl.currentPageIndicatorTintColor = UIColor.mainColor()
        imageDataSource = PromotionsSliderProtocol(datasrc: images == nil ? []: images!, path: "path")
        imageDataSource?.willDisplay = { [weak self] index in
            self?.pageControl.currentPage = index
        }
        imageDataSource?.turnOnSlider()
        collectionView.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        collectionView.delegate = imageDataSource
        collectionView.dataSource = imageDataSource
        
    }
    //MARK:- SubFunction
    
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions

}
