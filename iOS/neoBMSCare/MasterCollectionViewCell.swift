//
//  MasterCollectionViewCell.swift
//  neoBMSCare
//
//  Created by mac on 7/10/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class MasterCollectionViewCell: UICollectionViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        setDefault()
        setupUI()
    }
    func setDefault() {
        self.layer.cornerRadius = 5
    }
    func setupUI() {
        
    }
    
}
