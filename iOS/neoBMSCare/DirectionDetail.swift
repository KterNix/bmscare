//
//  DirectionDetail.swift
//  neoBMSCare
//
//  Created by pro on 7/29/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit

class DirectionDetail: UIViewController {

    
    
    
    @IBOutlet weak var myTableView: UITableView!
    
    
    var directionDetail = NSArray()
    var directionInfo = NSDictionary()
    var lblSrcDest = UILabel()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myTableView.estimatedRowHeight = 44
        self.myTableView.rowHeight = UITableViewAutomaticDimension
        self.navigationItem.prompt = self.directionInfo .object(forKey: "end_address") as? String
        self.navigationItem.title = self.directionInfo .object(forKey: "start_address") as? String
        self.directionDetail = directionInfo.object(forKey: "steps") as! NSArray
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
extension DirectionDetail: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int{
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if section == 0 {
            return 1
        }
        return self.directionDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "directionDetailCell", for: indexPath) as! DirectionDetailCell
        if indexPath.section == 0 {
            cell.dirextionDescription.text = NSString(format:"total Distace = %@ \ntotal Duration = %@",directionInfo.value(forKey: "distance")as! NSString,directionInfo.value(forKey: "duration")as! NSString) as String
            cell.directionDetail.text = NSString(format:"Driving directions \nfrom \n%@ \nto \n%@",directionInfo.value(forKey: "start_address")as! NSString,directionInfo.value(forKey: "end_address")as! NSString) as String
        }else{
            let idx:Int = indexPath.row
            let dictTable:NSDictionary = self.directionDetail[idx] as! NSDictionary
            cell.directionDetail.text =  dictTable["instructions"] as? String
            let distance = dictTable["distance"] as! NSString
            let duration = dictTable["duration"] as! NSString
            let detail = "distance : \(distance) duration : \(duration)"
            cell.dirextionDescription.text = detail
            cell.selectionStyle = UITableViewCellSelectionStyle.none
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (section == 0) {
            return "Driving directions Summary"
        }else{
            return "Driving directions Detail"
        }
    }
}















