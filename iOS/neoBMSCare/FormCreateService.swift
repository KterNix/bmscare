//
//  FormCreateService.swift
//  neoBMSCare
//
//  Created by mac on 7/27/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka
import PopupDialog
private let idCell = "ResponsibleCell"
class FormCreateService: MasterFormController {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    //MARK:- SubFunction
    func setupUI() {
        
    }
    func setupDataSource() {
        form
        +++ Section()
            <<< PickerInputRow<String>(){
                $0.title = "Loại dịch vụ"
                $0.options = ["Sửa chữa nội thất","Sửa chữa thiết bị điện","Sửa chữa vật liệu xây dựng"]
            }
            <<< TextAreaRow(){
                $0.value = "Sữa chữa những nội thất trong nhà. VD: Ghế sofa, Bàn làm việc, Giường ngủ,..."
            }
            <<< DateInlineRow(){
                $0.title = "Ngày sửa chữa"
               
            }
            <<< DecimalRow(){
                $0.title = "Phí"
                
                }
            <<< DecimalRow(){
                    $0.title = "Đặt cọc"
                
                    }
//            <<< PickerInputRow<String>(){
//                $0.title = "Đảm nhận bởi"
//                $0.value = "Huỳnh Kim Vân"
//                $0.options = ["Huỳnh Kim Vân","Bá Hữu Phước","Bùi Văn Hến","Nguyễn Ngọc An","Trần Hải Vương"]
//            }
            +++ Section(){section in
                //Footer Người đảm nhiệm
                var footer = HeaderFooterView<DetailHouseInforHeader>(.nibFile(name:"DetailHouseInforHeader",bundle:nil))
                footer.onSetupView = {view, _ in
                    view.title.text = "Người đảm nhiệm"
                    view.setupUI()
                    view.btnMore.text = "Thêm"
                    view.btnMore.addTarget(self, action: #selector(self.didPressAddMoreResponsible), for: .touchUpInside)
                    view.collectionView.backgroundColor = UIColor.groupTableViewBackground
                    view.collectionView.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
                    view.collectionView.delegate = self
                    view.collectionView.dataSource = self
                    
                }
                footer.height = {200}
                section.footer = footer
        }
    }
    
    @objc func  didPressAddMoreResponsible() {
        
        let vc = HumanResourceController(nibName: "HumanResourceController", bundle: nil)
        vc.collectionView?.frame = CGRect(x: 0, y: 0, width: 335, height: 400)
        let heightContraint = NSLayoutConstraint(item:vc.view , attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 400)
        vc.view.addConstraint(heightContraint)
        vc.collectionView?.allowsMultipleSelection = true
        let cancel = CancelButton(title: "Huỷ") {
            
        }
        let accept = DefaultButton(title: "Xong") {
            
        }
        cancel.setTitleColor(.lightGray, for: .normal)
        cancel.backgroundColor = UIColor.groupTableViewBackground
        accept.setTitleColor(.white, for: .normal)
        accept.backgroundColor = .mainColor()
        let popUp = PopupDialog(viewController: vc)
        popUp.buttonAlignment = .horizontal
        popUp.addButtons([cancel,accept])
        self.present(popUp, animated: true, completion: nil)
    }
    //MARK:- Outlet Actions
    

}
extension FormCreateService : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! ResponsibleCell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: collectionView.frame.height * 0.8)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
