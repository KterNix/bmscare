//
//  NewProductsProtocal.swift
//  neoBMSCare
//
//  Created by mac on 8/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
class CollectionViewProtocol: NSObject {
    var idCell = ""
    var sizeItem = CGSize()
    init(sizeItem :CGSize ,idCell:String){
        self.idCell = idCell
        self.sizeItem = sizeItem
    }
}
extension CollectionViewProtocol : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numerItemsInHeight = Int(collectionView.height)/Int(sizeItem.height)
        return CGSize(width: sizeItem.width, height: (collectionView.height - CGFloat(5*numerItemsInHeight + 5))/CGFloat(numerItemsInHeight))
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}

