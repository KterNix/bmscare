//
//  DetailHistoryMaintence.swift
//  neoBMSCare
//
//  Created by mac on 8/1/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka
import PopupDialog
private let idCell = "ResponsibleCell"
class DetailHistoryMaintence: MasterFormController {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    var times = 0
    var titleText = ""

    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
        setUpForm()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func setUpForm() {
        form
        +++ Section("Thông tin")
        {
            $0.hidden = self.isCreateNote() ? true : false
            $0.evaluateHidden()
        }
            <<< LabelRow(){
                $0.title = "Lần sửa"
                $0.value = "Thứ \(times)"
                
            }
            <<< LabelRow(){
                $0.title = "Người tạo"
                $0.value = "Nguyễn Hữu Bình"
            }
            
        +++ Section("Thời gian")
            <<< LabelRow(){
                $0.title = "Ngày tạo"
                $0.value = "10:30 01/02/2017"
                $0.hidden = self.isCreateNote() ? true : false
                $0.evaluateHidden()
            }
            <<< DateTimeRow(){
                $0.title = "Ngày bắt đầu"
                $0.value = Date(timeIntervalSince1970: 1496384100)
            }
            <<< DateTimeRow(){
                $0.title = "Ngày kết thúc"
                $0.value = Date(timeIntervalSince1970: 1497075300)
                $0.hidden = self.isCreateNote() ? true : false
                $0.evaluateHidden()
            }
            +++ Section(){section in
                //Footer Người đảm nhiệm
                var footer = HeaderFooterView<DetailHouseInforHeader>(.nibFile(name:"DetailHouseInforHeader",bundle:nil))
                footer.onSetupView = {view, _ in
                    view.title.text = "Người đảm nhiệm"
                    view.setupUI()
                    view.btnMore.text = "Thêm"
                    view.btnMore.addTarget(self, action: #selector(self.didPressAddMoreResponsible), for: .touchUpInside)
                    view.collectionView.backgroundColor = UIColor.groupTableViewBackground
                    view.collectionView.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
                    view.collectionView.delegate = self
                    view.collectionView.dataSource = self
                    
                }
                footer.height = {200}
                section.footer = footer
            }
        +++ Section(){section in
                var header = HeaderFooterView<UIView>(.callback({
                    let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
                    let button = StyleGhost(frame: CGRect(x: 0, y: 0, width: self.view.width * 0.8, height: 40))
                    button.text = "Hoàn thành"
                    button.center = view.center
                    button.tag = 2
                    view.addSubview(button)
                    return view
                }))
                header.height = {50}
                header.onSetupView = {view, _ in
                    guard let button = view.viewWithTag(2) as? UIButton else {return}
                    button.addTarget(self, action: #selector(self.didPressDone), for: .touchUpInside)
                }
                section.header = header
            section.hidden = self.isCreateNote() ? true : false
            section.evaluateHidden()
        }
        +++ Section("Bình luận"){section in
            var footer = HeaderFooterView<CommentFooter>(.nibFile(name:"CommentFooter",bundle:nil))
            footer.onSetupView = {view, _ in
                    
                }
            footer.height = {
                    self.view.frame.height - (Constants.HeightTool.tabbar + Constants.heightSubmitComment)
                }
            section.footer = footer
            
            //------------
            var header = HeaderFooterView<SubmitComment>(.nibFile(name:"SubmitComment",bundle:nil))
            header.onSetupView = {view, _ in
                view.setupDataSource(textTitle: "Bình luận")
            }
            header.height = {Constants.heightSubmitComment}
            section.header = header
            
            //-----------
            section.hidden = self.isCreateNote() ? true : false
            section.evaluateHidden()
        }
        
    }
    //MARK:- SubFunction
    func setupUI() {
        self.title = self.isCreateNote() ? titleText : "Chi tiết lịch sử"
        self.setBackButton()
    }
    func setupDataSource() {
        
    }
    @objc func didPressDone() {
        
    }
    @objc func  didPressAddMoreResponsible() {
        
        let vc = HumanResourceController(nibName: "HumanResourceController", bundle: nil)
        vc.collectionView?.frame = CGRect(x: 0, y: 0, width: 335, height: 400)
        let heightContraint = NSLayoutConstraint(item:vc.view , attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 400)
        vc.view.addConstraint(heightContraint)
        vc.collectionView?.allowsMultipleSelection = true
        let cancel = CancelButton(title: "Huỷ") {
            
        }
        let accept = DefaultButton(title: "Xong") {
            
        }
        cancel.setTitleColor(.lightGray, for: .normal)
        cancel.backgroundColor = UIColor.groupTableViewBackground
        accept.setTitleColor(.white, for: .normal)
        accept.backgroundColor = .mainColor()
        let popUp = PopupDialog(viewController: vc)
        popUp.buttonAlignment = .horizontal
        popUp.addButtons([cancel,accept])
        self.present(popUp, animated: true, completion: nil)
    }
    func isCreateNote() -> Bool {
        let bool =  titleText.contains("Tạo")
        return bool
    }
    //MARK:- Outlet Actions

}
extension DetailHistoryMaintence : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.isCreateNote() ? 0 : 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! ResponsibleCell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: collectionView.frame.height * 0.8)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
