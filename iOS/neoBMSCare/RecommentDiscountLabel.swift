//
//  RecommentDiscountLabel.swift
//  POS
//
//  Created by mac on 4/4/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class RecommentDiscountLabel: UILabel {
    var color:UIColor?{
        didSet{
            setupUI()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    func setupUI() {
        self.textColor = color ?? UIColor.red
        self.font = UIFont.recommentDiscountFont
        
    }
}
