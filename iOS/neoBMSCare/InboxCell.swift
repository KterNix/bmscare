//
//  InboxCell.swift
//  neoBMSCare
//
//  Created by mac on 7/26/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class InboxCell: MasterCollectionViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var avatar: UIImageView!
    
    @IBOutlet weak var nameUser: RecommentLabel!
    
    @IBOutlet weak var lastMessage: NormalLabel!
    
    @IBOutlet weak var timeAgo: NormalLabel!
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layoutIfNeeded()
        avatar.makeCircle = true
        avatar.layer.borderColor = UIColor.mainColor().cgColor
        avatar.layer.borderWidth = 0.5
    }
    //MARK:- SubFunction
    override func setupUI() {
        self.dropShadow()
        self.timeAgo.color = .lightGray
        
    }
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions

}
