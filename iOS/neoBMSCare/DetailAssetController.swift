//
//  DetailAssetController.swift
//  neoBMSCare
//
//  Created by mac on 7/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka
class DetailAssetController: MasterFormController {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        createRightBarItem()
        setBackButton()
        self.title = "Chi tiết thiết bị"
       form
        +++ Section(){ section in
            var header = HeaderFooterView<HeaderDetailAsset>(.nibFile(name: "HeaderDetailAsset", bundle: nil))
            header.height = {300}
            header.onSetupView = { view, _ in
                
            }
            section.header = header
        }
        <<< TextRow() {
            $0.title = "Mã"
            $0.value = "A 0792"
        }
        <<< TextRow() {
            $0.title = "Số lượng"
            $0.value = "16"
        }
        <<< TextRow() {
            $0.title = "Bảo trì"
            $0.value = "Có"
        }
        <<< PickerInlineRow<String>() {
            $0.title = "Định kỳ"
            $0.value = "6 tháng"
            $0.options = Array(1...12).map{"\($0) tháng"}
        }
        +++ Section("Vị trí")
        <<< TextAreaRow(){
            $0.value = "Ở tầng 1 gần nhà A4 - 10"
        }
        
    }
    func setupDataSource() {
        
    }
    func createRightBarItem() {
        let bar = UIBarButtonItem(badge: nil, image: (UIImage(named: "clock")?.resize(width: 24, height: 24)?.withRenderingMode(.alwaysTemplate))!, target: self, action: #selector(didPressHistory))
        self.navigationItem.rightBarButtonItem = bar
    }
    @objc func didPressHistory() {
        let vc = HistoryMaintenceController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK:- Outlet Actions

}
